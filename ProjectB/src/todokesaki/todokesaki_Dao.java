package todokesaki;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import DTO.CartDTO;
import DTO.KaiinDTO;
import DTO.KyosankigyouDTO;

public class todokesaki_Dao {
	public static ArrayList<KyosankigyouDTO> getkyosankigyou(int kaishano){

		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		KyosankigyouDTO record = new KyosankigyouDTO();
		ArrayList<KyosankigyouDTO> kaishadata = new ArrayList<KyosankigyouDTO>();

		try {

			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			String sql = "SELECT * FROM tkyosankigyou WHERE kaishano = '" + kaishano + "'";
            System.out.println(sql);
			ResultSet result = startment.executeQuery(sql);

			if (result.next()) {
				record.setKaishamei(result.getString("kaishamei"));
				record.setPostcode(result.getString("kaishapost"));
				record.setAddress(result.getString("kaishaad"));
				record.setTantoushadenwano(result.getString("tantoushatel"));
				kaishadata.add(record);
			}

		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return kaishadata;

	}

	public static ArrayList<KaiinDTO> getjitaku(int kaishano,String shainno, String kaishamei){

		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		KaiinDTO record = new KaiinDTO();
		ArrayList<KaiinDTO> jitakudata = new ArrayList<KaiinDTO>();

		try {

			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			String sql = "SELECT shainno,katakana,kanji,tel,fax,address,tkaiin.postcode,tkyosankigyou.kaishamei,shozokubusho,memo,nyukaidate FROM tkaiin,tkyosankigyou WHERE tkyosankigyou.kaishamei Like '%"+kaishamei+"%' and tkaiin.shainno='"+shainno+"'and tkyosankigyou.kaishano=tkaiin.kaishano";
            System.out.println(sql);
			ResultSet result = startment.executeQuery(sql);

			if (result.next()) {
				record.setKanji(result.getString("kanji"));
				record.setPostcode(result.getString("postcode"));
				record.setAddress(result.getString("address"));
				record.setDenwano(result.getString("tel"));
				jitakudata.add(record);

			}
		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return jitakudata;

	}


	public static ArrayList<CartDTO> todokesaki(int juchuno,String todokesakikubun,String todokesakipost,String todokesakiad,String todokesakimei,String todokesakitel,String rusu){
        String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String password = "";

        Connection con = null;
        Statement startment = null;

        ArrayList<CartDTO> Cart_data = new ArrayList<CartDTO>();
        java.util.Date d = new java.util.Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        java.sql.Date nowdate = new java.sql.Date(cal.getTimeInMillis());


        try {
            // MySQLとの接続を開始
            Class.forName("org.gjt.mm.mysql.Driver");
            con = DriverManager.getConnection(url, user, password);
            startment = con.createStatement();


            // 実行するSQLを定義
            // 定義したSQLを実行して、実行結果を変数resultに代入
            String sql = "INSERT INTO cart(nyuryoku,`juchuno`,`todokesakikubun`,`todokesakipost`,`todokesakiad`,`todokesakimei`,`todokesakitel`,`rusu`,`update`) VALUES('2','"
    						+ juchuno + "','"
    						+ todokesakikubun + "','"
            				+ todokesakipost + "','"
            				+ todokesakiad + "','"
            				+ todokesakimei + "','"
            				+ todokesakitel + "','"
            				+ rusu + "','"
            				+ nowdate + "')";

            System.out.println(sql);
            startment.executeUpdate(sql);


           //変数resultの内容を順次出力
//          while (result.next()) {
//          	CartDTO record = new CartDTO();
//          	record.setJuchuno(result.getInt("juchuno"));
//          	record.setColor(result.getString("color"));
//          	record.setGara(result.getString("gara"));
//          	record.setSize(result.getString("size"));
//          	record.setSuryo(result.getInt("suryo"));
//          	Cart_data.add(record);
//          }



        // エラー情報を取得
        } catch (ClassNotFoundException err) {
            System.out.println("例外発生:" + err);

        } catch (SQLException err){
        	System.out.println("例外発生:" + err);
        	try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        finally{
            // MySQLとの接続を終了
        	try {
        		if (startment != null){
        			startment.close();
        		}
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        	try {
        		if (con != null){
        			con.close();
        		}
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        }
		return Cart_data;
	}


}