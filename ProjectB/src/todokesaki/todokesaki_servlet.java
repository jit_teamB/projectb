package todokesaki;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KaiinDTO;
import DTO.KyosankigyouDTO;
import delete_cart.delete_DAO;

/**
 * Servlet implementation class todokesaki_servlet
 */
@WebServlet("/todokesaki_servlet")
public class todokesaki_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public todokesaki_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		HttpSession session1=request.getSession();
		HttpSession session2=request.getSession();
		HttpSession session3=request.getSession();

		String shainno = (String) session1.getAttribute("shainno");
		int kaishano = (int) session2.getAttribute("kaishano");
		int juchuno = (int) session3.getAttribute("juchuno");

		delete_DAO.delcart_todokesaki(juchuno);
		String todokesakikubun = request.getParameter("todokesakikubun");
		System.out.println(todokesakikubun);
       String kaishamei=(String)session1.getAttribute("kaishamei");
		String todokesakipost = null;
		String todokesakiad = null;
		String todokesakimei = null;
		String todokesakitel = null;
		String rusu = "";

		switch(todokesakikubun){
		case "1":{
			 ArrayList<KaiinDTO> jitaku = todokesaki_Dao.getjitaku(kaishano, shainno,kaishamei);
			 for(KaiinDTO i : jitaku){
				 todokesakipost = i.getpostcode();
				 todokesakiad = i.getAddress();
					todokesakimei = request.getParameter("todokesakimei1");
				 if(todokesakimei.equals("")){
					 todokesakimei = i.getkanji();
					}
				 todokesakitel = i.getDenwano();
				 rusu = request.getParameter("rusu1");
				 }
			 todokesaki_Dao.todokesaki(juchuno, todokesakikubun, todokesakipost, todokesakiad, todokesakimei, todokesakitel, rusu);
			 System.out.println(todokesakipost + todokesakiad + todokesakimei + todokesakitel);
			break;
		}
		case "2":{
			ArrayList<KyosankigyouDTO> kaisha = todokesaki_Dao.getkyosankigyou(kaishano);
			for(KyosankigyouDTO j : kaisha){
				todokesakipost = j.getPostcode();
				todokesakiad = j.getAddress();
				todokesakimei = request.getParameter("todokesakimei2");
				if(todokesakimei.equals("")){
					todokesakimei=(String) session1.getAttribute("kanji");
				}
				todokesakitel = j.getTantoushadenwano();
				rusu = request.getParameter("rusu2");
				}
				todokesaki_Dao.todokesaki(juchuno, todokesakikubun, todokesakipost, todokesakiad, todokesakimei, todokesakitel, rusu);
				System.out.println(todokesakipost + todokesakiad + todokesakimei + todokesakitel);
			break;
		}
		case "3":{
			todokesakipost = request.getParameter("todokesakipost");
			todokesakiad = request.getParameter("todokesakiad");
			todokesakimei = request.getParameter("todokesakimei3");
			todokesakitel = request.getParameter("todokesakitel");
			rusu = request.getParameter("rusu3");
			todokesaki_Dao.todokesaki(juchuno, todokesakikubun, todokesakipost, todokesakiad, todokesakimei, todokesakitel, rusu);
			break;
		}
		}
        RequestDispatcher rd=request.getRequestDispatcher("Layout_jsp/B008.jsp");
        rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF8");
		doGet(request,response);
	}

}
