package updateKeiyaku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KeiyakutenDTO;

/**
 * Servlet implementation class updatekeiyakuDao
 */
@WebServlet("/updatekeiyakuDao")
public class B044 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B044() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
		//htmlから取得
		String keiyakuname = request.getParameter("keiyakuname");
		String keiyakukana = request.getParameter("keiyakukana");
		String keiyakutel = request.getParameter("keiyakutel");
		String keiyakufax = request.getParameter("keiyakufax");
		String keiyakupost = request.getParameter("keiyakupost");
		String keiyakuadd = request.getParameter("keiyakuadd");
		String keiyakutanto = request.getParameter("keiyakutanto");

		HttpSession session = request.getSession();
		int keiyakuno = (int)session.getAttribute("keiyakuno");

		//編集DAOへ
		updatekeiyakuDao.getUpdatekeiyaku(keiyakuno,keiyakuname,keiyakukana,keiyakutel,keiyakufax,keiyakupost,keiyakuadd,keiyakutanto);

		ArrayList<KeiyakutenDTO> keiyakuList = updatekeiyakuDao.getSearchkeiyaku(keiyakukana);

		request.setAttribute("keiyakuList", keiyakuList);
		RequestDispatcher rd = request.getRequestDispatcher("B045.jsp");//アクセス先のJSP
		rd.forward(request, response);



	}

}
