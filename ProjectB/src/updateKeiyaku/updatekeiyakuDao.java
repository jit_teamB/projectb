package updateKeiyaku;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KeiyakutenDTO;

public class updatekeiyakuDao {
	//契約店編集
	public static void getUpdatekeiyaku(int keiyakuno, String keiyakuname,String keiyakukana,String keiyakutel,String keiyakufax,String keiyakupost,String keiyakuadd,String keiyakutanto){
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			//System.out.println("接続成功");

			// 実行するSQLを定義
			String sql = "UPDATE tkeiyakuten SET keiyakutenmei='"+keiyakuname+"', keiyakutenkana='"+keiyakukana+"', keiyakutenpost='"+keiyakupost+"', keiyakutenad='"+keiyakuadd+"', keiyakutentel='"+keiyakutel+"', keiyakutenfax='"+keiyakufax+"', keiyakutentanto'"+keiyakutanto+"' where keiyakutenno="+keiyakuno;//DBにあわせて 契約店番号が一致する行の編集

			// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
			int count = startment.executeUpdate(sql);

			if (count!= 1) {
				System.out.println("更新した件数:" + count);
			} else {
				System.out.println("更新した件数:" + count);
			}

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			try{
				con.rollback();
			}catch(SQLException sqep2){
				System.out.println(sqep2);
			}
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
	}
	//契約店検索
		public static ArrayList<KeiyakutenDTO> getSearchkeiyaku(String keiyakukana) {
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			Connection con = null;
			Statement startment = null;

			ArrayList<KeiyakutenDTO> keiyakuList = new ArrayList<KeiyakutenDTO>();

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				// 実行するSQLを定義
				String sql = "SELECT * FROM tkeiyakuten where keiyakutenkana = "+ keiyakukana;

				// 定義したSQLを実行して、実行結果を変数resultに代入
				ResultSet result = startment.executeQuery(sql);

				// 変数resultの内容を順次出力
				while (result.next()) {//DBが出来てから
					KeiyakutenDTO keiyaku = new KeiyakutenDTO();
					keiyaku.setKeiyakutenno(result.getInt("keiyakuten"));//契約店番号
					keiyaku.setKeiyakutenmei(result.getString("keiyakutenmei"));
					keiyaku.setKeiyakutenkana(result.getString("keiyakutenkana"));
					keiyaku.setKeiyakutenpost(result.getString("keiyakutenpost"));
					keiyaku.setKeiyakutenad(result.getString("keiyakutenad"));
					keiyaku.setKeiyakutendenwano(result.getString("keiyakutentel"));
					keiyaku.setKeiyakutenfaxno(result.getString("keiyakutenfax"));
					keiyaku.setKeiyakutentanto(result.getString("keiyakutentanto"));
					keiyakuList.add(keiyaku);// 配列に追加
				}

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
			return keiyakuList;
		}
}
