package updateKeiyaku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KeiyakutenDTO;

/**
 * Servlet implementation class B043
 */
@WebServlet("/B043")
public class B043 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B043() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
		//DBから取得
		int keiyakuno = Integer.parseInt(request.getParameter("keiyakuno"));//htmlから取得
		//ArrayListに
		ArrayList<KeiyakutenDTO> keiyakuList = SearchkeiyakuDao.getSearchkeiyaku(keiyakuno);//

		HttpSession session = request.getSession();
		session.setAttribute("keiyakuno", keiyakuno);

		//requestに保存してjspで取得データ使えるように
		request.setAttribute("keiyakuList", keiyakuList);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp.B044.jsp");//アクセス先のJSP
		rd.forward(request, response);
	}

}
