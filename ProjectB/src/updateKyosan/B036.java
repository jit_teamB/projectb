package updateKyosan;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KyosankigyouDTO;

/**
 * Servlet implementation class B036
 */
@WebServlet("/B036")
public class B036 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B036() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
				request.setCharacterEncoding("utf8");
				//DBから取得
				int kaishano = Integer.parseInt(request.getParameter("kaishano"));//htmlから取得
				//ArrayListに
				ArrayList<KyosankigyouDTO> kyosanList = SearchkyosanDao.getSearchkyosan(kaishano);

				HttpSession session = request.getSession();
                session.setAttribute("kaishano", kaishano);
				//requestに保存してjspで取得データ使えるように
				request.setAttribute("kyosanList", kyosanList);
				RequestDispatcher rd = request.getRequestDispatcher("");//アクセス先のJSP
				rd.forward(request, response);
	}

}
