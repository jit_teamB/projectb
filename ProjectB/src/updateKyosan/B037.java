package updateKyosan;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import updateKeiyaku.updatekeiyakuDao;
import DTO.KeiyakutenDTO;

/**
 * Servlet implementation class B037
 */
@WebServlet("/B037")
public class B037 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B037() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
				request.setCharacterEncoding("utf8");
				//htmlから取得
				String kyosankanji = request.getParameter("kaishamei");
				String kyosankana = request.getParameter("kaishakana");
				String kyosantel = request.getParameter("kaishatel");
				String kyosanfax = request.getParameter("kaishafax");
				String kyosanpost = request.getParameter("postcode");
				String kyosanadd = request.getParameter("address");
				String kyosantanto = request.getParameter("tantoubusho");
				String tantoushakanji = request.getParameter("tantoushakanji");
				String tantoushakatakana = request.getParameter("tantoushakatakana");
				String tantoushatel = request.getParameter("tantoushatel");
				String tantoushafax = request.getParameter("tantoushafax");
				int kyuyo = Integer.parseInt(request.getParameter("kyuyo"));

				HttpSession session = request.getSession();
				int kaishano = (int)session.getAttribute("kaishano");

				//編集DAOへ
				upadatekyosanDao.getUpdatekyosan(kaishano,kyosankanji,kyosankana,kyosantel,kyosanfax,kyosanpost,kyosanadd,
						kyosantanto,tantoushakanji,tantoushakatakana,tantoushatel,tantoushafax,kyuyo);

				//編集した情報を取得してみよう！
				ArrayList<KeiyakutenDTO> kyosanList = updatekeiyakuDao.getSearchkeiyaku(kyosankana);

				request.setAttribute("kyosanList", kyosanList);
				RequestDispatcher rd = request.getRequestDispatcher("B038.jsp");
                rd.forward(request,response);
	}

}
