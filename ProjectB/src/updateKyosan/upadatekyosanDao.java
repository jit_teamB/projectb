package updateKyosan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KyosankigyouDTO;

public class upadatekyosanDao {
	//協賛企業編集
		public static void getUpdatekyosan(int kaishano, String kyosankanji, String kyosankana, String kyosantel, String kyosanfax, String kyosanpost, String kyosanadd, String kyosantanto, String tantoushakanji, String tantoushakatakana, String tantoushatel, String tantoushafax, int kyuyo){
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/hanbai?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			Connection con = null;
			Statement startment = null;

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				//System.out.println("接続成功");

				// 実行するSQLを定義
				String sql = "UPDATE tkyosankigyou SET kaishamei='"+kyosankanji+"', kaishakatakana='"+kyosankana+"', kaishapost='"+kyosanpost+"', "
						+ "kaishaad='"+kyosanadd+"', kaishatel='"+kyosantel+"', kaishafax='"+kyosanfax+"', tantoubuho='"+kyosantanto+"', "
								+ "tantoushakanji='"+tantoushakanji+"', tantoushakatakana='"+tantoushakatakana+"', tantoushatel='"+tantoushatel+"', "
										+ "tantoushafax='"+tantoushafax+"', kyuuyosiharai='"+kyuyo+"' where kaishano ='"+kaishano+"'";//DBにあわせて 商品番号が一致する行の編集

				// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
				int count = startment.executeUpdate(sql);

				if (count!= 1) {
					System.out.println("更新した件数:" + count);
				} else {
					System.out.println("更新した件数:" + count);
				}

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				try{
					con.rollback();
				}catch(SQLException sqep2){
					System.out.println(sqep2);
				}
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
		}
		//協賛企業検索
				public static ArrayList<KyosankigyouDTO> getSearchkyosan(int kyosanno) {
					// 接続情報を定義
					String url = "jdbc:mysql://localhost:3306/bbs?useUnicode=true&characterEncoding=utf8";
					String user = "root";
					String password = "";
					Connection con = null;
					Statement startment = null;

					ArrayList<KyosankigyouDTO> kyosanList = new ArrayList<KyosankigyouDTO>();

					try {
						// MySQLとの接続を開始
						Class.forName("org.gjt.mm.mysql.Driver");
						con = DriverManager.getConnection(url, user, password);
						startment = con.createStatement();
						System.out.println("接続成功");

						// 実行するSQLを定義
						String sql = "SELECT * FROM tkyosankigyou where keiyakutenno = "+ kyosanno;

						// 定義したSQLを実行して、実行結果を変数resultに代入
						ResultSet result = startment.executeQuery(sql);

						// 変数resultの内容を順次出力
						while (result.next()) {//DBが出来てから
							KyosankigyouDTO kyosan = new KyosankigyouDTO();
							kyosan.setKaishamei(result.getString("kaishamei"));
							kyosan.setKaishameikana(result.getString("kaishameikana"));
							kyosan.setPostcode(result.getString("kaishapost"));
							kyosan.setAddress(result.getString("kaishaad"));
		                    kyosan.setKaishatel(result.getString("kaishatel"));
		                    kyosan.setKaishafax(result.getString("kaishafax"));
		                    kyosan.setTantoubusho(result.getString("tantoubusho"));
		                    kyosan.setTantoushakatakana(result.getString("tantoushakatakana"));
		                    kyosan.setTantoushakanji(result.getString("tantoushakanji"));
		                    kyosan.setTantoushadenwano(result.getString("tantoushatel"));
		                    kyosan.setTantoushafaxno(result.getString("tantoushafax"));
							kyosanList.add(kyosan);// 配列に追加
						}

						// MySQLとの接続を終了
						con.close();
						System.out.println("接続終了");

						// エラー情報を取得
					} catch (ClassNotFoundException conf) {
						System.out.println(conf);

					} catch (SQLException sqep) {
						System.out.println(sqep);
					} finally {
						if (startment != null) {
							try {
								startment.close();
							} catch (SQLException e) {
							}
						}
						if (con != null) {
							try {
								con.close();
							} catch (SQLException e) {

							}
						}
					}
					return kyosanList;
				}
}
