package Chumonshuturyoku;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Chumonuketuke.ChumonuketukehyoDAO;
import DTO.ChumonuketukehyoDTO;

/**
 * Servlet implementation class B012Servlet
 */
@WebServlet("/B012Servlet")
public class B012Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B012Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		 HttpSession session1=request.getSession();
		 HttpSession session2=request.getSession();
		 HttpSession sessionArray=request.getSession();

		// int juchuno=(Integer)session1.getAttribute("juchuno");
        // String shainno=(String)session2.getAttribute("shainno");

		int juchuno=Integer.parseInt(request.getParameter("juchuno"));
		 String shainno=request.getParameter("shainno");

        System.out.println(juchuno);
        session1.setAttribute("juchuno", juchuno);
        ArrayList<ChumonuketukehyoDTO> Chumon = ChumonuketukehyoDAO.getChumonuketukehyo_juchu(juchuno,shainno);
        sessionArray.setAttribute("Chumon", Chumon);


            RequestDispatcher rd=request.getRequestDispatcher("Layout_jsp/B009.jsp");
            rd.forward(request,response);



		//TODO Auto-generated method stub
	}

	}



