package kaiinToroku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.KaiinDTO;
import DTO.KyosankigyouDTO;

/**
 * Servlet implementation class B019
 */
@WebServlet("/B019")
public class B019 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B019() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字コード
		request.setCharacterEncoding("utf8");

		//htmlから
        try{
        	String shainno = request.getParameter("shainno");
        	String kanji = request.getParameter("shainkanji");
        	String kana = request.getParameter("shainkana");
        	int tel = Integer.parseInt(request.getParameter("tel"));
        	int fax = Integer.parseInt(request.getParameter("fax"));
        	String yubin = request.getParameter("shainpost");
        	String jusho = request.getParameter("shainadd");
        	String kaishamei = request.getParameter("kaishamei");
        	String kaishabusho = request.getParameter("shozokubusho");
        	String memo =request.getParameter("bikou");

        	ArrayList<KyosankigyouDTO> kyosanList = kaiinDao.getSearchkyosan(kaishamei);
        	KyosankigyouDTO kyosan = new KyosankigyouDTO();

            int kaishano=kyosan.getKaishano();
        	kaiinDao.getInsertkaiin(shainno, kanji, kana, tel, fax, yubin, jusho, kaishano, kaishabusho, memo);

        	ArrayList<KaiinDTO> kaiinList = kaiinDao.getSearchkaiin(shainno);

        	request.setAttribute("kaiinList", kaiinList);
    		RequestDispatcher rd = request.getRequestDispatcher("../B021.jsp");//アクセス先のJSP
    		rd.forward(request, response);


        }catch(Exception e){

        }finally{

        }

	}

}
