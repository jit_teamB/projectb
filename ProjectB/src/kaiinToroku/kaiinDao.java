package kaiinToroku;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KaiinDTO;
import DTO.KyosankigyouDTO;

public class kaiinDao {

	//会員追加
		public static void getInsertkaiin(String shainno,String kanji,String kana,int tel,int fax,String yubin,String jusho,
				int kaishano,String kaishabusho,String memo) {
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			// Class.forName("org.gjt.mm.mysql.Driver");
			Connection con = null;
			Statement startment = null;

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				// 実行するSQLを定義
				String sql = "SELECT tkaiin "+ "VALUES ("+",9999/12/31)";//DBのカラムにあわせて書く
				String sql1 = "insert into tkaiin(shainno,kanji,katakana,tel,fax,postcode,address,kaishano,memo,kaishabusho,todokesakikubun,yoshingendogaku,orderdate) "
				    + "VALUES ('"+shainno+"','"+kanji+"','"+kana+"','"+tel+"','"+fax+"','"+yubin+"','"+jusho+"','"+memo+"','"+kaishabusho+"',1,500000,9999/12/31)";//DBのカラムにあわせて書く



				// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
				int count = startment.executeUpdate(sql1);

				if (count!=1) {
					System.out.println("更新した件数:" + count);
				} else {
					System.out.println("更新した件数:" + count);
				}

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}

		}

		//会員検索
		public static ArrayList<KaiinDTO> getSearchkaiin(String shainno) {
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/bbs?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			Connection con = null;
			Statement startment = null;

			ArrayList<KaiinDTO> kaiinList = new ArrayList<KaiinDTO>();

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				// 実行するSQLを定義
				String sql = "SELECT * FROM tkaiin where keiyakutenno = "+ shainno;

				// 定義したSQLを実行して、実行結果を変数resultに代入
				ResultSet result = startment.executeQuery(sql);

				// 変数resultの内容を順次出力
				while (result.next()) {//DBが出来てから
					KaiinDTO shain = new KaiinDTO();
					shain.setShainno(result.getString("shainno"));
					shain.setKanji(result.getString("kanji"));
					shain.setKatakana(result.getString("katakana"));
					shain.setAddress(result.getString("address"));
					shain.setPostcode(result.getString("postcode"));
					shain.setDenwano(result.getString("tel"));
					shain.setFaxno(result.getString("fax"));
					shain.setShozokubusho(result.getString("shozokubusho"));
					shain.setMemo(result.getString("memo"));
					kaiinList.add(shain);// 配列に追加
				}

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
			return kaiinList;
		}
		//企業番号検索
		public static ArrayList<KyosankigyouDTO> getSearchkyosan(String kaishamei) {
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/bbs?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			Connection con = null;
			Statement startment = null;

			ArrayList<KyosankigyouDTO> kyosanList = new ArrayList<KyosankigyouDTO>();

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				// 実行するSQLを定義
				String sql = "SELECT * FROM tkyosankigyou where kaishamei like '%"+ kaishamei+"%'";

				// 定義したSQLを実行して、実行結果を変数resultに代入
				ResultSet result = startment.executeQuery(sql);

				// 変数resultの内容を順次出力
				while (result.next()) {//DBが出来てから
					KyosankigyouDTO kyosan = new KyosankigyouDTO();
					kyosan.setKaishano(result.getInt("kaishano"));
					kyosan.setKaishamei(result.getString("kaishamei"));
					kyosan.setKaishameikana(result.getString("kaishameikana"));
					kyosanList.add(kyosan);// 配列に追加
				}

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
			return kyosanList;
		}
}
