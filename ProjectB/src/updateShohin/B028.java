package updateShohin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.shohinDTO;

/**
 * Servlet implementation class B028
 */
@WebServlet("/B028")
public class B028 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B028() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
				request.setCharacterEncoding("utf8");
				//jspから取得
				int shohinno = Integer.parseInt(request.getParameter("shohinno"));
				String shohinmei = request.getParameter("shohinmei");
				String gara  = request.getParameter("gara");
				String color  = request.getParameter("color");
				String size  = request.getParameter("size");
				String endday  = request.getParameter("endday");
				int price  = Integer.parseInt(request.getParameter("price"));
				int keiyakutenno  = Integer.parseInt(request.getParameter("keiyakutenno"));
				String startday = request.getParameter("startday");

				//編集DAOへ
				updateShohinDao.getUpdateshohin(shohinno,shohinmei,gara,color,size,endday,price,keiyakutenno,startday);

				//検索
				ArrayList<shohinDTO> shohinList = updateShohinDao.getSearchshohin(shohinmei);

				request.setAttribute("shohinList", shohinList);
				RequestDispatcher rd = request.getRequestDispatcher("B029.jsp");
				rd.forward(request, response);
	}

}
