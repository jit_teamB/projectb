package updateShohin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.shohinDTO;

/**
 * Servlet implementation class B004A
 */
@WebServlet("/B004A")
public class B004A extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B004A() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
				request.setCharacterEncoding("utf8");
				//DBから取得
				int shohinno = Integer.parseInt(request.getParameter("shohin"));//htmlから取得
				//ArrayListに
				ArrayList<shohinDTO> shohinList = searchShohin.getSearchshohin(shohinno);//

				//requestに保存してjspで取得データ使えるように
				request.setAttribute("shohinList", shohinList);
				RequestDispatcher rd = request.getRequestDispatcher("B028.jsp");//アクセス先のJSP
				rd.forward(request, response);
	}

}
