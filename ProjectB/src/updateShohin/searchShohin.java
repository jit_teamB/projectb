package updateShohin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.shohinDTO;

public class searchShohin {
	// 商品検索
	public static ArrayList<shohinDTO> getSearchshohin(int shohinno) {
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/bbs?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<shohinDTO> shohinList = new ArrayList<shohinDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");

			// 実行するSQLを定義
			String sql = "SELECT * FROM tshohin where shohinno = "+ shohinno;

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {// DBが出来てから
				shohinDTO shohin = new shohinDTO();
				shohin.setShohinno(result.getInt("shohinno"));// 商品番号
				shohin.setShohinmei(result.getString("shohinmei"));
				shohin.setGara(result.getString("gara"));
				shohin.setColor(result.getString("color"));
				shohin.setSize(result.getString("size"));
                shohin.setStartdate(result.getDate("startdate"));
                shohin.setEnddate(result.getDate("enddate"));
                shohin.setPrice(result.getInt("price"));
                shohin.setKeiyakuten(result.getInt("keiyakutenno"));
				shohinList.add(shohin);// 配列に追加
			}

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
		return shohinList;
	}
}
