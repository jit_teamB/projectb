package DTO;

import java.sql.Timestamp;

public class KeiyakutenDTO {


	private int keiyakutenno;
	private String keiyakutenmei;
	private String keiyakutenpost;
	private String keiyakutenad;
	private String keiyakutendenwano;
	private String keiyakutenfaxno;
	private String keiyakutentanto;
	private Timestamp update;
	private String keiyakutenkana;



	public int getKeiyakutenno(){
	return keiyakutenno;
	}

	public void setKeiyakutenno(int keiyakutenno) {
		this.keiyakutenno = keiyakutenno;
	}

	public String getKeiyakutenmei() {
		return keiyakutenmei;
	}

	public void setKeiyakutenmeii(String keiyakutenmei) {
		this.keiyakutenmei = keiyakutenmei;
	}

	public String getKeiyakutenpost() {
		return keiyakutenpost;
	}

	public void setKeiyakutenpost(String keiyakutenpost) {
		this.keiyakutenpost = keiyakutenpost;

	}

	public String getKeiyakutenad() {
		return keiyakutenad;
	}

	public void setKeiyakutenad(String keiyakutenad) {
		this.keiyakutenad = keiyakutenad;
	}

	public String getKeiyakutendenwano() {
		return keiyakutendenwano;
	}

	public void setKeiyakutendenwano(String keiyakutendenwano) {
		this.keiyakutendenwano = keiyakutendenwano;
	}

	public String getKeiyakutenfaxno() {
		return keiyakutenfaxno;
	}

	public void setKeiyakutenfaxno(String keiyakutenfaxno) {
		this.keiyakutenfaxno = keiyakutenfaxno;
	}

	public String getKeiyakutentanto() {
		return keiyakutentanto;
	}

	public void setKeiyakutentanto(String keiyakutentanto) {
		this.keiyakutentanto = keiyakutentanto;
	}

	public void setKeiyakutenmei(String keiyakutenmei) {
		this.keiyakutenmei = keiyakutenmei;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}

	public String getKeiyakutenkana() {
		return keiyakutenkana;
	}

	public void setKeiyakutenkana(String keiyakutenkana) {
		this.keiyakutenkana = keiyakutenkana;
	}


}
