package DTO;

import java.sql.Timestamp;

public class JuchuDTO {

	private int juchuno;
	private String siharaikubun;
	private int bunkatukaisu;
	private int todokesakikubun;
	private String todokesakipost;
	private int total;
	private Timestamp chumonbi;
	private String todokesakiad;
	private String juchutantousha;
	private String hacchutantousha;
	private int todokesakitel;
	private String rusu;
	private String memo;
	private int shokei;
	private int specialfee;
	private Timestamp kojostart;
	private int kaishano;
	private int shainno;
	private String todokesakiname;
	private String juchucancel;
	private Timestamp hacchubi;
	private Timestamp update;
	private String tukisiharai;



	public int getJuchuno(){
	return juchuno;
	}

	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}

	public String getSiharaikubun() {
		return siharaikubun;
	}

	public void setSiharaikubun(String siharaikubun) {
		this.siharaikubun = siharaikubun;
	}

	public int getBunkatukaisu() {
		return bunkatukaisu;
	}

	public void setBunkatukaisu(int bunkatukaisu) {
		this.bunkatukaisu = bunkatukaisu;

	}

	public int getTodokesakikubun() {
		return todokesakikubun;
	}

	public void setTodokesakikubun(int todokesakikubun) {
		this.todokesakikubun = todokesakikubun;
	}

	public String getTodokesakipost() {
		return todokesakipost;
	}

	public void setTodokesakipost(String todokesakipost) {
		this.todokesakipost = todokesakipost;
	}

	public String getJuchutantousha() {
		return juchutantousha;
	}

	public void setJuchutantousha(String juchutantousha) {
		this.juchutantousha = juchutantousha;
	}

	public String getHacchutantousha() {
		return hacchutantousha;
	}

	public void setHacchutantousha(String hacchutantousha) {
		this.hacchutantousha = hacchutantousha;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Timestamp getChumonbi() {
		return chumonbi;
	}

	public void setChumonbi(Timestamp chumonbi) {
		this.chumonbi = chumonbi;
	}

	public String getTodokesakiad() {
		return todokesakiad;
	}

	public void setTodokesakiad(String todokesakiad) {
		this.todokesakiad = todokesakiad;

	}

	public int getTodokesakitel() {
		return todokesakitel;
	}

	public void setTodokesakitel(int todokesakitel) {
		this.todokesakitel = todokesakitel;
	}

	public String getRusu() {
		return rusu;
	}

	public void setRusu(String rusu) {
		this.rusu = rusu;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public int getShokei() {
		return shokei;
	}

	public void setShokei(int shokei) {
		this.shokei = shokei;
	}

	public int getSpecialfee() {
		return specialfee;
	}

	public void setSpecialfee(int specialfee) {
		this.specialfee = specialfee;
	}

	public Timestamp getKojostart() {
		return kojostart;
	}

	public void setKojostart(Timestamp kojostart) {
		this.kojostart = kojostart;
	}

	public int getKaishano() {
		return kaishano;
	}

	public void setKaishano(int kaishano) {
		this.kaishano = kaishano;
	}

	public int getShainno() {
		return shainno;
	}

	public void setShainno(int shainno) {
		this.shainno = shainno;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}
}
