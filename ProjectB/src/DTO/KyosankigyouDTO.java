package DTO;

import java.sql.Timestamp;

public class KyosankigyouDTO {

	private int kaishano;
	private String kaishamei;
	private String tantoubusho;
	private String tantoushakanji;
	private String tantoushakatakana;
	private String tantoushadenwano;
	private String tantoushafaxno;
	private int kyuuyosiharai;
	private String postcode;
	private int todokesakino;
	private String address;
	private String kaishatel;
	private String kaishafax;
	private String kaishameikana;
	private Timestamp update;



	public int getKaishano(){
	return kaishano;
	}

	public void setKaishano(int kaishano) {
		this.kaishano = kaishano;
	}

	public String getKaishamei() {
		return kaishamei;
	}

	public void setKaishamei(String kaishamei) {
		this.kaishamei = kaishamei;
	}

	public String getTantoubusho() {
		return tantoubusho;
	}

	public void setTantoubusho(String tantoubusho) {
		this.tantoubusho = tantoubusho;

	}

	public String getTantoushakanji() {
		return tantoushakanji;
	}

	public void setTantoushakanji(String tantoushakanji) {
		this.tantoushakanji = tantoushakanji;
	}

	public String getTantoushakatakana() {
		return tantoushakatakana;
	}

	public void setTantoushakatakana(String tantoushakatakana) {
		this.tantoushakatakana = tantoushakatakana;
	}

	public String getTantoushadenwano() {
		return tantoushadenwano;
	}

	public void setTantoushadenwano(String tantoushadenwano) {
		this.tantoushadenwano = tantoushadenwano;
	}

	public String getTantoushafaxno() {
		return tantoushafaxno;
	}

	public void setTantoushafaxno(String tantoushafaxno) {
		this.tantoushafaxno = tantoushafaxno;
	}

	public int getKyuuyosiharai() {
		return kyuuyosiharai;
	}

	public void setKyuuyosiharai(int kyuuyosiharai) {
		this.kyuuyosiharai = kyuuyosiharai;
	}


	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public int getTodokesakino() {
		return todokesakino;
	}

	public void setTodokesakino(int todokesakino) {
		this.todokesakino = todokesakino;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getKaishatel() {
		return kaishatel;
	}

	public void setKaishatel(String kaishatel) {
		this.kaishatel = kaishatel;
	}

	public String getKaishafax() {
		return kaishafax;
	}

	public void setKaishafax(String kaishafax) {
		this.kaishafax = kaishafax;
	}



	public String getKaishameikana() {
		return kaishameikana;
	}

	public void setKaishameikana(String kaishameikana) {
		this.kaishameikana = kaishameikana;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}
}
