package DTO;

import java.sql.Date;
import java.sql.Timestamp;

public class shohinDTO {


	private int shohinno;
	private String shohinmei;
	private String gara;
	private String color;
	private String size;
	private Date enddate;
	private int price;
	private int keiyakuten;
	private Date startdate;
	private String besso;
	private String tani;
	private int toriatukai;
	private String memo;
	private String haisosize;
	private Timestamp update;



	public int getShohinno(){
	return shohinno;
	}

	public void setShohinno(int shohinno) {
		this.shohinno = shohinno;
	}

	public String getShohinmei() {
		return shohinmei;
	}

	public void setShohinmei(String shohinmei) {
		this.shohinmei = shohinmei;
	}

	public String getGara() {
		return gara;
	}

	public void setGara(String gara) {
		this.gara = gara;

	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getKeiyakuten() {
		return keiyakuten;
	}

	public void setKeiyakuten(int keiyakuten) {
		this.keiyakuten = keiyakuten;

	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date date) {
		this.startdate = date;
	}

	public String getBesso() {
		return besso;
	}

	public void setBesso(String besso) {
		this.besso = besso;
	}

	public String getTani() {
		return tani;
	}

	public void setTani(String tani) {
		this.tani = tani;
	}

	public int getToriatukai() {
		return toriatukai;
	}

	public void setToriatukai(int toriatukai) {
		this.toriatukai = toriatukai;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}

	public String getHaisosize() {
		return haisosize;
	}

	public void setHaisosize(String haisosize) {
		this.haisosize = haisosize;
	}
}
