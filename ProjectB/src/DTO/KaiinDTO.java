package DTO;

import java.sql.Timestamp;

public class KaiinDTO {

		private String shainno;
		private String kanji;
		private String katakana;
		private String denwano;
		private String faxno;
		private String address;
		private String todokesakino;
		private String postcode;
		private int yosingendogaku;
		private int kaishano;
		private Timestamp nyukaidate;
		private Timestamp orderdate;
		private String memo;
		private String shozokubusho;
		private int tukisiharaigaku;
		private Timestamp update;
		private String kaishamei;


		public String getKaishamei() {
			return kaishamei;
		}

		public void setKaishamei(String kaishamei) {
			this.kaishamei = kaishamei;
		}

		public String getKanji() {
			return kanji;
		}

		public String getPostcode() {
			return postcode;
		}

		public String getShainno() {
			return shainno;
		}

		public void setShainno(String shainno) {
			this.shainno = shainno;
		}

		public String getkanji() {
			return kanji;
		}

		public void setKanji(String kanji) {
			this.kanji = kanji;
		}


		public String getKatakana() {
			return katakana;
		}

		public void setKatakana(String katakana) {
			this.katakana = katakana;

		}


		public String getDenwano() {
			return denwano;
		}

		public void setDenwano(String string) {
			this.denwano = string;
		}


		public String getFaxno() {
			return faxno;
		}

		public void setFaxno(String faxno) {
			this.faxno = faxno;
		}


		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}


		public String getTodokesakino() {
			return todokesakino;
		}

		public void setTodokesakino(String todokesakino) {
			this.todokesakino = todokesakino;
		}


		public String getpostcode() {
			return postcode;
		}

		public void setPostcode(String postcode) {
			this.postcode = postcode;
		}


		public int getYosingendogaku() {
			return yosingendogaku;
		}

		public void setYosingendogaku(int yosingendogaku) {
			this.yosingendogaku = yosingendogaku;
		}


		public int getKaishano() {
			return kaishano;
		}

		public void setKaishano(int kaishano) {
			this.kaishano = kaishano;
		}


		public Timestamp getNyukaidate() {
			return nyukaidate;
		}


		public void setNyukaidate(Timestamp nyukaidate) {
			this.nyukaidate = nyukaidate;
		}


		public Timestamp getOrderdate() {
			return orderdate;
		}

		public void setOrderdate(Timestamp orderdate) {
			this.orderdate = orderdate;
		}


		public String getMemo() {
			return memo;
		}

		public void setMemo(String memo) {
			this.memo = memo;
		}


		public String getShozokubusho() {
			return shozokubusho;
		}

		public void setShozokubusho(String shozokubusho) {
			this.shozokubusho = shozokubusho;
		}


		public int getTukisiharaigaku() {
			return tukisiharaigaku;
		}

		public void setTukisiharaigaku(int tukisiharaigaku) {
			this.tukisiharaigaku = tukisiharaigaku;
		}

		public Timestamp getUpdate() {
			return update;
		}

		public void setUpdate(Timestamp update) {
			this.update = update;
		}
}
