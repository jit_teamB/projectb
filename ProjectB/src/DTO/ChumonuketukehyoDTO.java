package DTO;

import java.sql.Date;

public class ChumonuketukehyoDTO {

	private int juchuno;
	private int shohinno;
	private String gara;
	private String color;
	private String size;
	private int suryo;
	private int siharaikubun;
	private int bunkatukaisu;
	private int todokesakikubun;
	private String todokesakipost;
	private String todokesakiad;
	private String todokesakitel;
	private String rusu;
	private int keiyakutenno;
	private String keiyakutenmei;
	private String haisosize;
	private int total;
	private Date chumonbi;
	private String memo;
	private int shokei;
	private int specialfee;
	private String kojostart;
	private int kaishano;
	private String shainno;
	private int shohincancel;
	private Date hacchubi;
	private String tani;
	private String kanji;
	private String juchutantousha;
	private String hacchutantousha;
	private String shohinmei;
	private String tel;
	private String kaishatel;
	private int price;
	private String shozokubusho;
	private String Todokesakimei;
	private Date update;
	private String kaishamei;
	private int shuturyoku;

	public String getKaishamei() {
		return kaishamei;
	}

	public void setKaishamei(String kaishamei) {
		this.kaishamei = kaishamei;
	}

	/**
	 * juchunoを取得します。
	 *
	 * @return juchuno
	 */
	public int getJuchuno() {
		return juchuno;
	}

	/**
	 * juchunoを設定します。
	 *
	 * @param juchuno
	 *            juchuno
	 */
	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}

	/**
	 * shohinnoを取得します。
	 *
	 * @return shohinno
	 */
	public int getShohinno() {
		return shohinno;
	}

	/**
	 * shohinnoを設定します。
	 *
	 * @param shohinno
	 *            shohinno
	 */
	public void setShohinno(int shohinno) {
		this.shohinno = shohinno;
	}

	/**
	 * garaを取得します。
	 *
	 * @return gara
	 */
	public String getGara() {
		return gara;
	}

	/**
	 * garaを設定します。
	 *
	 * @param gara
	 *            gara
	 */
	public void setGara(String gara) {
		this.gara = gara;
	}

	/**
	 * colorを取得します。
	 *
	 * @return color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * colorを設定します。
	 *
	 * @param color
	 *            color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * sizeを取得します。
	 *
	 * @return size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * sizeを設定します。
	 *
	 * @param size
	 *            size
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * suryoを取得します。
	 *
	 * @return suryo
	 */
	public int getSuryo() {
		return suryo;
	}

	/**
	 * suryoを設定します。
	 *
	 * @param suryo
	 *            suryo
	 */
	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}

	/**
	 * siharaikubunを取得します。
	 *
	 * @return siharaikubun
	 */
	public int getSiharaikubun() {
		return siharaikubun;
	}

	/**
	 * siharaikubunを設定します。
	 *
	 * @param siharaikubun
	 *            siharaikubun
	 */
	public void setSiharaikubun(int siharaikubun) {
		this.siharaikubun = siharaikubun;
	}

	/**
	 * bunkatukaisuを取得します。
	 *
	 * @return bunkatukaisu
	 */
	public int getBunkatukaisu() {
		return bunkatukaisu;
	}

	/**
	 * bunkatukaisuを設定します。
	 *
	 * @param bunkatukaisu
	 *            bunkatukaisu
	 */
	public void setBunkatukaisu(int bunkatukaisu) {
		this.bunkatukaisu = bunkatukaisu;
	}

	/**
	 * todokesakikubunを取得します。
	 *
	 * @return todokesakikubun
	 */
	public int getTodokesakikubun() {
		return todokesakikubun;
	}

	/**
	 * todokesakikubunを設定します。
	 *
	 * @param todokesakikubun
	 *            todokesakikubun
	 */
	public void setTodokesakikubun(int todokesakikubun) {
		this.todokesakikubun = todokesakikubun;
	}

	/**
	 * todokesakipostを取得します。
	 *
	 * @return todokesakipost
	 */
	public String getTodokesakipost() {
		return todokesakipost;
	}

	/**
	 * todokesakipostを設定します。
	 *
	 * @param todokesakipost
	 *            todokesakipost
	 */
	public void setTodokesakipost(String todokesakipost) {
		this.todokesakipost = todokesakipost;
	}

	/**
	 * todokesakiadを取得します。
	 *
	 * @return todokesakiad
	 */
	public String getTodokesakiad() {
		return todokesakiad;
	}

	/**
	 * todokesakiadを設定します。
	 *
	 * @param todokesakiad
	 *            todokesakiad
	 */
	public void setTodokesakiad(String todokesakiad) {
		this.todokesakiad = todokesakiad;
	}

	/**
	 * todokesakitelを取得します。
	 *
	 * @return todokesakitel
	 */
	public String getTodokesakitel() {
		return todokesakitel;
	}

	/**
	 * todokesakitelを設定します。
	 *
	 * @param todokesakitel
	 *            todokesakitel
	 */
	public void setTodokesakitel(String todokesakitel) {
		this.todokesakitel = todokesakitel;
	}

	/**
	 * rusuを取得します。
	 *
	 * @return rusu
	 */
	public String getRusu() {
		return rusu;
	}

	/**
	 * rusuを設定します。
	 *
	 * @param rusu
	 *            rusu
	 */
	public void setRusu(String rusu) {
		this.rusu = rusu;
	}

	/**
	 * keiyakutennoを取得します。
	 *
	 * @return keiyakutenno
	 */
	public int getKeiyakutenno() {
		return keiyakutenno;
	}

	/**
	 * keiyakutennoを設定します。
	 *
	 * @param keiyakutenno
	 *            keiyakutenno
	 */
	public void setKeiyakutenno(int keiyakutenno) {
		this.keiyakutenno = keiyakutenno;
	}

	/**
	 * keiyakutenmeiを取得します。
	 *
	 * @return keiyakutenmei
	 */
	public String getKeiyakutenmei() {
		return keiyakutenmei;
	}

	/**
	 * keiyakutenmeiを設定します。
	 *
	 * @param keiyakutenmei
	 *            keiyakutenmei
	 */
	public void setKeiyakutenmei(String keiyakutenmei) {
		this.keiyakutenmei = keiyakutenmei;
	}

	/**
	 * haisosizeを取得します。
	 *
	 * @return haisosize
	 */
	public String getHaisosize() {
		return haisosize;
	}

	/**
	 * haisosizeを設定します。
	 *
	 * @param haisosize
	 *            haisosize
	 */
	public void setHaisosize(String haisosize) {
		this.haisosize = haisosize;
	}

	/**
	 * totalを取得します。
	 *
	 * @return total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * totalを設定します。
	 *
	 * @param total
	 *            total
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * chumonbiを取得します。
	 *
	 * @return chumonbi
	 */
	public Date getChumonbi() {
		return chumonbi;
	}

	/**
	 * chumonbiを設定します。
	 *
	 * @param chumonbi
	 *            chumonbi
	 */
	public void setChumonbi(Date chumonbi) {
		this.chumonbi = chumonbi;
	}

	/**
	 * memoを取得します。
	 *
	 * @return memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * memoを設定します。
	 *
	 * @param memo
	 *            memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * shokeiを取得します。
	 *
	 * @return shokei
	 */
	public int getShokei() {
		return shokei;
	}

	/**
	 * shokeiを設定します。
	 *
	 * @param shokei
	 *            shokei
	 */
	public void setShokei(int shokei) {
		this.shokei = shokei;
	}

	/**
	 * specialfeeを取得します。
	 *
	 * @return specialfee
	 */
	public int getSpecialfee() {
		return specialfee;
	}

	/**
	 * specialfeeを設定します。
	 *
	 * @param specialfee
	 *            specialfee
	 */
	public void setSpecialfee(int specialfee) {
		this.specialfee = specialfee;
	}

	/**
	 * kojostartを取得します。
	 *
	 * @return kojostart
	 */
	public String getKojostart() {
		return kojostart;
	}

	/**
	 * kojostartを設定します。
	 *
	 * @param kojostart
	 *            kojostart
	 */
	public void setKojostart(String kojostart) {
		this.kojostart = kojostart;
	}

	/**
	 * kaishanoを取得します。
	 *
	 * @return kaishano
	 */
	public int getKaishano() {
		return kaishano;
	}

	/**
	 * kaishanoを設定します。
	 *
	 * @param kaishano
	 *            kaishano
	 */
	public void setKaishano(int kaishano) {
		this.kaishano = kaishano;
	}

	/**
	 * shainnoを取得します。
	 *
	 * @return shainno
	 */
	public String getShainno() {
		return shainno;
	}

	/**
	 * shainnoを設定します。
	 *
	 * @param shainno
	 *            shainno
	 */
	public void setShainno(String shainno) {
		this.shainno = shainno;
	}

	/**
	 * shohincancelを取得します。
	 *
	 * @return shohincancel
	 */
	public int getShohincancel() {
		return shohincancel;
	}

	/**
	 * shohincancelを設定します。
	 *
	 * @param shohincancel
	 *            shohincancel
	 */
	public void setShohincancel(int shohincancel) {
		this.shohincancel = shohincancel;
	}

	/**
	 * hacchubiを取得します。
	 *
	 * @return hacchubi
	 */
	public Date getHacchubi() {
		return hacchubi;
	}

	/**
	 * hacchubiを設定します。
	 *
	 * @param hacchubi
	 *            hacchubi
	 */
	public void setHacchubi(Date hacchubi) {
		this.hacchubi = hacchubi;
	}

	/**
	 * taniを取得します。
	 *
	 * @return tani
	 */
	public String getTani() {
		return tani;
	}

	/**
	 * taniを設定します。
	 *
	 * @param tani
	 *            tani
	 */
	public void setTani(String tani) {
		this.tani = tani;
	}

	/**
	 * kanjiを取得します。
	 *
	 * @return kanji
	 */
	public String getKanji() {
		return kanji;
	}

	/**
	 * kanjiを設定します。
	 *
	 * @param kanji
	 *            kanji
	 */
	public void setKanji(String kanji) {
		this.kanji = kanji;
	}

	/**
	 * juchutantoushaを取得します。
	 *
	 * @return juchutantousha
	 */
	public String getJuchutantousha() {
		return juchutantousha;
	}

	/**
	 * juchutantoushaを設定します。
	 *
	 * @param juchutantousha
	 *            juchutantousha
	 */
	public void setJuchutantousha(String juchutantousha) {
		this.juchutantousha = juchutantousha;
	}

	/**
	 * hacchutantoushaを取得します。
	 *
	 * @return hacchutantousha
	 */
	public String getHacchutantousha() {
		return hacchutantousha;
	}

	/**
	 * hacchutantoushaを設定します。
	 *
	 * @param hacchutantousha
	 *            hacchutantousha
	 */
	public void setHacchutantousha(String hacchutantousha) {
		this.hacchutantousha = hacchutantousha;
	}

	/**
	 * shohinmeiを取得します。
	 *
	 * @return shohinmei
	 */
	public String getShohinmei() {
		return shohinmei;
	}

	/**
	 * shohinmeiを設定します。
	 *
	 * @param shohinmei
	 *            shohinmei
	 */
	public void setShohinmei(String shohinmei) {
		this.shohinmei = shohinmei;
	}

	/**
	 * telを取得します。
	 *
	 * @return tel
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * telを設定します。
	 *
	 * @param tel
	 *            tel
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * kaishatelを取得します。
	 *
	 * @return kaishatel
	 */
	public String getKaishatel() {
		return kaishatel;
	}

	/**
	 * kaishatelを設定します。
	 *
	 * @param kaishatel
	 *            kaishatel
	 */
	public void setKaishatel(String kaishatel) {
		this.kaishatel = kaishatel;
	}

	/**
	 * priceを取得します。
	 *
	 * @return price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * priceを設定します。
	 *
	 * @param price
	 *            price
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * shozokubushoを取得します。
	 *
	 * @return shozokubusho
	 */
	public String getShozokubusho() {
		return shozokubusho;
	}

	/**
	 * shozokubushoを設定します。
	 *
	 * @param shozokubusho
	 *            shozokubusho
	 */
	public void setShozokubusho(String shozokubusho) {
		this.shozokubusho = shozokubusho;
	}

	/**
	 * Todokesakimeiを取得します。
	 *
	 * @return Todokesakimei
	 */
	public String getTodokesakimei() {
		return Todokesakimei;
	}

	/**
	 * Todokesakimeiを設定します。
	 *
	 * @param Todokesakimei
	 *            Todokesakimei
	 */
	public void setTodokesakimei(String Todokesakimei) {
		this.Todokesakimei = Todokesakimei;
	}

	/**
	 * updateを取得します。
	 *
	 * @return update
	 */
	public Date getUpdate() {
		return update;
	}

	/**
	 * updateを設定します。
	 *
	 * @param update
	 *            update
	 */
	public void setUpdate(Date update) {
		this.update = update;
	}

	/**
	 * shuturyokuを取得します。
	 * @return shuturyoku
	 */
	public int getShuturyoku() {
	    return shuturyoku;
	}

	/**
	 * shuturyokuを設定します。
	 * @param shuturyoku shuturyoku
	 */
	public void setShuturyoku(int shuturyoku) {
	    this.shuturyoku = shuturyoku;
	}



}
