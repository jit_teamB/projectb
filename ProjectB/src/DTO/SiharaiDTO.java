package DTO;

import java.sql.Timestamp;

public class SiharaiDTO {

	private int shainno;
	private String katakana;
	private int yosingaku;
	private Timestamp update;
	private int mibarai;



	public int getShainno(){
	return shainno;
	}

	public void setShainno(int shainno) {
		this.shainno = shainno;
	}

	public String getKatakana() {
		return katakana;
	}

	public void setKatakana(String katakana) {
		this.katakana = katakana;
	}

	public int getYosingaku() {
		return yosingaku;
	}

	public void setYosingaku(int yosingaku) {
		this.yosingaku = yosingaku;
	}


	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}

	public int getMibarai() {
		return mibarai;
	}

	public void setMibarai(int mibarai) {
		this.mibarai = mibarai;
	}

}
