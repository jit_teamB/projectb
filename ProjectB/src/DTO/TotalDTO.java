package DTO;

import java.sql.Timestamp;

public class TotalDTO {

	private int juchuno;
	private int shohinno;
	private String gara;
	private String color;
	private String size;
	private int suryo;
	private int shokei;
	private Timestamp hassobi;
	private String cancel;
	private String tani;
	private int keiyakutenno;
	private Timestamp update;



	public int getJuchuno(){
	return juchuno;
	}

	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}

	public int getShohinno() {
		return shohinno;
	}

	public void setShohinno(int shohinno) {
		this.shohinno = shohinno;
	}

	public String getGara() {
		return gara;
	}

	public void setGara(String gara) {
		this.gara = gara;

	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getSuryo() {
		return suryo;
	}

	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}

	public int getShokei() {
		return shokei;
	}

	public void setShokei(int shokei) {
		this.shokei = shokei;
	}

	public Timestamp getHassobi() {
		return hassobi;
	}

	public void setHassobi(Timestamp hassobi) {
		this.hassobi = hassobi;

	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public int getKeiyakutenno() {
		return keiyakutenno;
	}

	public void setKeiyakutenno(int keiyakutenno) {
		this.keiyakutenno = keiyakutenno;
	}

	public String getTani() {
		return tani;
	}

	public void setTani(String tani) {
		this.tani = tani;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}
}
