package juchuno_seisei;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.CartDTO;
import ShohinNyuuryoku.CALCDTO;

/**
 * Servlet implementation class juchuno_seisei
 */
@WebServlet("/juchuno_seisei")
public class juchuno_seisei extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public juchuno_seisei() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session1=request.getSession();
		CartDTO data = juchuno_Dao.getjuchuno();
		session1.setAttribute("juchuno", data.getJuchuno());

		ArrayList<CALCDTO> shokika = new ArrayList<CALCDTO>();
		session1.setAttribute("sentakudata", shokika);

		int yosin=1000;
		int addsum=0;
		int shokei=0;
		session1.setAttribute("yosin", yosin);
		session1.setAttribute("addsum", addsum);
		session1.setAttribute("shokei", shokei);

		System.out.println(data.getJuchuno());
		System.out.println(session1.getAttribute("juchuno"));
        RequestDispatcher rd=request.getRequestDispatcher("Layout_jsp/B004S.jsp");
        rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF8");
		doGet(request,response);
	}

}
