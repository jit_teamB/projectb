package siharaihouhou;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import delete_cart.delete_DAO;

/**
 * Servlet implementation class siharaihouhou
 */
@WebServlet("/siharaihouhou")
public class siharaihouhou extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public siharaihouhou() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		HttpSession session3=request.getSession();
		int juchuno = (int) session3.getAttribute("juchuno");
		delete_DAO.delcart_siharai(juchuno);
		String siharaikubun = request.getParameter("shiharai");
		String bunkatukaisu = "1";

		/*******************確認用*********************/
//		juchuno = "1";
		/**********************************************/

		switch(siharaikubun){
		case "1":{
			siharaihouhou_Dao.siharaihouhou(juchuno, siharaikubun, bunkatukaisu);
			break;
		}
		case "2":{
			siharaihouhou_Dao.siharaihouhou(juchuno, siharaikubun, bunkatukaisu);
			break;
		}
		case "3":{
			bunkatukaisu = request.getParameter("bunkatu");
			siharaihouhou_Dao.siharaihouhou(juchuno, siharaikubun, bunkatukaisu);
			break;
		}
		}
        RequestDispatcher rd=request.getRequestDispatcher("/chumonuke_katsuda");
        rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF8");
		doGet(request,response);
	}

}
