package delete_cart;

//import goodToroku.ShohinDao;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ShohinNyuuryoku.CALCDTO;

/**
 * Servlet implementation class B004Servlet
 */
@WebServlet("/delshohin")
public class delshohin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public delshohin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 文字コード
		HttpSession session = request.getSession();
		ArrayList<CALCDTO> sentaku = (ArrayList<CALCDTO>) session.getAttribute("sentakudata");
		ArrayList<CALCDTO> saku = new ArrayList<CALCDTO>();
		int juchuno = (int) session.getAttribute("juchuno");
		int cnt=1;

		int yosin = (Integer) session.getAttribute("yosin");
		int mibarai = (Integer) session.getAttribute("mibarai");



		for(CALCDTO j : sentaku){
			String check = request.getParameter("sakujo"+cnt);
			System.out.println(check);
			if(check!=null){
				int shohinno = j.getShohinno();
				String color = j.getColor();
				String gara = j.getGara();
				String size = j.getSize();
				delete_DAO.delcart_shohin(juchuno,shohinno,color,gara,size);
			}
			else{
				CALCDTO shohin = new CALCDTO();
				shohin.setShohinno(j.getShohinno());
				shohin.setShohinmei(j.getShohinmei());
				shohin.setGara(j.getGara());
				shohin.setColor(j.getColor());
				shohin.setSize(j.getSize());
				shohin.setPrice(j.getPrice());
				shohin.setSuryo(j.getSuryo());
				saku.add(shohin);
			}
			cnt++;
		}

		cnt=1;
		int zeikomi=0;
		int zeinuki=0;
		int addsum =0;
		for(CALCDTO j : saku){
				System.out.println(j.getShohinmei()+"  "+j.getPrice());
				int kosu = j.getSuryo();
				zeikomi = (int) (zeikomi + (j.getPrice()*kosu*1.08));
				zeinuki = (int) (zeinuki + (j.getPrice()*kosu));
				cnt++;
		}
		System.out.println(mibarai);

		addsum=mibarai+zeikomi;
		System.out.println(yosin + "   " + addsum);
		session.setAttribute("shokei", zeikomi);
		session.setAttribute("addsum", addsum);
		session.setAttribute("sentakudata", saku);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B006.jsp");//アクセス先のJSP
		rd.forward(request, response);
	}
}
