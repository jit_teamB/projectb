package delete_cart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class delete_DAO {
	public static  void delcart_shohin(int juchuno,int shohinno,String color,String gara, String size){
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			System.out.println("接続成功");
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "DELETE FROM cart WHERE nyuryoku = 1 "
														+ "AND juchuno = '" + juchuno
														+ "'AND shohinno = '" + shohinno
														+ "' AND color = '" + color
														+ "' AND gara = '" + gara
														+ "' AND size = '" + size + "'";
			//定義したSQLを実行して、実行結果を変数resultに代入
			System.out.println(sql);
			startment.executeUpdate(sql);


			// 変数resultの内容を順次出力

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
		return;
	}
	public static  void delcart_todokesaki(int juchuno){
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			System.out.println("接続成功");
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "DELETE FROM cart WHERE nyuryoku = 2";

			//定義したSQLを実行して、実行結果を変数resultに代入
			System.out.println(sql);
			startment.executeUpdate(sql);


			// 変数resultの内容を順次出力

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
		return;
	}
	public static  void delcart_siharai(int juchuno){
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			System.out.println("接続成功");
			startment = con.createStatement();

			// 実行するSQLを定義
			String sql = "DELETE FROM cart WHERE nyuryoku = 3 AND juchuno = '"+juchuno+"'";

			//定義したSQLを実行して、実行結果を変数resultに代入
			System.out.println(sql);
			startment.executeUpdate(sql);


			// 変数resultの内容を順次出力

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
		return;
	}
		public static  void delcart_all(int juchuno){
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			// Class.forName("org.gjt.mm.mysql.Driver");
			Connection con = null;
			Statement startment = null;

			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				System.out.println("接続成功");
				startment = con.createStatement();

				// 実行するSQLを定義
				String sql = "DELETE FROM cart where juchuno='"+juchuno+"'";

				//定義したSQLを実行して、実行結果を変数resultに代入
				System.out.println(sql);
				startment.executeUpdate(sql);


				// 変数resultの内容を順次出力

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
			return;
	}
}

