package keiyakuToroku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.KeiyakutenDTO;

/**
 * Servlet implementation class B040
 */
@WebServlet("/B040")
public class B040 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B040() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字コード
		request.setCharacterEncoding("utf8");

		//htmlから
        try{
        	String keiyakuname = request.getParameter("keiyakuname");
        	String keiyakukana = request.getParameter("keiyakukana");
        	int keiyakutel = Integer.parseInt(request.getParameter("keiyakutel"));
        	int keiyakufax = Integer.parseInt(request.getParameter("keiyakufax"));
        	String keiyakupost = request.getParameter("keiyakupost");
        	String keiyakuadd = request.getParameter("keiyakuadd");
        	String keiyakutanto = request.getParameter("keiyakutanto");

        	ArrayList<KeiyakutenDTO> keiyakunoList = keiyakuDao.getSearchkeiyakuno();
            KeiyakutenDTO keiyaku = new KeiyakutenDTO();

            int keiyakuno = (keiyaku.getKeiyakutenno())+1;
        	keiyakuDao.getInsertkeiyaku(keiyakuno,keiyakuname,keiyakukana,keiyakutel,keiyakufax,keiyakupost,keiyakuadd,keiyakutanto);//htmlから取得した入力項目を入れる

            ArrayList<KeiyakutenDTO> keiyakuList = keiyakuDao.getSearchkeiyaku(keiyakuno);

            request.setAttribute("keiyakuList", keiyakuList);
			RequestDispatcher rd = request.getRequestDispatcher("../B042.jsp");//アクセス先のJSP
			rd.forward(request, response);

        }catch(Exception e){

        }finally{

        }
	}

}
