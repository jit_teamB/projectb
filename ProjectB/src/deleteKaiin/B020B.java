package deleteKaiin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KaiinDTO;
import Login.KaiinDAO;

/**
 * Servlet implementation class B020B
 */
@WebServlet("/B020B")
public class B020B extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B020B() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String kaishamei=request.getParameter("kaisyamei");
        String shainno=request.getParameter("syainno");
        String katakana=request.getParameter("katakana");

        System.out.println(kaishamei);

		 HttpSession session1=request.getSession();
		 HttpSession session2=request.getSession();


        ArrayList<KaiinDTO> kaiin = KaiinDAO.getKaiinList(kaishamei, shainno);
        for(KaiinDTO i : kaiin){
       	 int kaishano=i.getKaishano();
       	 System.out.println(kaishano);
       	 request.setAttribute("member", kaiin);
            RequestDispatcher rd=request.getRequestDispatcher("Layout_jsp/B020B.jsp");
            rd.forward(request,response);
            session1.setAttribute("shainno",shainno);
            session2.setAttribute("kaishano",kaishano);
        }
        if(kaiin.size()==0){
       	 response.sendRedirect("Layout_jsp/B024.jsp");//アクセス先のJSP
        }


	}

}
