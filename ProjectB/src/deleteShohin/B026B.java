package deleteShohin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class B026B
 */
@WebServlet("/B026B")
public class B026B extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B026B() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");

		//htmlから取得
	try{
		int shohin = Integer.parseInt(request.getParameter("shohinno"));

		//削除DAOへ
		deleteshohinDao.getdeleteshohin(shohin);
		request.setAttribute("shohinuList",shohin);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B030.jsp");//アクセス先のJSP
		rd.forward(request, response);

	   }catch(Exception e){

	    }finally{

	    	}
		}
	}
