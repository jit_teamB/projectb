package deleteShohin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.shohinDTO;

/**
 * Servlet implementation class B004B
 */
/**
 * @author Masuko
 *
 */
@WebServlet("/B004B")
public class B004B extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B004B() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
		//DBから取得
	try{
		int shohinno = Integer.parseInt(request.getParameter("shohinno"));//htmlから取得

		//ArrayListに
		ArrayList<shohinDTO> shoihnList = SearchshohinDao.getSearchshohin(shohinno);//

		//requestに保存してjspで取得データ使えるように
		request.setAttribute("shoihnList", shoihnList);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B026B.jsp");//アクセス先のJSP
		rd.forward(request, response);

    }catch(Exception e){

    }finally{

    	}
	}
}
