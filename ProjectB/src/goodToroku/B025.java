package goodToroku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.shohinDTO;

/**
 * Servlet implementation class B025
 */
@WebServlet("/B025")
public class B025 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B025() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
		// htmlから
		try{
			int shohinno = Integer.parseInt(request.getParameter("shohinno"));
		    String shohinmei = request.getParameter("shohinmei");
		    String gara = request.getParameter("shohingara");
		    String color = request.getParameter("shohincolor");
		    String size = request.getParameter("shohinsize");
		    String startday = request.getParameter("startday");//日付
		    String endday = request.getParameter("endday");//日付
		    int keiyakutenno = Integer.parseInt(request.getParameter("keiyakuno"));
		    int price = Integer.parseInt(request.getParameter("price"));
		    int besso = Integer.parseInt(request.getParameter("besso"));
		    ShohinDao.getInsertshohin(shohinno,shohinmei,gara,color,size,startday,endday,keiyakutenno,price,besso);

		    ArrayList<shohinDTO> shohinList = ShohinDao.getSearchshohin(shohinno);

		    request.setAttribute("shohinList", shohinList);
			RequestDispatcher rd = request.getRequestDispatcher("../B027.jsp");//アクセス先のJSP
			rd.forward(request, response);




		}catch(Exception e){

		}finally{

		}

	}

}
