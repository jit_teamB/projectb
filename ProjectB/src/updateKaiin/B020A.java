package updateKaiin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KaiinDTO;

/**
 * Servlet implementation class B020
 */
@WebServlet("/B020A")
public class B020A extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B020A() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字コード
		request.setCharacterEncoding("utf8");
		//jspから呼び出す
		String kanji = request.getParameter("kanji");
		String katakana = request.getParameter("katakana");
		String tel = request.getParameter("tel");
		String fax = request.getParameter("fax");
		String post = request.getParameter("post");
		String address = request.getParameter("address");
		String kaishamei = request.getParameter("kaishamei");
		String busho = request.getParameter("busho");
		String memo = request.getParameter("memo");

		HttpSession session = request.getSession();
		String shainno = (String) session.getAttribute("shainno");

		updateKaiinDao.getUpdatekaiin(shainno,kanji,katakana,tel,fax,post,address,busho,memo);

		ArrayList<KaiinDTO> kaiinList = updateKaiinDao.getSeachkaiin(kaishamei,shainno);

        request.setAttribute("kaiinList", kaiinList);
        RequestDispatcher rd = request.getRequestDispatcher("B023.jsp");
        rd.forward(request, response);

	}

}
