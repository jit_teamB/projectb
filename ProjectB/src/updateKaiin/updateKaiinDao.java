package updateKaiin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KaiinDTO;

public class updateKaiinDao {
	//会員編集
			public static void getUpdatekaiin(String shainno, String kanji, String katakana, String tel, String fax, String post,
					String address, String busho, String memo){
				// 接続情報を定義
				String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";
				Connection con = null;
				Statement startment = null;

				try {
					// MySQLとの接続を開始
					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					startment = con.createStatement();
					//System.out.println("接続成功");

					// 実行するSQLを定義
					String sql = "UPDATE tkaiin SET shainno ='"+shainno+"',kanji ='"+kanji+"',katakana='"+katakana+"' "
							+ ",tel='"+tel+"' ,fax='"+fax+"' ,postcode='"+post+"' ,address='"+address+"' ,busho='"+busho+"' "
									+ ",memo='"+memo+"' where shainno='"+shainno+"'";//DBにあわせて 商品番号が一致する行の編集

					// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
					int count = startment.executeUpdate(sql);

					if (count!= 1) {
						System.out.println("更新した件数:" + count);
					} else {
						System.out.println("更新した件数:" + count);
					}

					// エラー情報を取得
				} catch (ClassNotFoundException conf) {
					System.out.println(conf);

				} catch (SQLException sqep) {
					try{
						con.rollback();
					}catch(SQLException sqep2){
						System.out.println(sqep2);
					}
					System.out.println(sqep);
				} finally {
					if (startment != null) {
						try {
							startment.close();
						} catch (SQLException e) {
						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {

						}
					}
				}
			}
			public static ArrayList<KaiinDTO> getSeachkaiin(String kaishamei,String shainno) {

				String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";

				Statement stmt = null;
				Connection con = null;
				KaiinDTO record = new KaiinDTO();
				ArrayList<KaiinDTO> kaiinList = new ArrayList<KaiinDTO>();

				try {

					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					Statement startment = con.createStatement();

					String sql = "SELECT shainno,katakana,kanji,tel,fax,address,tkaiin.postcode,tkyosankigyou.kaishamei,shozokubusho,memo,nyukaidate "
							+ "FROM tkaiin,tkyosankigyou WHERE tkyosankigyou.kaishamei Like '%"+kaishamei+"%' and tkaiin.shainno='"+shainno+"'";
		            System.out.println(sql);
					ResultSet result = startment.executeQuery(sql);

					while(result.next()) {
						record.setShainno(result.getString("shainno"));
						record.setKanji(result.getString("kanji"));
						record.setKatakana(result.getString("katakana"));
						record.setPostcode(result.getString("postcode"));
						record.setAddress(result.getString("address"));
						record.setDenwano(result.getString("tel"));
						record.setFaxno(result.getString("fax"));
						record.setNyukaidate(result.getTimestamp("nyukaidate"));
						record.setMemo(result.getString("memo"));
						record.setAddress(result.getString("address"));
						record.setKaishamei(result.getString("kaishamei"));
						record.setShozokubusho(result.getString("shozokubusho"));
						kaiinList.add(record);

					}
				} catch (ClassNotFoundException e) {
					System.out.println("ドライバのロードに失敗しました。");
				} catch (Exception e) {
					System.out.println("例外発生:" + e);

				} finally {
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException e) {
						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {
						}
					}

				}
				return kaiinList;

			}
}
