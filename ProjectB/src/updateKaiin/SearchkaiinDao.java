package updateKaiin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KaiinDTO;

public class SearchkaiinDao {
	public static ArrayList<KaiinDTO> getKaiinList(String kaishamei, String shainno) {

		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		KaiinDTO record = new KaiinDTO();
		ArrayList<KaiinDTO> kaiinList = new ArrayList<KaiinDTO>();

		try {

			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();

			String sql = "SELECT shainno,katakana,kanji,tel,fax,address,tkaiin.postcode,tkyosankigyou.kaishamei,shozokubusho,memo,nyukaidate "
					+ "FROM tkaiin,tkyosankigyou WHERE tkyosankigyou.kaishamei Like '%"+kaishamei+"%' and tkaiin.shainno='"+shainno+"'";
            System.out.println(sql);
			ResultSet result = startment.executeQuery(sql);

			while(result.next()) {
				record.setShainno(result.getString("shainno"));
				record.setKanji(result.getString("kanji"));
				record.setKatakana(result.getString("katakana"));
				record.setPostcode(result.getString("postcode"));
				record.setAddress(result.getString("address"));
				record.setDenwano(result.getString("tel"));
				record.setFaxno(result.getString("fax"));
				record.setNyukaidate(result.getTimestamp("nyukaidate"));
				record.setMemo(result.getString("memo"));
				record.setAddress(result.getString("address"));
				record.setKaishamei(result.getString("kaishamei"));
				record.setShozokubusho(result.getString("shozokubusho"));
				kaiinList.add(record);

			}
		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return kaiinList;

	}

}
