package Chumonuketuke;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.ChumonuketukehyoDTO;

/**
 * Servlet implementation class chumonuke_katsuda
 */
@WebServlet("/Hakkou")
public class Hakkou extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Hakkou() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		 HttpSession session1=request.getSession();
		 HttpSession session2=request.getSession();
		 HttpSession session3=request.getSession();
		// HttpSession sessionArray1=request.getSession();
		 //HttpSession sessionArray2=request.getSession();


		 int juchuno=(Integer)session2.getAttribute("juchuno");
		 String shainno=(String)session3.getAttribute("shainno");
		 String katakana=(String)session1.getAttribute("katakana");
		 int shokei=(Integer)session1.getAttribute("shokei");
		 System.out.println(juchuno);
		 System.out.println(shainno);
		 System.out.println(katakana);


		 int shohinflag=0;
		 int todokesakiflag=0;
		 int siharaiflag=0;



		    ArrayList<ChumonuketukehyoDTO> ShohinList = (ArrayList<ChumonuketukehyoDTO>) session1.getAttribute("ShohinArray");
			ArrayList<ChumonuketukehyoDTO> KaiinList = (ArrayList<ChumonuketukehyoDTO>) session2.getAttribute("KaiinArray");


		 for(ChumonuketukehyoDTO i : ShohinList){

			ChumonuketukehyoDAO.InsertJuchu(i.getJuchuno(),i.getSiharaikubun(),i.getBunkatukaisu(),i.getTodokesakikubun(),i.getTodokesakipost(),i.getTotal(),i.getUpdate(),i.getTodokesakiad() ,
						i.getJuchutantousha(),i.getTodokesakitel(),i.getRusu(),i.getShokei(),i.getSpecialfee(),i.getKaishano(),shainno,i.getTodokesakimei(),i.getShohinno(),i.getGara(),i.getColor(),i.getSize(),
						i.getSuryo(),i.getTani(),i.getKeiyakutenno(),i.getHaisosize(),i.getShohincancel());
         }


			 ChumonuketukehyoDAO.updatemibarai(shainno,katakana,shokei);

		 RequestDispatcher rd=request.getRequestDispatcher("Layout_jsp/B015.jsp");
         rd.forward(request,response);

		 //TODO Auto-generated method stub
	}

	}

