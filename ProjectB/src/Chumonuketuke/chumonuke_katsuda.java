package Chumonuketuke;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.ChumonuketukehyoDTO;

/**
 * Servlet implementation class chumonuke_katsuda
 */
@WebServlet("/chumonuke_katsuda")
public class chumonuke_katsuda extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public chumonuke_katsuda() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session1 = request.getSession();
		HttpSession session2 = request.getSession();
		HttpSession session3 = request.getSession();
		HttpSession session4 = request.getSession();
		HttpSession session5 = request.getSession();
		HttpSession session6 = request.getSession();
		HttpSession sessionArray1 = request.getSession();
		HttpSession sessionArray2 = request.getSession();

		int shohinflag = 0;
		int todokesakiflag = 0;
		int siharaiflag = 0;

		int juchuno = (Integer) session1.getAttribute("juchuno");
		String shainno = (String) session2.getAttribute("shainno");
		String kaishamei = (String) session3.getAttribute("kaishamei");

		System.out.println(juchuno);
		session1.setAttribute("juchuno", juchuno);
		ArrayList<ChumonuketukehyoDTO> Chumon1 = ChumonuketukehyoDAO
				.getChumonuketukehyo_juchu(juchuno);
		ArrayList<ChumonuketukehyoDTO> Chumon2 = ChumonuketukehyoDAO
				.getChumonuketukehyo_juchu2(shainno, kaishamei);
		sessionArray1.setAttribute("ShohinArray", Chumon1);
		sessionArray2.setAttribute("KaiinArray", Chumon2);

		for (ChumonuketukehyoDTO i : Chumon1) {
			if (i.getShohinno()!= 0) {
				shohinflag = 1;
			}
		}
		session4.setAttribute("shohinflag", shohinflag);

		for (ChumonuketukehyoDTO i : Chumon1) {
			if (i.getTodokesakikubun() != 0  && !i.getTodokesakiad().equals("") && !i.getTodokesakimei().equals("") && !i.getTodokesakipost().equals("")&& !i.getTodokesakitel().equals("")) {
				System.out.println(i.getTodokesakikubun()+"----"+i.getTodokesakiad());
				todokesakiflag = 1;
			}
		}
		session5.setAttribute("todokesakiflag", todokesakiflag);

		for (ChumonuketukehyoDTO i : Chumon1) {
			if (i.getSiharaikubun() != 0) {
				siharaiflag = 1;
			}
		}
		session6.setAttribute("siharaiflag", siharaiflag);

		RequestDispatcher rd = request
				.getRequestDispatcher("Layout_jsp/B009.jsp");
		rd.forward(request, response);

		// TODO Auto-generated method stub
	}

}
