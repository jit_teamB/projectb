package Chumonuketuke;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.ChumonuketukehyoDTO;

public class ChumonuketukehyoDAO {

	// 商品追加
	public static ArrayList<ChumonuketukehyoDTO> getChumonuketukehyo_juchu(int juchuno) {
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;
		ArrayList<ChumonuketukehyoDTO> comList = new ArrayList<ChumonuketukehyoDTO>();
		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");
			// 実行するSQLを定義
			String sql1 = "select cart.juchuno, cart.shohinno, tshohin.shohinmei, cart.gara, cart.color, cart.size, cart.suryo, cart.siharaikubun, cart.bunkatukaisu, cart.todokesakikubun, cart.todokesakiad, cart.keiyakutenno, cart.haisosize, cart.todokesakipost, cart.update, cart.rusu, cart.todokesakimei, cart.todokesakitel, cart.shuturyoku, tshohin.price, tshohin.tani, tkeiyakuten.keiyakutenmei, cart.nyuryoku FROM cart, tshohin, tkeiyakuten WHERE (cart.shohinno = tshohin.shohinno AND cart.gara = tshohin.gara AND cart.color = tshohin.color AND cart.size = tshohin.size AND cart.juchuno = '"+juchuno+"' AND cart.keiyakutenno = tkeiyakuten.keiyakutenno AND cart.nyuryoku = 1 ) OR (cart.nyuryoku = 2 AND cart.juchuno = '"+juchuno+"') OR (cart.nyuryoku = 3 AND cart.juchuno = '"+juchuno+"') group by cart.shohinno, cart.gara, cart.color, cart.size, cart.siharaikubun order by keiyakutenno";
			//String sql2 ="select tkaiin.shainno,tkaiin.kanji, tkaiin.tel,tkaiin.shozokubusho,tkyosankigyou.kaishamei,tkyosankigyou.kaishano, tkyosankigyou.kaishatel FROM tkaiin,tkyosankigyou where tkaiin.kaishano=tkyosankigyou.kaishano and shainno='"+shainno+"'";
			// 定義したSQLを実行して、実行結果を変数resultに代入
			System.out.println(sql1);
			ResultSet result = startment.executeQuery(sql1);
			// 変数resultの内容を順次出力
			while (result.next()) {
				ChumonuketukehyoDTO com = new ChumonuketukehyoDTO();
				com.setJuchuno(result.getInt("juchuno"));
				com.setGara(result.getString("gara"));
				com.setColor(result.getString("color"));
				com.setSize(result.getString("size"));
				com.setTodokesakikubun(result.getInt("todokesakikubun"));
				com.setTodokesakipost(result.getString("todokesakipost"));
				com.setTodokesakiad(result.getString("todokesakiad"));
				com.setTodokesakitel(result.getString("todokesakitel"));
				com.setHaisosize(result.getString("haisosize"));
				com.setTani(result.getString("tani"));
				com.setShohinmei(result.getString("shohinmei"));
				com.setTodokesakimei(result.getString("todokesakimei"));
				com.setShohinno(result.getInt("shohinno"));
				com.setSuryo(result.getInt("suryo"));
				com.setSiharaikubun(result.getInt("siharaikubun"));
				com.setBunkatukaisu(result.getInt("bunkatukaisu"));
				com.setKeiyakutenno(result.getInt("keiyakutenno"));
				com.setKeiyakutenmei(result.getString("keiyakutenmei"));
				com.setPrice(result.getInt("price"));
				com.setShuturyoku(result.getInt("shuturyoku"));
				com.setRusu(result.getString("rusu"));
				com.setUpdate(result.getDate("update"));
				comList.add(com);// 配列に追加
			}
			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");
			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);
		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return comList;
	}






	// 商品追加
	public static ArrayList<ChumonuketukehyoDTO> getChumonuketukehyo_juchu2(String shainno,String kaishamei) {
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;
		ArrayList<ChumonuketukehyoDTO> comList = new ArrayList<ChumonuketukehyoDTO>();
		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");
			// 実行するSQLを定義
			String sql2 ="select tkaiin.shainno,tkaiin.kanji, tkaiin.tel,tkaiin.shozokubusho,tkyosankigyou.kaishamei,tkyosankigyou.kaishano, tkyosankigyou.kaishatel FROM tkaiin,tkyosankigyou WHERE tkyosankigyou.kaishamei Like '%"+kaishamei+"%' and tkaiin.shainno='"+shainno+"'and tkyosankigyou.kaishano=tkaiin.kaishano";
			// 定義したSQLを実行して、実行結果を変数resultに代入
			System.out.println(shainno);
			System.out.println(sql2);
			ResultSet result = startment.executeQuery(sql2);
			// 変数resultの内容を順次出力
			while (result.next()) {
				ChumonuketukehyoDTO com = new ChumonuketukehyoDTO();
				com.setKanji(result.getString("kanji"));
				com.setShozokubusho(result.getString("shozokubusho"));
				com.setKaishano(result.getInt("kaishano"));
				com.setShainno(result.getString("shainno"));
				com.setKaishamei(result.getString("kaishamei"));
				com.setTel(result.getString("tel"));
				com.setKaishatel(result.getString("kaishatel"));
				comList.add(com);// 配列に追加

			}
			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");
			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);
		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return comList;
	}





	public static void InsertJuchu(int juchuno,int siharaikubun,int bunkatukaisu,int todokesakikubun,String todokesakipost,int total,Date chumonbi,String todokesakiad,
			String juchutantousha,String todokesakitel,String rusu,int shokei,int specialfee,int kaishano,String shainno,String todokesakimei,int shohinno,String gara,String color,
			String size,int suryo,String tani,int keiyakutenno,String haisosize,int shohincancel){//受注情報登録
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");

			// 実行するSQLを定義
			String sql = "INSERT INTO total (juchuno,shohinno,gara,color,size,suryo,shokei,shohincancel,tani,keiyakutenno,haisosize) VALUES ('"+juchuno+"','"+shohinno+"','"+gara+"','"+color+"','"+size+"','"+suryo+"','"+shokei+"','"+shohincancel+"','"+tani+"','"+keiyakutenno+"','"+haisosize+"')";
			String sql1 = "INSERT INTO tjuchu (juchuno,siharaikubun,bunkatukaisu,todokesakikubun,todokesakipost,total,chumonbi,todokesakiad,todokesakitel,shokei,kaishano,shainno,todokesakimei) VALUES ('"+juchuno+"','"+siharaikubun+"','"+bunkatukaisu+"','"+todokesakikubun+"','"+todokesakipost+"','"+total+"','"+chumonbi+"','"+todokesakiad+"','"+todokesakitel+"','"+shokei+"','"+kaishano+"','"+shainno+"','"+todokesakimei+"')";

			System.out.println(sql);
			System.out.println(sql1);


			//DBのカラムにあわせて書く


			// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
			int count = startment.executeUpdate(sql);
			int count1=startment.executeUpdate(sql1);

			if (count!=1) {
				System.out.println("更新した件数:" + count);
			} else {
				System.out.println("更新した件数:" + count);
			}

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
	}




	public static void delete(int juchuno){//受注情報登録
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");

			// 実行するSQLを定義
			String sql = "DELETE FROM cart WHERE juchuno = '"+ juchuno+ "'";
			System.out.println(sql);


			//DBのカラムにあわせて書く


			// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
			int count = startment.executeUpdate(sql);

			if (count!=1) {
				System.out.println("更新した件数:" + count);
			} else {
				System.out.println("更新した件数:" + count);
			}

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
	}



	public static void updatemibarai(String shainno,String katakana,int shokei){//未払い額更新
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		// Class.forName("org.gjt.mm.mysql.Driver");
		Connection con = null;
		Statement startment = null;

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");

			// 実行するSQLを定義
			String sql = "UPDATE `tshiharai` SET `mibarai`='"+shokei+"' WHERE shainno = '"+shainno+"' and katakana = '"+katakana+"'";;
			System.out.println(sql);


			//DBのカラムにあわせて書く


			// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
			int count = startment.executeUpdate(sql);

			if (count!=1) {
				System.out.println("更新した件数:" + count);
			} else {
				System.out.println("更新した件数:" + count);
			}

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
	}



}
