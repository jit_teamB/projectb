package deleteKyosan;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class B034B
 */
@WebServlet("/B034B")
public class B034B extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B034B() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
		//htmlから取得
	try{
		int kyosanno = Integer.parseInt(request.getParameter(""));


		//削除DAOへ
		deletekyosanDao.getdeletekyosan(kyosanno);

		request.setAttribute("kyosankigyouList",kyosanno);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B039.jsp");//アクセス先のJSP
		rd.forward(request, response);

	   }catch(Exception e){

	    }finally{

	    	}
		}
	}
