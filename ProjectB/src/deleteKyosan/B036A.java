package deleteKyosan;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import updateKyosan.SearchkyosanDao;
import DTO.KyosankigyouDTO;

/**
 * Servlet implementation class B036A
 */
@WebServlet("/B036A")
public class B036A extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B036A() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
		//DBから取得
	try{
		int kyosanno = Integer.parseInt(request.getParameter("kyosangigyono"));//htmlから取得

		SearchkyosanDao.getSearchkyosan(kyosanno);

		//ArrayListに
		ArrayList<KyosankigyouDTO> kyosanList = SearchkyosanDao.getSearchkyosan(kyosanno);//

		//requestに保存してjspで取得データ使えるように
		request.setAttribute("kyosanList", kyosanList);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B034B.jsp");//アクセス先のJSP
		rd.forward(request, response);
    }catch(Exception e){

    }finally{

    	}
	}
}
