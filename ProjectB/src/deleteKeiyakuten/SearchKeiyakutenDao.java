package deleteKeiyakuten;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KeiyakutenDTO;

public class SearchKeiyakutenDao {
	//契約店検索
	public static ArrayList<KeiyakutenDTO> getSearchKeiyakuten(int keiyakutenno) {
		// 接続情報を定義
		String url = "jdbc:mysql://localhost:3306/bbs?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";
		Connection con = null;
		Statement startment = null;

		ArrayList<KeiyakutenDTO> keiyakutenList = new ArrayList<KeiyakutenDTO>();

		try {
			// MySQLとの接続を開始
			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			startment = con.createStatement();
			System.out.println("接続成功");

			// 実行するSQLを定義
			String sql = "SELECT * FROM tkeiyakuten where keiyakutenno = " + keiyakutenno;

			// 定義したSQLを実行して、実行結果を変数resultに代入
			ResultSet result = startment.executeQuery(sql);

			// 変数resultの内容を順次出力
			while (result.next()) {//DBが出来てから DTOの名前!!!　tableの名前!!
				KeiyakutenDTO keiyakuten = new KeiyakutenDTO();
				keiyakuten.setKeiyakutenno(result.getInt("keiyakutenno"));//契約店番号
				keiyakuten.setKeiyakutenmei(result.getString("keiyakutenmei"));//契約店名
				keiyakuten.setKeiyakutenkana(result.getString("keiyakutenkana"));//契約店名(カナ)
				keiyakuten.setKeiyakutenpost(result.getString("keiyakutenpost"));//契約店
				keiyakuten.setKeiyakutenad(result.getString("keiyakutenad"));//契約店郵便番号
				keiyakuten.setKeiyakutendenwano(result.getString("keiyakutentel"));//契約店住所
				keiyakuten.setKeiyakutenfaxno(result.getString("keiyakutenfax"));//契約店電話番号
				keiyakuten.setKeiyakutentanto(result.getString("keiyakutentanto"));//契約店FAX


				keiyakutenList.add(keiyakuten);// 配列に追加
			}

			// MySQLとの接続を終了
			con.close();
			System.out.println("接続終了");

			// エラー情報を取得
		} catch (ClassNotFoundException conf) {
			System.out.println(conf);

		} catch (SQLException sqep) {
			System.out.println(sqep);
		} finally {
			if (startment != null) {
				try {
					startment.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

				}
			}
		}
		return keiyakutenList;
	}
}


