package deleteKeiyakuten;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import updateKeiyaku.SearchkeiyakuDao;
import DTO.KeiyakutenDTO;

/**
 * Servlet implementation class B043A
 */
@WebServlet("/B043A")
public class B043A extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B043A() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");

		//DBから取得
	try{
		int keiyakutenno = Integer.parseInt(request.getParameter("keiyakutenno"));//htmlから取得
		//ArrayListに

		SearchKeiyakutenDao.getSearchKeiyakuten(keiyakutenno);

		ArrayList<KeiyakutenDTO> keiyakutenList = SearchkeiyakuDao.getSearchkeiyaku(keiyakutenno);//

		//requestに保存してjspで取得データ使えるように
		request.setAttribute("keiyakutenList",keiyakutenList);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B041B.jsp");//アクセス先のJSP
		rd.forward(request, response);
    }catch(Exception e){

    }finally{

    	}
	}
}
