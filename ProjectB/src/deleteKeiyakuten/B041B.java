package deleteKeiyakuten;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class B041B
 */
@WebServlet("/B041B")
public class B041B extends HttpServlet {


    /**
     * @see HttpServlet#HttpServlet()
     */
    public B041B() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コード
		request.setCharacterEncoding("utf8");
	try{
		//htmlから取得
		int Keiyakutenno = Integer.parseInt(request.getParameter(""));

		//削除DAOへ
		deleteKeiyakutenDao.getdeletekeiyakuten(Keiyakutenno);

		request.setAttribute("keiyakutenList",Keiyakutenno);
		RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B046.jsp");//アクセス先のJSP
		rd.forward(request, response);

	   }catch(Exception e){

	    }finally{

	    	}
		}
	}
