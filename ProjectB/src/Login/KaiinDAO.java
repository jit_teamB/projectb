package Login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KaiinDTO;

public class KaiinDAO {

	public static ArrayList<KaiinDTO> getKaiinList(String kaishamei, String shainno) {

		String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "";

		Statement stmt = null;
		Connection con = null;
		KaiinDTO record = new KaiinDTO();
		ArrayList<KaiinDTO> data = new ArrayList<KaiinDTO>();
		System.out.println(kaishamei);
if(kaishamei.length()<2){
	kaishamei="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";
}
		try {

			Class.forName("org.gjt.mm.mysql.Driver");
			con = DriverManager.getConnection(url, user, password);
			Statement startment = con.createStatement();
			String shamei = kaishamei;
			String shainum = shainno;


			// 値を埋め込む前の形のSQL文をコンパイルし、構文を確定
			PreparedStatement st = con.prepareStatement("SELECT shainno,tkaiin.kaishano,katakana,kanji,tel,fax,address,tkaiin.postcode,tkyosankigyou.kaishamei,shozokubusho,memo,nyukaidate "
					+ "FROM tkaiin,tkyosankigyou WHERE tkyosankigyou.kaishamei Like ? and tkaiin.shainno=? and tkyosankigyou.kaishano=tkaiin.kaishano");
			st.setString(1, '%'+shamei+'%'); 	// ? の場所に値を埋め込む
			st.setString(2, shainum); 	// ? の場所に値を埋め込む

			// クエリの実行
			ResultSet result = st.executeQuery();
//
//			String sql = "SELECT shainno,tkaiin.kaishano,katakana,kanji,tel,fax,address,tkaiin.postcode,tkyosankigyou.kaishamei,shozokubusho,memo,nyukaidate "
//					+ "FROM tkaiin,tkyosankigyou WHERE tkyosankigyou.kaishamei Like '%"+kaishamei+"%' and tkaiin.shainno='"+shainno+"'and tkyosankigyou.kaishano=tkaiin.kaishano";
//            System.out.println(sql);
//			ResultSet result = startment.executeQuery(sql);

			if (result.next()) {
				record.setShainno(result.getString("shainno"));
				record.setKaishano(result.getInt("kaishano"));
				record.setKanji(result.getString("kanji"));
				record.setKatakana(result.getString("katakana"));
				record.setPostcode(result.getString("postcode"));
				record.setAddress(result.getString("address"));
				record.setDenwano(result.getString("tel"));
				record.setFaxno(result.getString("fax"));
				record.setNyukaidate(result.getTimestamp("nyukaidate"));
				record.setMemo(result.getString("memo"));
				record.setAddress(result.getString("address"));
				record.setKaishamei(result.getString("kaishamei"));
				record.setShozokubusho(result.getString("shozokubusho"));
				data.add(record);

			}
		} catch (ClassNotFoundException e) {
			System.out.println("ドライバのロードに失敗しました。");
		} catch (Exception e) {
			System.out.println("例外発生:" + e);

		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}

		}
		return data;

	}

}
