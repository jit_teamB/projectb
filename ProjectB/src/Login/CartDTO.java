package Login;

import java.sql.Timestamp;

public class CartDTO {

	private int juchuno;
	private int shohinno;
	private String gara;
	private String color;
	private String size;
	private int suryo;
	private int siharaikubun;
	private int payment;
	private String todokesakikubun;
	private String todokesakiad;
	private int keiyakutenno;
	private int besso;
	private String todokesakipost;
	private Timestamp update;



	private int getJuchuno(){
	return juchuno;
	}

	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}

	public int getShohinno() {
		return shohinno;
	}

	public void setShohinno(int shohinno) {
		this.shohinno = shohinno;
	}

	public String getGara() {
		return gara;
	}

	public void setGara(String gara) {
		this.gara = gara;

	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getSuryo() {
		return suryo;
	}

	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}

	public int getSiharaikubun() {
		return siharaikubun;
	}

	public void setSiharaikubun(int siharaikubun) {
		this.siharaikubun = siharaikubun;
	}

	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;

	}

	public String getTodokesakikubun() {
		return todokesakikubun;
	}

	public void setTodokesakikubun(String todokesakikubun) {
		this.todokesakikubun = todokesakikubun;
	}

	public String getTodokesakiad() {
		return todokesakiad;
	}

	public void setTodokesakiad(String todokesakiad) {
		this.todokesakiad = todokesakiad;
	}

	public int getKeiyakutenno() {
		return keiyakutenno;
	}

	public void setKeiyakutenno(int keiyakutenno) {
		this.keiyakutenno = keiyakutenno;
	}

	public int getBesso() {
		return besso;
	}

	public void setBesso(int besso) {
		this.besso = besso;
	}


	public String getTodokesakipost() {
		return todokesakipost;
	}

	public void setTodokesakipost(String todokesakipost) {
		this.todokesakipost = todokesakipost;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;
	}
}
