package Login;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.KaiinDTO;

/**
 * Servlet implementation class login
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public login() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

         String kaishamei=request.getParameter("kaisyamei");
         String shainno=request.getParameter("syainno");
         String kaiinmei=request.getParameter("kaiinmei");

         System.out.println(kaishamei);

		 HttpSession session1=request.getSession();
		 HttpSession session2=request.getSession();
		 HttpSession session3=request.getSession();
		 HttpSession session5=request.getSession();

		 session5.setAttribute("kaiinmei", kaiinmei);

         ArrayList<KaiinDTO> kaiin = KaiinDAO.getKaiinList(kaishamei, shainno);
         for(KaiinDTO i : kaiin){
        	 int kaishano=i.getKaishano();
        	 String katakana=i.getKatakana();
        	 request.setAttribute("member", kaiin);
             session1.setAttribute("shainno",shainno);
             session2.setAttribute("kaishano",kaishano);
             session3.setAttribute("kanji",i.getkanji());
             session1.setAttribute("katakana", katakana);
             session1.setAttribute("kaishamei", kaishamei);

             System.out.println(session2.getAttribute("kaishano"));
             RequestDispatcher rd=request.getRequestDispatcher("Layout_jsp/B020C.jsp");
             rd.forward(request,response);
         }
         if(kaiin.size()==0){
        	 response.sendRedirect("Layout_jsp/B002.jsp");
         }

		//TODO Auto-generated method stub
	}

}
