package kigyoToroku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTO.KyosankigyouDTO;

/**
 * Servlet implementation class B033
 */
@WebServlet("/B033")
public class B033 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B033() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字コード
				request.setCharacterEncoding("utf8");

				//htmlから
		        try{
		        	String kaishamei = request.getParameter("kaishamei");
		        	String kaishakana = request.getParameter("kaishakana");
		        	String tantobusho = request.getParameter("tantobusho");
		        	String tantoshimei = request.getParameter("tantoshimei");
		        	String tantokana = request.getParameter("tantokana");
		        	int tantotel = Integer.parseInt(request.getParameter("tantotel"));
		        	int tantofax = Integer.parseInt(request.getParameter("tantofax"));
		        	String kaishapost = request.getParameter("kaishapost");
		        	String kaishaadd =request.getParameter("kaishaadd");
		        	int kaishatel = Integer.parseInt(request.getParameter("kaishatel"));
		        	int kaishafax = Integer.parseInt(request.getParameter("kaishafax"));
		        	int kyuyo = Integer.parseInt(request.getParameter("kyuyo"));
		        	int todokesaki = Integer.parseInt(request.getParameter("todokesaki"));

		        	ArrayList<KyosankigyouDTO> kyosannoList = kigyoDao.getSearchkyousanno();
		        	KyosankigyouDTO kyosanno = new KyosankigyouDTO();

		        	int kaishano = (kyosanno.getKaishano())+1;

		        	kigyoDao.getInsertkigyo(kaishano,kaishamei,kaishakana,tantobusho,tantoshimei,tantokana,tantotel
		        			,tantofax,kaishapost,kaishaadd,kaishatel,kaishafax,kyuyo,todokesaki);//htmlから取得した入力項目を入れる

		        	ArrayList<KyosankigyouDTO> kigyoList = kigyoDao.getSearchkyosan(kaishakana);

		        	request.setAttribute("kigyoList", kigyoList);
		    		RequestDispatcher rd = request.getRequestDispatcher("../B035.jsp");//アクセス先のJSP
		    		rd.forward(request, response);


		        }catch(Exception e){

		        }finally{

		        }

	}

}
