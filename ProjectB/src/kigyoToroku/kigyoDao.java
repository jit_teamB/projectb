package kigyoToroku;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.KyosankigyouDTO;

public class kigyoDao {
	//協賛企業追加
			public static void getInsertkigyo(int kaishano, String kaishamei, String kaishakana, String tantobusho,
					String tantoshimei, String tantokana, int tantotel, int tantofax, String kaishapost,
					String kaishaadd, int kaishatel, int kaishafax, int kyuyo, int todokesaki) {
				// 接続情報を定義
				String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";
				// Class.forName("org.gjt.mm.mysql.Driver");
				Connection con = null;
				Statement startment = null;

				try {
					// MySQLとの接続を開始
					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					startment = con.createStatement();
					System.out.println("接続成功");

					// 実行するSQLを定義
					String sql = "INSERT INTO tkyosankigyou(kaishano,kaishamei,kaishakatakana,kaishatel,kaishafax,kaishapost,kaishaad,tantoubusho,tantoushakanji,tantoushakatakana,tantoushatel,tantoushafax,kyuuyosiharai,todokesakikubun) "
							+ "VALUES ('"+kaishano+"','"+kaishamei+"','"+kaishakana+"','"+kaishatel+"','"+kaishafax+"','"+kaishapost+"','"+kaishaadd+"','"+tantobusho+"','"+tantoshimei+"','"+tantokana+"','"+tantotel+"','"+tantofax+"','"+kyuyo+"',2)";//DBのカラムにあわせて書く


					// 定義したSQLを実行、UPDATEの場合は更新した件数が戻る
					int count = startment.executeUpdate(sql);

					if (count!=1) {
						System.out.println("更新した件数:" + count);
					} else {
						System.out.println("更新した件数:" + count);
					}

					// MySQLとの接続を終了
					con.close();
					System.out.println("接続終了");

					// エラー情報を取得
				} catch (ClassNotFoundException conf) {
					System.out.println(conf);

				} catch (SQLException sqep) {
					System.out.println(sqep);
				} finally {
					if (startment != null) {
						try {
							startment.close();
						} catch (SQLException e) {
						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {

						}
					}
				}

			}
			//協賛企業検索
			public static ArrayList<KyosankigyouDTO> getSearchkyosan(String kaishakana) {
				// 接続情報を定義
				String url = "jdbc:mysql://localhost:3306/bbs?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";
				Connection con = null;
				Statement startment = null;

				ArrayList<KyosankigyouDTO> kyosanList = new ArrayList<KyosankigyouDTO>();

				try {
					// MySQLとの接続を開始
					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					startment = con.createStatement();
					System.out.println("接続成功");

					// 実行するSQLを定義
					String sql = "SELECT * FROM tkyosankigyou where kaishakatakana = "+ kaishakana;

					// 定義したSQLを実行して、実行結果を変数resultに代入
					ResultSet result = startment.executeQuery(sql);

					// 変数resultの内容を順次出力
					while (result.next()) {//DBが出来てから
						KyosankigyouDTO kyosan = new KyosankigyouDTO();
						kyosan.setKaishamei(result.getString("kaishamei"));//契約店番号
						kyosan.setKaishameikana(result.getString("kaishakatakana"));
						kyosan.setPostcode(result.getString("kaishapost"));
						kyosan.setAddress(result.getString("kaishaad"));
						kyosan.setTantoubusho(result.getString("tantoubusho"));
						kyosan.setTantoushakanji(result.getString("tantoushakanji"));
						kyosan.setTantoushakatakana(result.getString("tantoushakatakana"));
						kyosan.setTantoushadenwano(result.getString("tantoushatel"));
						kyosan.setTantoushafaxno(result.getString("tantoushafax"));
						kyosan.setTodokesakino(result.getInt("todokesakikubun"));

						kyosanList.add(kyosan);// 配列に追加
					}

					// MySQLとの接続を終了
					con.close();
					System.out.println("接続終了");

					// エラー情報を取得
				} catch (ClassNotFoundException conf) {
					System.out.println(conf);

				} catch (SQLException sqep) {
					System.out.println(sqep);
				} finally {
					if (startment != null) {
						try {
							startment.close();
						} catch (SQLException e) {
						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {

						}
					}
				}
				return kyosanList;
			}

			//契約店番号
			public static ArrayList<KyosankigyouDTO> getSearchkyousanno() {
				// 接続情報を定義
				String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
				String user = "root";
				String password = "";
				Connection con = null;
				Statement startment = null;

				ArrayList<KyosankigyouDTO> kyosannoList = new ArrayList<KyosankigyouDTO>();

				try {
					// MySQLとの接続を開始
					Class.forName("org.gjt.mm.mysql.Driver");
					con = DriverManager.getConnection(url, user, password);
					startment = con.createStatement();
					System.out.println("接続成功");

					// 実行するSQLを定義
					String sql = "SELECT MAX(kaishano) FROM tkyosankigyou";

					// 定義したSQLを実行して、実行結果を変数resultに代入
					ResultSet result = startment.executeQuery(sql);

					// 変数resultの内容を順次出力
					while (result.next()) {//DBが出来てから
						KyosankigyouDTO keiyaku = new KyosankigyouDTO();
						keiyaku.setKaishano(result.getInt("kaishano"));//契約店番号
						kyosannoList.add(keiyaku);// 配列に追加
					}

					// MySQLとの接続を終了
					con.close();
					System.out.println("接続終了");

					// エラー情報を取得
				} catch (ClassNotFoundException conf) {
					System.out.println(conf);

				} catch (SQLException sqep) {
					System.out.println(sqep);
				} finally {
					if (startment != null) {
						try {
							startment.close();
						} catch (SQLException e) {
						}
					}
					if (con != null) {
						try {
							con.close();
						} catch (SQLException e) {

						}
					}
				}
				return kyosannoList;
			}
}
