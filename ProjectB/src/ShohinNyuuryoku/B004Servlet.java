package ShohinNyuuryoku;

//import goodToroku.ShohinDao;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.shohinDTO;

/**
 * Servlet implementation class B004Servlet
 */
@WebServlet("/B004Servlet")
public class B004Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B004Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 文字コード
		request.setCharacterEncoding("utf8");
		// htmlから
//			int shohinno1 = Integer.parseInt(request.getParameter("shohinno1"));
//			int shohinno2 = Integer.parseInt(request.getParameter("shohinno2"));
//			int shohinno3 = Integer.parseInt(request.getParameter("shohinno3"));
//			int shohinno4 = Integer.parseInt(request.getParameter("shohinno4"));
//			int shohinno5 = Integer.parseInt(request.getParameter("shohinno5"));
//			int shohinno6 = Integer.parseInt(request.getParameter("shohinno6"));


		String shohinno1 = request.getParameter("shohinno1");
		String shohinno2 = request.getParameter("shohinno2");
		String shohinno3 = request.getParameter("shohinno3");
		String shohinno4 = request.getParameter("shohinno4");
		String shohinno5 = request.getParameter("shohinno5");
		String shohinno6 = request.getParameter("shohinno6");

			ArrayList<shohinDTO> shohinno = new ArrayList<shohinDTO>();
			if(shohinno1.equals(null)){
				shohinno1 = "0";
			}
			if(shohinno2.equals(null)){
				shohinno2 = "0";
			}
			if(shohinno3.equals(null)){
				shohinno3 = "0";
			}
			if(shohinno4.equals(null)){
				shohinno4 = "0";
			}
			if(shohinno5.equals(null)){
				shohinno5 = "0";
			}
			if(shohinno6.equals(null)){
				shohinno6 = "0";
			}



		    //Daoにこの値を条件にとってこいと依頼して配列に格納してる
			ArrayList<shohinDTO> Nyuuryoku = new ArrayList<shohinDTO>();
		    Nyuuryoku = B004DAO.getSelectshohin(shohinno1,shohinno2,shohinno3,shohinno4,shohinno5,shohinno6);
		    ArrayList<shohinDTO> tikuseki = new ArrayList<shohinDTO>();

		    tikuseki.addAll(Nyuuryoku);
		    int cnt=0;
		    for(shohinDTO i : Nyuuryoku){
		    	cnt++;
		    }
		    if(cnt>0){
			//requestに保存してjspで取得データ使えるように
			request.setAttribute("Nyuuryoku", tikuseki);
			HttpSession session = request.getSession();
			session.setAttribute("Nyuuryoku1", tikuseki);
			RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B005.jsp");//アクセス先のJSP
			rd.forward(request, response);
		    }
		    if(cnt<=0){
		    	RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B004.jsp");//アクセス先のJSP
				rd.forward(request, response);
		    }
		}
}
