package ShohinNyuuryoku;

import java.sql.Timestamp;

public class CARTDTO {

	private int juchuno;
	private int shohinno;
	private String gara;
	private String color;
	private String size;
	private int suryo;
	private int siharaikubun;
	private int bunkatukaisu;
	private String todokesakikubun;
	private String todokesakiad;
	private int keiyakutenno;
	private String haisosize;
	private String todokesakipost;
	private String todokesakitel;
	private String todokesakimei;
	private String rusu;
	private Timestamp update;
	private String shohinmei;
	private int specialfee;



	public String getShohinmei() {
		return shohinmei;
	}

	public void setShohinmei(String shohinmei) {
		this.shohinmei = shohinmei;
	}

	public int getJuchuno() {
		return juchuno;
	}

	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}

	public int getShohinno() {
		return shohinno;
	}

	public void setShohinno(int shohinno) {
		this.shohinno = shohinno;
	}

	public String getGara() {
		return gara;
	}

	public void setGara(String gara) {
		this.gara = gara;

	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getSuryo() {
		return suryo;
	}

	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}

	public int getSiharaikubun() {
		return siharaikubun;
	}

	public void setSiharaikubun(int siharaikubun) {
		this.siharaikubun = siharaikubun;
	}

	public int getBunkatukaisu() {
		return bunkatukaisu;
	}

	public void setBunkatukaisu(int payment) {
		this.bunkatukaisu = payment;
	}

	public String getTodokesakikubun() {
		return todokesakikubun;
	}

	public void setTodokesakikubun(String todokesakikubun) {
		this.todokesakikubun = todokesakikubun;
	}

	public String getTodokesakiad() {
		return todokesakiad;
	}

	public void setTodokesakiad(String todokesakiad) {
		this.todokesakiad = todokesakiad;
	}

	public int getKeiyakutenno() {
		return keiyakutenno;
	}

	public void setKeiyakutenno(int keiyakutenno) {
		this.keiyakutenno = keiyakutenno;
	}

	public String getHaisosize() {
		return haisosize;
	}

	public void setHaisosize(String haizosize) {
		this.haisosize = haisosize;
	}

	public String getTodokesakipost() {
		return todokesakipost;
	}

	public void setTodokesakipost(String todokesakipost) {
		this.todokesakipost = todokesakipost;
	}

	public String getTodokesakitel() {
		return todokesakitel;
	}

	public void setTodokesakitel(String todokesakitel) {
		this.todokesakitel = todokesakitel;
	}

	public String getTodokesakimei() {
		return todokesakimei;
	}

	public void setTodokesakimei(String todokesakimei) {
		this.todokesakimei = todokesakimei;
	}

	public String getRusu() {
		return rusu;
	}

	public void setRusu(String rusu) {
		this.rusu = rusu;
	}

	public Timestamp getUpdate() {
		return update;
	}

	public void setUpdate(Timestamp update) {
		this.update = update;

	}


	public int getSpecialfee() {
		return specialfee;
	}

	public void setSpecialfee(int i) {
		this.specialfee = specialfee;
	}

}
