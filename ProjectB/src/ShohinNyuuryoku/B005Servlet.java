package ShohinNyuuryoku;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DTO.SiharaiDTO;
import DTO.shohinDTO;

/**
 * Servlet implementation class B005Servlet
 */
@WebServlet("/B005Servlet")
public class B005Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public B005Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int juchuno = (int) session.getAttribute("juchuno");
		String shainno = (String) session.getAttribute("shainno");
		ArrayList<shohinDTO> Nyuuryoku = (ArrayList<shohinDTO>) session.getAttribute("Nyuuryoku1");

		int cnt=1;

		SiharaiDTO siharai = yoshinDAO.gettshiharai(shainno);
		int yosin = siharai.getYosingaku();
		int mibarai = siharai.getMibarai();
		session.setAttribute("yosin", yosin);
		session.setAttribute("mibarai", mibarai);

		//
		ArrayList<CALCDTO> sentakudata = new ArrayList<CALCDTO>();

		//addsentakuは蓄積商品データ
		ArrayList<CALCDTO> addsentaku = (ArrayList<CALCDTO>)session.getAttribute("sentakudata");

		if(addsentaku==null){
			addsentaku = new ArrayList<CALCDTO>();
		}

		int kaisu = 0;

		//Nyuuryokuは詳細選択した商品一覧
			for(shohinDTO j : Nyuuryoku){
				int update = 0;
				String check = request.getParameter("shohin"+cnt);
				//チェックボックスにチェックが入っていたら;
				System.out.println("チェック  "+check);
				if(check!=null){
					//数量を持ってくる
					String suryo = request.getParameter("suryo"+cnt);
					double kosud = Double.parseDouble(suryo);
					int kosu = (int) kosud;


					int ima_sno = j.getShohinno();
					String ima_color = j.getColor();
					String ima_gara = j.getGara();
					String ima_size = j.getSize();

					//すでに入っているカートの中身を格納
					ArrayList<CALCDTO> cartdata = B005DAO.getSelectshohin(juchuno);

					for(CALCDTO k : cartdata){;
						int cart_juchu = k.getJuchuno();
						int cart_sno = k.getShohinno();
						String cart_color = k.getColor();
						String cart_gara = k.getGara();
						String cart_size = k.getSize();
						int cart_kosu = k.getSuryo();
						System.out.println("カート内=  "+cart_sno+cart_color+cart_gara+cart_size+cart_kosu);

						if(juchuno==cart_juchu && ima_sno==cart_sno && ima_color.equals(cart_color) && ima_gara.equals(cart_gara) && ima_size.equals(cart_size)){
							//今の個数を足す
							kosu = kosu + cart_kosu;
							B005DAO.updateshohin(j.getShohinno(), j.getColor(), j.getGara(), j.getSize(),kosu);
							update = 1;
							break;
						}
					}
					if(update == 0){
						B005DAO.setSelectshohin(juchuno, j.getShohinmei(),j.getShohinno(), j.getColor(), j.getGara(), j.getSize(), j.getKeiyakuten(),j.getHaisosize(),kosu);
					}
				}
				cnt++;
			}
			addsentaku = B005DAO.getSelectshohin(juchuno);
			session.setAttribute("sentakudata", addsentaku);
			ArrayList<CALCDTO> addall = (ArrayList<CALCDTO>)session.getAttribute("sentakudata");

			cnt=1;
			int zeikomi=0;
			int zeinuki=0;
			int addsum =0;
			for(CALCDTO j : addall){
					System.out.println(j.getShohinmei()+"  "+j.getPrice());
					int kosu = j.getSuryo();
					zeikomi = (int) (zeikomi + (j.getPrice()*kosu*1.08));
					zeinuki = (int) (zeinuki + (j.getPrice()*kosu));
					cnt++;
			}
			System.out.println(mibarai);

			addsum=mibarai+zeikomi;
			System.out.println(yosin + "   " + addsum);
			session.setAttribute("shokei", zeikomi);
			session.setAttribute("addsum", addsum);
			cnt=1;

			RequestDispatcher rd = request.getRequestDispatcher("Layout_jsp/B006.jsp");//アクセス先のJSP
			rd.forward(request, response);
		}
}
