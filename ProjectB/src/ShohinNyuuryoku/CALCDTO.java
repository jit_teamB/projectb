package ShohinNyuuryoku;

import java.sql.Timestamp;

public class CALCDTO {

	private int shohinno;
	private int juchuno;
	private String shohinmei;
	private String color;
	private String gara;
	private String size;
	private int price;
	private int suryo;
	private int keiyakuten;
	private Timestamp update;



	public int getPrice() {
		return price;
	}
	public Timestamp getUpdate() {
		return update;
	}
	public void setUpdate(Timestamp update) {
		this.update = update;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getShohinmei() {
		return shohinmei;
	}
	public void setShohinmei(String shohinmei) {
		this.shohinmei = shohinmei;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getGara() {
		return gara;
	}
	public void setGara(String gara) {
		this.gara = gara;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getShohinno() {
		return shohinno;
	}
	public void setShohinno(int shohinno) {
		this.shohinno = shohinno;
	}
	public int getSuryo() {
		return suryo;
	}
	public void setSuryo(int suryo) {
		this.suryo = suryo;
	}
	public int getJuchuno() {
		return juchuno;
	}
	public void setJuchuno(int juchuno) {
		this.juchuno = juchuno;
	}
	public int getKeiyakuten() {
		return keiyakuten;
	}
	public void setKeiyakuten(int keiyakuten) {
		this.keiyakuten = keiyakuten;
	}

}
