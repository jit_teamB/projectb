/**
 *
 */
package ShohinNyuuryoku;

/**
 * @author i-learning
 *
 */

	import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import DTO.SiharaiDTO;

	public class yoshinDAO {
		//商品検索
		public static SiharaiDTO gettshiharai(String shainno){
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			// Class.forName("org.gjt.mm.mysql.Driver");
			Connection con = null;
			Statement startment = null;
			SiharaiDTO siharai = new SiharaiDTO();


			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				// 実行するSQLを定義
				String sql = "SELECT * FROM tshiharai where shainno='" + shainno + "'";

				//定義したSQLを実行して、実行結果を変数resultに代入
				System.out.println(sql);
				ResultSet result = startment.executeQuery(sql);


				// 変数resultの内容を順次出力

				while (result.next()) {
					siharai.setYosingaku(result.getInt("yosingaku"));
					siharai.setMibarai(result.getInt("mibarai"));
				}

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
			return siharai;
		}
}
