/**
 *
 */
package ShohinNyuuryoku;

/**
 * @author i-learning
 *
 */

	import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DTO.shohinDTO;

	public class B004DAO {
		//商品検索
		public static ArrayList<shohinDTO> getSelectshohin(String shohinno1,String shohinno2,String shohinno3,String shohinno4,String shohinno5,String shohinno6){
			// 接続情報を定義
			String url = "jdbc:mysql://localhost:3306/hanbaikanridb?useUnicode=true&characterEncoding=utf8";
			String user = "root";
			String password = "";
			// Class.forName("org.gjt.mm.mysql.Driver");
			Connection con = null;
			Statement startment = null;


			ArrayList<shohinDTO> Nyuuryoku = new ArrayList<shohinDTO>();


			try {
				// MySQLとの接続を開始
				Class.forName("org.gjt.mm.mysql.Driver");
				con = DriverManager.getConnection(url, user, password);
				startment = con.createStatement();
				System.out.println("接続成功");

				String parameter1 = shohinno1;
				String parameter2 = shohinno2;
				String parameter3 = shohinno3;
				String parameter4 = shohinno4;
				String parameter5 = shohinno5;
				String parameter6 = shohinno6;


				// 値を埋め込む前の形のSQL文をコンパイルし、構文を確定
				PreparedStatement st = con.prepareStatement("SELECT * FROM tshohin where shohinno=? or shohinno=? or shohinno=? or shohinno=? or shohinno=? or shohinno=?");

				st.setString(1, parameter1); 	// ? の場所に値を埋め込む
				st.setString(2, parameter2); 	// ? の場所に値を埋め込む
				st.setString(3, parameter3); 	// ? の場所に値を埋め込む
				st.setString(4, parameter4); 	// ? の場所に値を埋め込む
				st.setString(5, parameter5); 	// ? の場所に値を埋め込む
				st.setString(6, parameter6); 	// ? の場所に値を埋め込む

				// クエリの実行
				ResultSet result = st.executeQuery();


				// 変数resultの内容を順次出力

				while (result.next()) {
					shohinDTO shohin = new shohinDTO();

					shohin.setShohinno(result.getInt("shohinno"));
					shohin.setShohinmei(result.getString("shohinmei"));
					shohin.setGara(result.getString("gara"));
					shohin.setColor(result.getString("color"));
					shohin.setSize(result.getString("size"));
					shohin.setPrice(result.getInt("price"));
					shohin.setToriatukai(result.getInt("toriatukai"));
					shohin.setHaisosize(result.getString("haisosize"));
					shohin.setKeiyakuten(result.getInt("keiyakutenno"));
					Nyuuryoku.add(shohin);
				}

				// MySQLとの接続を終了
				con.close();
				System.out.println("接続終了");

				// エラー情報を取得
			} catch (ClassNotFoundException conf) {
				System.out.println(conf);

			} catch (SQLException sqep) {
				System.out.println(sqep);
			} finally {
				if (startment != null) {
					try {
						startment.close();
					} catch (SQLException e) {
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {

					}
				}
			}
			return Nyuuryoku;
		}
}
