<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>商品情報入力</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>
<form action="B025" method="post">
新規の商品情報を入力してください。
<br><br>
<table align="center">
<tr>
<td>商品番号</td><td>：</td><td><input type="text" name="shohinno"></td>
</tr><tr>
<td>商品名</td><td>：</td><td><input type="text" name="shohinmei"></td>
</tr><tr>
<td>重量・サイズ</td><td>：</td><td><input type="text" name="shohinsize"></td>
</tr><tr>
<td>柄</td><td>：</td><td><input type="text" name="shohingara"></td>
</tr><tr>
<td>色</td><td>：</td><td><input type="text" name="shohincolor"></td>
</tr><tr>
<td>単位</td><td>：</td><td><input type="text" name="shohintani"></td>
</tr><tr>
<td>取り扱い状況</td><td>：</td><td><input type="checkbox" name="toriatsukai" value="1"></td>
</tr><tr>
<td>販売開始日</td><td>：</td><td><input type="text" name="startday"></td>
</tr><tr>
<td>販売終了日</td><td>：</td><td><input type="text" name="endday"></td>
</tr><tr>
<td>商品価格</td><td>：</td><td><input type="text" name="price"></td>
</tr><tr>
<td>契約店番号</td><td>：</td><td><input type="text" name="keiyakuno"></td>
</tr><tr>
<td>備考</td><td>：</td><td><input type="text" name="memo"></td>
</tr><tr>
<td>別送</td><td>：</td><td><input type="checkbox" name="besso" value="1"></td>
</tr>
</table>
<br>
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="登録" />
</form>
<form action="B018A.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div>
</body>
</html>