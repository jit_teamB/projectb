<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
//文字コード
request.setCharacterEncoding("utf8");
//htmlから取得
String shainno = request.getParameter("shainno");
String shainkanji = request.getParameter("shainkanji");
String shainkana = request.getParameter("shainkana");
int shaintel = Integer.parseInt(request.getParameter("shaintel"));
int shainfax = Integer.parseInt(request.getParameter("shainfax"));
int shainpost = Integer.parseInt(request.getParameter("shainpost"));
String shainadd = request.getParameter("shainadd");
String kaishamei = request.getParameter("kaishamei");
String shainbusho = request.getParameter("shainbusho");
String memo = request.getParameter("bikou");

%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet" type="text/css" />
<title>登録会員情報確認</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>

<div style="text-align:center">


<form action="B019.jsp" method="post">
登録会員情報を確認してください。
<br><br>
<table align="center">
<tr>
<td>社員番号　</td><td>：</td><td><input type="hidden" name="shainno" value=shainno><%=shainno %>
</tr><tr>
<td>氏名(漢字)　</td><td>：</td><td>　<input type="hidden" name="shainkanji" value=shainkanji><%=shainkanji %>
</tr><tr>
<td>氏名(カタカナ)　</td><td>：</td><td>　<input type="hidden" name="shainkana" value=shainkana><%=shainkana %>
</tr><tr>
<td>電話番号　</td><td>：</td><td>　<input type="hidden" name="shaintel" value=shaintel><%=shaintel %>
</tr><tr>
<td>FAX番号　</td><td>：</td><td>　<input type="hidden" name="shainfax" value=shainfax><%=shainfax %>
</tr><tr>
<td>郵便番号　</td><td>：</td><td>　<input type="hidden" name="shainpost" value=shainpost><%=shainpost %>
</tr><tr>
<td>会員住所　</td><td>：</td><td>　<input type="hidden" name="shainadd" value=shainadd><%=shainadd %>
</tr><tr>
<td>会社名　</td><td>：</td><td>　<input type="hidden" name="kaishamei" value=kaishamei><%=kaishamei %>
</tr><tr>
<td>所属部署　</td><td>：</td><td>　<input type="hidden" name="shainbusho" value=shainbusho><%=shainbusho %>
</tr><tr>
<td>備考　</td><td>：</td><td>　<input type="hidden" name="memo" value=memo><%=memo %>
</table>
<br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="登録完了" />
</form>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="B019.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div>
</div>
</div></div>
</body>
</html>