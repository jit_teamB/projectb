<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ShohinNyuuryoku.CALCDTO"%>
<%
	ArrayList<CALCDTO> a = (ArrayList<CALCDTO>) session
			.getAttribute("sentakudata");
	int yosin = (Integer) session.getAttribute("yosin");
	int addsum = (Integer) session.getAttribute("addsum");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet"
	type="text/css" />
<title>受注情報確認</title>
</head>
<body>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<br> はるみ通信販売管理システム <br>
						<p>
						<div id="nav">
							<ul>
								<li><a target="_top" href="/ProjectB/Layout_jsp/B001.jsp">TOP</a></li>
								<li><a <%if (yosin >= addsum) {%> target="_self"
									href="/ProjectB/Layout_jsp/B004.jsp" <%}%>>商品番号入力</a></li>
								<li><a <%if (yosin >= addsum) {%> target="_self"
									href="/ProjectB/Layout_jsp/B004.jsp" <%}%>>商品詳細選択</a></li>
								<li><a <%if (yosin >= addsum) {%> target="_self"
									href="/ProjectB/Layout_jsp/B006.jsp" <%}%>>受注情報確認</a></li>
								<li><a <%if (yosin >= addsum) {%> target="_self"
									href="/ProjectB/Layout_jsp/B007.jsp" <%}%>>配送先選択</a></li>
								<li><a <%if (yosin >= addsum) {%> target="_self"
									href="/ProjectB/Layout_jsp/B008.jsp" <%}%>>支払方法選択</a></li>
								<li><a <%if (yosin >= addsum) {%> target="_self"
									href="/ProjectB/chumonuke_katsuda" <%}%>>注文受付確認</a></li>
							</ul>
							<p>
							<p>登録済み商品を表示します</p>
							<br> <br>
							<form action="/ProjectB/delshohin" method="post">
								<div id="box75"></div>
								<div id="box125">商品番号</div>
								<div id="box325">商品名</div>
								<div id="box75">色</div>
								<div id="box75">柄</div>
								<div id="box75">
									<font size="1">サイズ</font>
								</div>
								<div id="box125">単価</div>
								<div id="box75">数量</div>
								<br>
								<%
									if (a.size() != 0) {
										int cnt = 1;
										for (CALCDTO com : a) {
								%>
								<div id="box75">
									<input type="checkbox" name="sakujo<%=cnt%>" value="<%=cnt%>">
								</div>
								<div id="box125">
									<%=com.getShohinno()%>
								</div>
								<div id="box325">
									<font size="1"> <%=com.getShohinmei()%></font>
								</div>
								<div id="box75">
									<font size="2"> <%=com.getColor()%></font>
								</div>
								<div id="box75">
									<font size="2"> <%=com.getGara()%></font>
								</div>
								<div id="box75">
									<font size="1"> <%=com.getSize()%></font>
								</div>
								<div id="box125">
									<%=com.getPrice()%>
								</div>
								<div id="box75">
									<%=com.getSuryo()%>
								</div>
								<%
									cnt++;
										}
									}
								%>
								<div id="box600a"></div>
								<div id="box75a"></div>
								<div id="box75">小計</div>
								<div id="box125"><%=session.getAttribute("shokei")%></div>
								<div id="box75a"></div>

								<div id="box275d"></div>
								<div id="box200d">
									<input type="submit" value="削除" name="next"
										style="WIDTH: 70px; HEIGHT: 50px">
								</div>

							</form>
							<%
								if (yosin >= addsum) {
							%>
							<form action="/ProjectB/Layout_jsp/B007.jsp" method="post">
								<div id="box50d"></div>
								<div id="box200d">
									<input type="submit" value="次へ" name="next"
										style="WIDTH: 70px; HEIGHT: 50px">
								</div>

							</form>

							<%
								} else {
									int koe = 0;
									koe = addsum - yosin;
							%>
							<div id = "box250n">
								<p>与信限度を<%=koe%>円超えています</p>
							商品を削除してください</div>
							<%
								}
							%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
