<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DTO.shohinDTO"%>
<%
	ArrayList<shohinDTO> a = (ArrayList<shohinDTO>) session
			.getAttribute("Nyuuryoku1");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet"
	type="text/css" />
<title>商品詳細選択</title>
</head>
<body>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<br> はるみ通信販売管理システム <br>
						<p>
						<div id="nav">
							<ul>
								<li><a target="_top" href="/ProjectB/Layout_jsp/B001.jsp">TOP</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B004.jsp">商品番号入力</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B004.jsp">商品詳細選択</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B006.jsp">受注情報確認</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B007.jsp">配送先選択</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B008.jsp">支払方法選択</a></li>
								<li><a target="_self" href="/ProjectB/chumonuke_katsuda">注文受付確認</a></li>
								<p>
							</ul>
						</div>
						商品詳細を入力してください。 <br> <br>
						<form action="/ProjectB/B005Servlet" method="post">
							<div id="box75"></div>
							<div id="box125">商品番号</div>
							<div id="box275">商品名</div>
							<div id="box75">色</div>
							<div id="box75">柄</div>
							<div id="box75">
								<font size="1">サイズ</font>
							</div>
							<div id="box125">単価</div>
							<div id="box75">
								<font size="1">配送サイズ</font>
							</div>
							<div id="box75">数量</div>
							<%
								int cnt = 1;
								for (shohinDTO com : a) {
									if (com.getToriatukai() == 1) {
							%>
							<br>
							<div align="center">
								<div id="box75">
									<input type="checkbox" name="shohin<%=cnt%>" value="<%=cnt%>">
								</div>
								<div id="box125">
									<%=com.getShohinno()%>
								</div>
								<div id="box275">
									<font size="1"> <%=com.getShohinmei()%></font>
								</div>

								<div id="box75">
									<font size="2"> <%=com.getColor()%></font>
								</div>
								<div id="box75">
									<font size="2"> <%=com.getGara()%></font>
								</div>
								<div id="box75">
									<font size="1"> <%=com.getSize()%></font>
								</div>
								<div id="box125">
									<%=com.getPrice()%>
								</div>
								<div id="box75">
									<%=com.getHaisosize()%>
								</div>
								<div id="box75">
									<input type="number" name="suryo<%=cnt%>" step="1" min="1"
										max="99" value="1" required>
								</div>
								<br>
								<%
									} else {
								%>
								<div alert center>
									<div align="center">
										<div id="box75">×</div>
										<div id="box125">
											<%=com.getShohinno()%>
										</div>
										<div id="box275">
											<font size="1"> <%=com.getShohinmei()%></font>
										</div>
										<div id="box75">
											<font size="1"> <%=com.getColor()%></font>
										</div>
										<div id="box75">
											<font size="1"> <%=com.getGara()%></font>
										</div>
										<div id="box75">
											<font size="1"> <%=com.getSize()%></font>
										</div>
										<div id="box125">
											<%=com.getPrice()%>
										</div>
										<div id="box75">
											<%=com.getHaisosize()%>
										</div>
										<div id="box75">
											<input type="number" name="suryo<%=cnt%>" step="1" min="1"
												max="99" value="1" required>
										</div>
										<br> <br>
										<%
											}
												cnt++;
											}
										%>

										<div id="box275d"></div>
										<div id="box200d">
											<input type="submit"
												style="width: 40%; padding: 8px; font-size: 20px;"
												value="次へ" />
										</div>
						</form>
						<form action="/ProjectB/Layout_jsp/B004.jsp" method="post">
							<div id="box50d"></div>
							<div id="box200d">
								<input type="submit"
									style="width: 40%; padding: 8px; font-size: 20px;" value="戻る" />
							</div>
						</form>


					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
