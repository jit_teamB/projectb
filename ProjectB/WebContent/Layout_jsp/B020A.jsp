<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
request.setCharacterEncoding("utf8");

String shainno = request.getParameter("shainno");
String kanji = request.getParameter("kanji");
String katakana = request.getParameter("katakana");
String tel = request.getParameter("tel");
String fax = request.getParameter("fax");
String post = request.getParameter("post");
String address = request.getParameter("address");
String kaishamei = request.getParameter("kaishamei");
String busho = request.getParameter("busho");
String memo = request.getParameter("memo");
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>編集内容確認画面</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>
<div style="text-align:center">
<form action="B020A" method="post">
編集内容を確認してください。
<br><br>
<table align="center">
<tr>
<td>氏名(漢字)　</td><td>：</td><td>　<input type="hidden" name="kanji" value=kanji><%=kanji %>
</tr><tr>
<td>氏名(カタカナ)　</td><td>：</td><td>　<input type="hidden" name="katakana" value=katakana><%=katakana %>
</tr><tr>
<td>電話番号　</td><td>：</td><td>　<input type="hidden" name="tel" value=tel><%=tel %>
</tr><tr>
<td>FAX番号　</td><td>：</td><td>　<input type="hidden" name="fax" value=fax><%=fax %>
</tr><tr>
<td>郵便番号　</td><td>：</td><td>　<input type="hidden" name="post" value=post><%=post %>
</tr><tr>
<td>会員住所　</td><td>：</td><td>　<input type="hidden" name="address" value=address><%=address %>
</tr><tr>
<td>会社名　</td><td>：</td><td>　<input type="hidden" name="keiyakuno" value=kaishamei><%=kaishamei %>
</tr><tr>
<td>所属部署　</td><td>：</td><td>　<input type="hidden" name="busho" value=busho><%=busho %>
</tr><tr>
<td>備考　</td><td>：</td><td>　<input type="hidden" name="memo" value=memo><%=memo %>
</tr>
</table>
<br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="編集完了" />
</form>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="B022.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>