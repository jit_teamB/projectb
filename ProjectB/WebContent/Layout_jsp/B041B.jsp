<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
        //文字コード
		request.setCharacterEncoding("utf8");

		//htmlから
      	String keiyakutenno = request.getParameter("keiyakutenno");
      	String keiyakutenmei = request.getParameter("keiyakutenmei");
      	String keiyakutenkana = request.getParameter("keiyakutenkana");
      	int keiyakutentel = Integer.parseInt(request.getParameter("keiyakutentel"));
      	int keiyakutenfax = Integer.parseInt(request.getParameter("keiyakutenfax"));
      	String keiyakutenpost = request.getParameter("keiyakutenpost");
      	String keiyakutenadd = request.getParameter("keiyakutenadd");
      	String keiyakutentanto = request.getParameter("keiyakutentanto");
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" /><title>契約店情報削除確認</title>
</head>
<div style="text-align:center">

<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報編集</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<form action="B041B" method="post">
削除する契約店情報を確認してください。
<table align="center">
<tr>
<td>契約店番号</td><td>：</td><td><input type="hidden" name="keiyakutenno" value="keiyakutenno"></td>
</tr><tr>
<td>契約店名(漢字)</td><td>：</td><td><input type="hidden" name="keiyakutenmei" value="keiyakutenmei"></td>
</tr><tr>
<td>契約店名(カタカナ)</td><td>：</td><td><input type="hidden" name="keiyakutenkana" value="keiyakutenkana"></td>
</tr><tr>
<td>契約店電話番号</td><td>：</td><td><input type="hidden" name="keiyakutentel" value="keiyakutentel"></td>
</tr><tr>
<td>契約店FAX番号</td><td>：</td><td><input type="hidden" name="keiyakutenfax" value="keiyakutenfax"></td>
</tr><tr>
<td>契約店郵便番号</td><td>：</td><td><input type="hidden" name="keiyakutenpost" value="keiyakutenpost"></td>
</tr><tr>
<td>契約店住所</td><td>：</td><td><input type="hidden" name="keiyakutenadd" value="keiyakutenadd"></td>
</tr><tr>
<td>契約店担当者</td><td>：</td><td><input type="hidden" name="keiyakutentanto" value="keiyakutentanto"></td>
</tr><tr>
</table>
<br><br>

<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="削除完了" />
</form>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="B043A.jsp" method="post">
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>