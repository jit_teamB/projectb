<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
//文字コード
request.setCharacterEncoding("utf8");
//jspから取得
String kaishamei = request.getParameter("kaishamei");
String kaishakana = request.getParameter("kaishakana");
String postcode = request.getParameter("postcode");
String address = request.getParameter("address");
String kaishatel = request.getParameter("kaishatel");
String kaishafax = request.getParameter("kaishafax");
String tantoubusho = request.getParameter("tantoubusho");
String tantoushakanji = request.getParameter("tantoushakanji");
String tantoushakatakana = request.getParameter("tantoushakatakana");
String tantoushatel = request.getParameter("tantoushatel");
String tantoushafax = request.getParameter("tantoushafax");
int kyuyo = Integer.parseInt(request.getParameter("kyuyo"));
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>協賛企業編集確認</title>
</head>


<div style="text-align:center">
<body>
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<body>
<form action="B037" method="post">
編集する協賛企業情報を確認してください。
<br><br>
<table align="center">
<tr>
<td>会社名(漢字)</td><td>：</td><td><input type="hidden" name="kaishamei" value=kaishamei><%=kaishamei %></td>
</tr><tr>
<td>会社名(カタカナ)</td><td>：</td><td><input type="hidden" name="kaishakana" value=kaishakana><%=kaishakana %></td>
</tr><tr>
<td>会社郵便番号</td><td>：</td><td><input type="hidden" name="postcode" value=postcode><%=postcode %></td>
</tr><tr>
<td>会社住所</td><td>：</td><td><input type="hidden" name="address" value=address><%=address %></td>
</tr><tr>
<td>会社電話番号</td><td>：</td><td><input type="hidden" name="kaishatel" value=kaishatel><%=kaishatel %></td>
</tr><tr>
<td>会社FAX番号</td><td> ： </td><td><input type="hidden" name="kaishafax" value=kaishafax><%=kaishafax %></td>
</tr><tr>
<td>担当部署</td><td>：</td><td><input type="hidden" name="tantoubusho" value=tantoubusho><%=tantoubusho %></td>
</tr><tr>
<td>担当者氏名(漢字)</td><td>：</td><td><input type="hidden" name="tantoushakanji" value=tantoushakanji><%=tantoushakanji %></td>
</tr><tr>
<td>担当者氏名(カタカナ)</td><td>：</td><td><input type="hidden" name="tantoushakatakana" value=tantoushakatakana><%=tantoushakatakana %></td>
</tr><tr>
<td>担当者電話番号</td><td>：</td><td><input type="hidden" name="tantoushatel" value=tantoushatel><%=tantoushatel %></td>
</tr><tr>
<td>担当者FAX番号</td><td>：</td><td><input type="hidden" name="tantoushafax" value=tantoushafax><%=tantoushafax %></td>
</tr><tr>
<td>給与支払日</td><td>：</td><td><input type="hidden" name="kyuyo" value=kyuyo><%=kyuyo %></td>
</tr><tr>
<td>届先区分</td><td>：</td><td>0002</td>
</tr>
</table>

<br><br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="編集完了" />
</form>
<form action="B037.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</body>
</div>
</html>