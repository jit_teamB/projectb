<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>協賛企業情報入力</title>
</head>

<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
				<div align = "center">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業削除</a></li>

					</ul>
				</div>
				<br> <br>
<form action="B034.jsp" method="post">
<div style="text-align:center">
新規の協賛企業情報を入力してください。
<br><br>
<table align="center">
<tr>
<td>会社名(漢字)</td><td>：</td><td><input type="text" name="kaishaname"></td>
</tr><tr>
<td>会社名(カタカナ)</td><td>：</td><td><input type="text" name="kaishakana"></td>
</tr>
<tr>
<td>担当部署</td><td>：</td><td><input type="text" name="tantobusho"></td>
</tr>
<tr>
<td>担当者氏名(漢字)</td><td>：</td><td><input type="text" name="tantoshimei"></td>
</tr>
<tr>
<td>担当者氏名(カタカナ)</td><td>：</td><td><input type="text" name="tantokana"></td>
</tr>
<tr>
<td>担当者電話番号</td><td>：</td><td><input type="text" name="tantotel"></td>
</tr>
<tr>
<td>担当者FAX番号</td><td>：</td><td><input type="text" name="tantofax"></td>
</tr>
<tr>
<td>会社郵便番号</td><td>：</td><td><input type="text" name="kaishapost"></td>
</tr>
<tr>
<td>会社住所</td><td>：</td><td><input type="text" name="kaishaadd"></td>
</tr>
<tr>
<td>会社電話番号</td><td>：</td><td><input type="text" name="kaishatel"></td>
</tr>
<tr>
<td>会社FAX番号</td><td>：</td><td><input type="text" name="kaishafax"></td>
</tr>
<tr>
<td>給与支払日</td><td>：</td><td><input type="text" name="kyuyo"></td>
</tr>
<tr>
<td>届先区分</td><td>：</td><td><input type="text" name="todokesakikubun"></td>
</tr>
</table>
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="登録" />
</form>
<br>
<form action="B018B.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>