<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,DTO.shohinDTO" %>
<%
ArrayList<shohinDTO> shohinList = (ArrayList<shohinDTO>)request.getAttribute("shohinList");
shohinDTO shohin = new shohinDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>編集内容確認画面</title>
</head>

<div style="text-align:center">
<body>
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<body>
<form action="select_action" method="post">
<br><br>
編集商品情報
<br><br>
<table align="center">
<tr>
<td>商品番号　</td><td>：</td><td>　<%=shohin.getShohinno() %>
</tr><tr>
<td>商品名　</td><td>：</td><td>　<%=shohin.getShohinmei() %>
</tr><tr>
<td>柄　</td><td>：</td><td>　<%=shohin.getGara() %>
</tr><tr>
<td>色　</td><td>：</td><td>　<%=shohin.getColor() %>
</tr><tr>
<td>サイズ　</td><td>：</td><td>　<%=shohin.getSize() %>
</tr><tr>
<td>販売開始日　</td><td>：</td><td>　<%=shohin.getStartdate() %>
</tr><tr>
<td>販売終了日　</td><td>：</td><td>　<%=shohin.getEnddate() %>
</tr><tr>
<td>価格　</td><td>：</td><td>　<%=shohin.getPrice() %>
</tr><tr>
<td>契約店番号　</td><td>：</td><td>　<%=shohin.getKeiyakuten() %>

</tr><tr>
</table>
<br><br>
編集完了しました。
<br><br>
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="TOPへ戻る" />
</form>
</body>
</div>
</html>