<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.ArrayList,DTO.KeiyakutenDTO"%>
<%
ArrayList<KeiyakutenDTO> keiyakuList = (ArrayList<KeiyakutenDTO>)request.getAttribute("keiyakuList");
KeiyakutenDTO keiyaku = new KeiyakutenDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" /><title>契約店情報登録完了</title>
</head>
<div style="text-align:center">
<body>
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<body>
<form action="select_action" method="post">
登録契約店情報
<br><br>
<table align="center">
<tr>
<td>契約店番号　</td><td>：　</td><td><%=keiyaku.getKeiyakutenno() %></td>
</tr><tr>
<td>契約店名(漢字)　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenmei() %></td>
</tr><tr>
<td>契約店名(カタカナ)　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenkana() %></td>
</tr><tr>
<td>契約店電話番号　</td><td>：</td><td>　<%=keiyaku.getKeiyakutendenwano() %></td>
</tr><tr>
<td>契約店FAX番号　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenfaxno() %></td>
</tr><tr>
<td>契約店郵便番号　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenpost() %></td>
</tr><tr>
<td>契約店住所　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenad() %></td>
</tr><tr>
<td>契約店担当者　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenmei() %></td>
</tr><tr>
</table>
<br><br>
会員情報の登録を完了しました。
<br><br>
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</body>
</html>