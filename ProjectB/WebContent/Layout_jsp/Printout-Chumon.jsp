<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@page
	import="java.util.ArrayList,DTO.ChumonuketukehyoDTO,Chumonuketuke.ChumonuketukehyoDAO"%>
<%
	int shokei = (Integer) session.getAttribute("shokei");
	ArrayList<ChumonuketukehyoDTO> ShohinList = (ArrayList<ChumonuketukehyoDTO>) session
			.getAttribute("ShohinArray");
	ArrayList<ChumonuketukehyoDTO> KaiinList = (ArrayList<ChumonuketukehyoDTO>) session
			.getAttribute("KaiinArray");
%>
<head>
<title>注文受付票</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet"
	type="text/css" />
</head>
<body onload="window.print();">
	<%
		int kubun = 0;
		for (ChumonuketukehyoDTO com : ShohinList) {
			if (com.getSiharaikubun() != 0) {
	%>
	<%
		kubun = com.getSiharaikubun();
	%>
	<%
		break;
			}
	%>
	<%
		}
	%>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<br> <br> <font size=2>

							<div id="box425">
								<font size=3>はるみ通信販売＜＜注文受付票＞＞</font>
							</div>
							<div id="box100">受注担当者</div>
							<div id="box100"></div>
							<div id="box100">注文受付</div>
							<div id="box100">
								<%=ShohinList.get(0).getUpdate()%>
							</div>
							<div id="box50">番号</div>
							<div id="box100">
								<%=ShohinList.get(0).getJuchuno()%>
							</div> <br>
							<div id="box525">
								ご依頼主<font size=1>(協賛企業名・会員氏名・社員番号・連絡先電話番号は必ず確認し記入する)</font>
							</div>
							<div id="box250z">
								<div align=left>
									&nbsp;&nbsp;お届け先住所 &nbsp;&nbsp;: &nbsp;&nbsp;〒
									<%
										for (ChumonuketukehyoDTO com : ShohinList) {
											if (!com.getTodokesakipost().equals(null)) {
									%>
									<%=com.getTodokesakipost()%>
									<%
										break;
											}
									%>
									<%
										}
									%>
								</div>
							</div>
							<div id="box100">届け先区分</div>
							<div id="box100">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (com.getTodokesakikubun() != 0) {
								%>
								<%
									if (com.getTodokesakikubun() == 1) {
								%>
								自宅
								<%
									break;
											} else if (com.getTodokesakikubun() == 2) {
								%>会社
								<%
									break;
											} else if (com.getTodokesakikubun() == 3) {
								%>その他
								<%
									break;
											}
								%>
								<%
									}
								%>
								<%
									}
								%>
							</div> <br>
							<div id="box75">協賛企業</div>
							<div id="box150">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (!com.getKaishamei().equals(null)) {
								%>
								<%=com.getKaishamei()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box75">会社番号</div>
							<div id="box75">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (com.getKaishano() != 0) {
								%>
								<%=com.getKaishano()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box50">所属</div>
							<div id="box100">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (!com.getShozokubusho().equals(null)) {
								%>
								<%=com.getShozokubusho()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box450">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (!com.getTodokesakiad().equals(null)) {
								%>
								<%=com.getTodokesakiad()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div> <br>
							<div id="box125">会員氏名</div>
							<div id="box150">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (!com.getKanji().equals(null)) {
								%>
								<%=com.getKanji()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box100">社員番号</div>
							<div id="box150">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (!com.getShainno().equals(null)) {
								%>
								<%=com.getShainno()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>

							<div id="box175">留守の時</div>
							<div id="box275">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (!com.getRusu().equals(null)) {
								%>
								<%=com.getRusu()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div> <br>
							<div id="box125">自宅電話番号</div>
							<div id="box150">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (!com.getTel().equals(null)) {
								%>
								<%=com.getTel()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box100">会社電話番号</div>
							<div id="box150">
								<%
									for (ChumonuketukehyoDTO com : KaiinList) {
										if (!com.getKaishatel().equals(null)) {
								%>
								<%=com.getKaishatel()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box75">氏名</div>
							<div id="box150">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (!com.getTodokesakimei().equals(null)) {
								%>
								<%=com.getTodokesakimei()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box100">電話番号</div>
							<div id="box125">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (!com.getTodokesakitel().equals(null)) {
								%>
								<%=com.getTodokesakitel()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div> <br> <br> <br> <br> <br> <br> <br>
							<br> <br> <br> <br>

							<div align="left">&nbsp;&nbsp;&nbsp;&nbsp;(注意)色・柄・サイズを指定する商品は、記入漏れのないように確認してください。</div>
							<div id="box75">商品番号</div>
							<div id="box175">商品名</div>
							<div id="box40">配送</div>
							<div id="box30">色</div>
							<div id="box40">柄</div>
							<div id="box40">
								<font size="1">サイズ</font>
							</div>
							<div id="box75">単価</div>
							<div id="box50">単位</div>
							<div id="box50">数量</div>
							<div id="box75">金額</div>
							<div id="box75">他・実費</div>
							<div id="box75">合計金額</div>
							<div id="box75">補足</div>
							<div id="box50">発送先</div>
							<div id="box50">発送日</div> <br> <%
 	int goukeikingaku = 0;
 	int goukeijip = 0;
 	int goukeigoukei = 0;
 	int goukeisuryo = 0;
 	int jip;
 	int kin;
 	for (ChumonuketukehyoDTO com : ShohinList) {
 		if (com.getShohinno() != 0) {
 %>
							<div align="center">
								<div id="box75s">
									<%=com.getShohinno()%>
								</div>
								<div id="box175s">
									<font size="1"> <%=com.getShohinmei()%></font>
								</div>
								<div id="box40s">
									<%=com.getHaisosize()%>
								</div>
								<div id="box30s">
									<font size="1"> <%=com.getColor()%></font>
								</div>
								<div id="box40s">
									<font size="1"> <%=com.getGara()%></font>
								</div>
								<div id="box40s">
									<font size="1"> <%=com.getSize()%></font>
								</div>
								<div id="box75s">
									<%=com.getPrice()%>

								</div>
								<div id="box50s">
									<%=com.getTani()%>
								</div>
								<div id="box50s">
									<%=com.getSuryo()%>
									<%
										goukeisuryo = goukeisuryo + com.getSuryo();
									%>
								</div>
								<div id="box75s">
									<%
										kin = (int) ((com.getPrice() * com.getSuryo() * 1.08));
									%>
									<%=kin%>
									<%
										goukeikingaku = goukeikingaku + kin;
									%>
								</div>
								<div id="box75s">
									<%
										if (kubun == 3) {
													jip = (int) (kin * 0.03);
									%>
									<%=jip%>
									<%
										goukeijip = goukeijip + jip;
									%>
									<%
										} else {
													jip = 0;
									%>
									<%=jip%>
									<%
										}
									%>
								</div>
								<div id="box75s">
									<%
										int goukei = kin + jip;
									%>
									<%=goukei%>
									<%
										goukeigoukei = goukei + goukeigoukei;
									%>
								</div>
								<div id="box75s"></div>
								<div id="box50s"><%=com.getKeiyakutenno()%></div>
								<div id="box50s"></div>
							</div> <br> <br> <br> <%
 	}
 	}
 %>

							<div id="box475n"></div>
							<div id="box50">合計</div>
							<div id="box50"><%=goukeisuryo%></div>
							<div id="box75"><%=goukeikingaku%></div>
							<div id="box75"><%=goukeijip%></div>
							<div id="box75"><%=goukeigoukei%></div>
							<div id="box175a"></div> <br> <br> <br> <br>
							<br> <br> <br> <br>
							<div id="box75">支払回数</div>
							<div id="box50">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (com.getBunkatukaisu() != 0) {
								%>
								<%=com.getBunkatukaisu()%>
								<%
									break;
										}
								%>
								<%
									}
								%>
							</div>
							<div id="box75">
								<font size=1>振込先</font>
							</div>
							<div id="box150z">
								アカネ銀行 西成支店<br>20965134
							</div>
							<div id="box50">発注</div>
							<div id="box50">日付</div>
							<div id="box150"></div>
							<div id="box50">担当者</div>
							<div id="box100"></div>
							<div id="box150">ボーナス控除手数料</div>
							<div id="box75"></div> <br>
							<div id="box125">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (com.getSiharaikubun() != 0) {
								%>
								<%
									if (com.getSiharaikubun() == 1) {
								%>
								<font style="border: solid">銀行振込</font>
								<%
									break;
											} else if (com.getSiharaikubun() == 2) {
								%>銀行振込
								<%
									break;
											} else if (com.getSiharaikubun() == 3) {
								%>銀行振込
								<%
									break;
											}
								%>
								<%
									}
								%>
								<%
									}
								%>

							</div>
							<div id="box75">
								<font size=1>振込元</font>
							</div>
							<div id="box150z"></div>
							<div id="box50">請求</div>
							<div id="box50">区分</div>
							<div id="box300"></div>
							<div id="box150">請求総合計額</div>
							<div id="box75"></div> <br>
							<div id="box125">
								<%
									for (ChumonuketukehyoDTO com : ShohinList) {
										if (com.getSiharaikubun() != 0) {
								%>
								<%
									if (com.getSiharaikubun() == 1) {
								%>
								給与控除
								<%
									break;
											} else if (com.getSiharaikubun() == 2) {
								%><font style="border: solid">給与控除</font>
								<%
									break;
											} else if (com.getSiharaikubun() == 3) {
								%><font style="border: solid">給与控除</font>
								<%
									break;
											}
								%>
								<%
									}
								%>
								<%
									}
								%>

							</div>
							<div id="box75">
								<font size=1>控除年月</font>
							</div>
							<div id="box150"></div>
							<div id="box50"></div>
							<div id="box50">日付</div>
							<div id="box150"></div>
							<div id="box50">担当者</div>
							<div id="box100"></div>
							<div id="box100">支払期日</div>
							<div id="box125"></div> <br>
							<div id="box125">ボーナス控除</div>
							<div id="box75">
								<font size=1>控除年月</font>
							</div>
							<div id="box150"></div>
							<div id="box50">入金</div>
							<div id="box50">日付</div>
							<div id="box150"></div>
							<div id="box50">担当者</div>
							<div id="box100"></div>
							<div id="box100">未入金催促日付</div>
							<div id="box125"></div> <br> <br> <br> <br>
						</font>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</body>
</html>