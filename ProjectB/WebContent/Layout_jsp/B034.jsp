<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
//文字コード
request.setCharacterEncoding("utf8");
//htmlから取得
String kaishamei = request.getParameter("kaishaname");
String kaishakana = request.getParameter("kaishakana");
String tantoubusho = request.getParameter("tantobusho");
String tantoshimei = request.getParameter("tantoshimei");
String tantokana = request.getParameter("tantokana");
int tantotel = Integer.parseInt(request.getParameter("tantotel"));
int tantofax = Integer.parseInt(request.getParameter("tantofax"));
int kaishapost = Integer.parseInt(request.getParameter("kaishapost"));
String kaishaadd = request.getParameter("kaishaadd");
int kaishatel = Integer.parseInt(request.getParameter("kaishatel"));
int kaishafax = Integer.parseInt(request.getParameter("kaishafax"));
int kyuyo = Integer.parseInt(request.getParameter("kyuyo"));
String todokesaki = request.getParameter("todokesakikubun");

%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>協賛企業情報登録確認</title>
</head>


<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<form action="B033" method="post">
登録する協賛企業情報を確認してください。
<br><br>
<table align="center">
<tr>
<td>会社名(漢字)</td><td>：</td><td><input type="hidden" name="kaishamei" value=kaishamei><%=kaishamei %></td>
</tr><tr>
<td>会社名(カタカナ)</td><td>：</td><td><input type="hidden" name="kaishakana" value=kaishakana><%=kaishakana %></td>
</tr><tr>
<td>会社郵便番号</td><td>：</td><td><input type="hidden" name="kaishapost" value=kaishapost><%=kaishapost %></td>
</tr><tr>
<td>会社住所</td><td>：</td><td><input type="hidden" name="kaishaadd" value=kaishaadd><%=kaishaadd %></td>
</tr><tr>
<td>会社電話番号</td><td>：</td><td><input type="hidden" name="kaishatel" value=kaishatel><%=kaishatel %></td>
</tr><tr>
<td>担当部署</td><td>：</td><td><input type="hidden" name="tantoubusho" value=tantoubusho><%=tantoubusho %></td>
</tr><tr>
<td>担当者氏名(漢字)</td><td>：</td><td><input type="hidden" name="tantoshimei" value=tantoshimei><%=tantoshimei %></td>
</tr><tr>
<td>担当者氏名(カタカナ)</td><td>：</td><td><input type="hidden" name="tantokana" value=tantokana><%=tantokana %></td>
</tr><tr>
<td>担当者電話番号</td><td>：</td><td><input type="hidden" name="tantotel" value=tantotel><%=tantotel %></td>
</tr><tr>
<td>担当者FAX番号</td><td>：</td><td><input type="hidden" name="tantofax" value=tantofax><%=tantofax %></td>
</tr><tr>
<td>給与支払日</td><td>：</td><td><input type="hidden" name="kyuyo" value=kyuyo><%=kyuyo %>日</td>
</tr><tr>
<td>届先区分</td><td>：</td><td><input type="hidden" name="todokesaki" value=2><%=todokesaki %></td>
</tr>
</table>

<br><br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="登録完了" />
</form>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="B033.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>