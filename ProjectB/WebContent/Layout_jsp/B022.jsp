<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,DTO.KaiinDTO" %>
<%
ArrayList<KaiinDTO> kaiinList = (ArrayList<KaiinDTO>)request.getAttribute("kaiinList");
KaiinDTO kaiin = new KaiinDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>会員情報編集</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>
<div style="text-align:center">

<br><br>
<form action="B020A" method="post">
編集する会員情報を選択し、入力してください。

<br><br><table align="center">
<tr>
<td><input type="text" name="shainno" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>氏名(漢字)　</td><td>：</td><td>　<%=kaiin.getkanji() %>
<td><input type="text" name="kanji" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>氏名(カタカナ)　</td><td>：</td><td>　<%=kaiin.getKatakana() %>
<td><input type="text" name="katakana" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>電話番号　</td><td>：</td><td>　<%=kaiin.getDenwano() %>
<td><input type="text" name="tel" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>FAX番号　</td><td>：</td><td>　<%=kaiin.getFaxno() %>
<td><input type="text" name="fax" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>郵便番号　</td><td>：</td><td>　<%=kaiin.getPostcode() %>
<td><input type="text" name="post" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会員住所　</td><td>：</td><td>　<%=kaiin.getAddress() %>
<td><input type="text" name="addresst" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社名　</td><td>：</td><td>　<%=kaiin.getKaishamei() %>
<td><input type="text" name="kaishamei" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>所属部署　</td><td>：</td><td>　<%=kaiin.getShozokubusho() %>
<td><input type="text" name="busho" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>備考　</td><td>：</td><td>　<%=kaiin.getMemo() %>
<td><input type="text" name="memo" id="textforscb3"  />
</tr>
</table>
<br>
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="編集" />
</form>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="B002.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>