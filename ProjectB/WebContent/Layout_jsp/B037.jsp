<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,updateKyosan.SearchkyosanDao,DTO.KyosankigyouDTO"%>
<%
ArrayList<KyosankigyouDTO> kyosanList = (ArrayList<KyosankigyouDTO>)request.getAttribute("kyosanList");
KyosankigyouDTO kyosan = new KyosankigyouDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>協賛企業情報入力</title>
</head>
<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
				<div align = "center">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業削除</a></li>

					</ul>
				</div>
				<br> <br>
<div style="text-align:center">
編集する商品情報を選択し、入力してください。
<br><br>
<form action="../B037" method="post">
<table align="center">
<tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社名(漢字)　</td><td>：</td><td>　<%=kyosan.getKaishamei() %>
<td><input type="text" name="kaishamei" id="textforscb3"  />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社名(カタカナ)　</td><td>：</td><td>　<%=kyosan.getKaishameikana() %>
<td><input type="text" name="kaishakana" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社郵便番号　</td><td>：</td><td>　<%=kyosan.getPostcode() %>
<td><input type="text" name="postcode" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社住所　</td><td>：</td><td>　<%=kyosan.getAddress() %>
<td><input type="text" name="address" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社電話番号　</td><td>：</td><td>　<%=kyosan.getKaishatel() %>
<td><input type="text" name="kaishatel" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>会社FAX番号番号　</td><td>：</td><td>　<%=kyosan.getKaishatel() %>
<td><input type="text" name="kaishafax" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>担当部署　</td><td>：</td><td>　<%=kyosan.getTantoubusho() %>
<td><input type="text" name="tantoubusho" id="textforscb3"/>
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>担当者氏名(漢字)　</td><td>：</td><td>　<%=kyosan.getTantoushakanji() %>
<td><input type="text" name="tantoushakanji" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>担当者氏名(カタカナ)　</td><td>：</td><td>　<%=kyosan.getTantoushakatakana() %>
<td><input type="text" name="tantoushakatakana" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>担当者電話番号　</td><td>：</td><td>　<%=kyosan.getTantoushadenwano() %>
<td><input type="text" name="tantoushatel" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>担当者FAX番号　</td><td>：</td><td>　<%=kyosan.getTantoushafaxno() %>
<td><input type="text" name="tantoushafax" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>給与支払日　</td><td>：</td><td>　<%=kyosan.getKyuuyosiharai() %>
<td><input type="text" name="kyuyo" id="textforscb3" />
</tr><tr>
</table>

<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="編集" />
</form>
<form action="B036.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div></div>
</body>
</html>