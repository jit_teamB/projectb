
<%@page import="java.util.ArrayList"%>
<%@page import="DTO.KaiinDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet" type="text/css" />
<title>会員情報確認</title>
</head>
<body>
<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
				<div align = "center">
				<br>				はるみ通信販売管理システム
									<br>
						<p>
						<div id="nav">
							<ul>
 								<li><a target="_top" href="/ProjectB/Layout_jsp/B001.jsp">TOP</a></li>
							</ul>
						</div>
<br><br>
<%
ArrayList<KaiinDTO> kaiin=(ArrayList<KaiinDTO>)request.getAttribute("member");
for(KaiinDTO i : kaiin){
%>
登録会員情報を確認してください。
<br><br>
 <div align="center">
 <% 	String kaiinmei = (String) session.getAttribute("kaiinmei");%>
 入力された社員名：<%=kaiinmei %>
 <br><br><br><br>
<table>
<tr>
<td>社員番号　</td><td>：</td><td><%= i.getShainno() %>
</tr><tr>
<td>氏名(漢字)　</td><td>：</td><td><%= i.getKanji() %>
</tr><tr>
<td>氏名(カタカナ)　</td><td>：</td><td>　<%= i.getKatakana() %>
</tr><tr>
<td>電話番号　</td><td>：</td><td>　<%=i.getDenwano() %>
</tr><tr>
<td>FAX番号　</td><td>：</td><td>　<%=i.getFaxno() %>
</tr><tr>
<td>郵便番号　</td><td>：</td><td>　<%=i.getpostcode() %>
</tr><tr>
<td>会員住所　</td><td>：</td><td>　<%=i.getAddress()%>
</tr><tr>
<td>会社名　</td><td>：</td><td>　<%=i.getKaishamei() %>
</tr><tr>
<td>所属部署　</td><td>：</td><td>　<%=i.getShozokubusho() %>
</tr><tr>
<td>入会日　</td><td>：</td><td>　<%=i.getNyukaidate() %>
</tr><tr>
<td>備考　</td><td>：</td><td>　<%=i.getMemo() %>
<%} %>

</table>
<form action="Layout_jsp/B003.jsp">
<div id="box275d"></div>
<div id="box200d"><input type="submit" style="width: 40%;padding: 8px;font-size:20px;" value="次へ" /></div>
</form>
<form action="Layout_jsp/B002.jsp">
<div id="boxv50d"></div>
<div id="box200d"><input type="submit" style="width: 40%;padding: 8px;font-size:20px;" value="戻る" /></div>
<div id="box275d"></div>
</form>

</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
