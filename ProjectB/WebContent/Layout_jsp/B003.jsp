<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="juchuno_seisei.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet"
	type="text/css" />
<title>注文受付担当者選択</title>
</head>
<body>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<br> はるみ通信販売管理システム <br>
						<p>
						<div id="nav">
							<ul>
								<li><a target="_top" href="/ProjectB/Layout_jsp/B001.jsp">TOP</a></li>
							</ul>
						</div>
						<br> お客様対応を選択してください。
						<form action="/ProjectB/juchuno_seisei" method="post">
							<br> <br> <input type="submit"
								style="width: 30%; padding: 8px; font-size: 20px;" value="注文受付">
						</form>
						<br>
						<form action="/ProjectB/Layout_jsp/B011.jsp" method="post">
							<input type="button"
								style="width: 30%; padding: 8px; font-size: 20px;"
								value="お問い合わせ対応">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
