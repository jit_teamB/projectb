<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>会員情報DBシステム</title>
</head>

<div style="text-align:center">
<body>
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="B001.html">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報削除</a></li>

					</ul>
				</div>
				<br> <br>
		</div>
<br>
会員情報の登録、編集、削除を選択してください。
<br><br><br>
<form action="B019.jsp" method="post">
<input type="submit" style="width: 30%;padding: 8px;font-size:20px;" value="会員情報を登録" />
</form>
<br><br>
<form action="B002A.jsp" method="post">
<input type="submit" style="width: 30%;padding: 8px;font-size:20px;" value="会員情報を編集" />
</form>
<br><br>
<form action="B002B.jsp" method="post">
<input type="submit" style="width: 30%;padding: 8px;font-size:20px;" value="会員情報を削除"/>
</form>
</body>
</html>
