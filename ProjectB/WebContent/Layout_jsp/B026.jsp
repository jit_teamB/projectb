<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
//文字コード
request.setCharacterEncoding("utf8");
//htmlから取得
int shohinno=Integer.parseInt(request.getParameter("shohinno"));
String shohinmei = request.getParameter("shohinmei");
String shohinsize = request.getParameter("shohinsize");
String shohingara = request.getParameter("shohingara");
String shohincolor = request.getParameter("shohincolor");
String shohintype = request.getParameter("shohintype");
String shohintani = request.getParameter("shohintani");
int atsukai = Integer.parseInt(request.getParameter("toriatsukai"));
String startday = request.getParameter("startday");
String endday = request.getParameter("endday");
int price = Integer.parseInt(request.getParameter("price"));
int keiyakuno = Integer.parseInt(request.getParameter("keiyakuno"));
String memo = request.getParameter("memo");
int besso = Integer.parseInt(request.getParameter("besso"));
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>新規商品情報確認</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<form action="B025" method="post">
登録する商品情報を確認してください。
<br><br>
<table align="center">
<tr>
<td>商品番号</td><td>：</td><td><input type="hidden" name="shohinno" value=shohinno><%=shohinno %></td>
</tr><tr>
<td>商品名</td><td>：</td><td><input type="hidden" name="shohinmei" value=shohinmei><%=shohinmei %></td>
</tr><tr>
<td>重量・サイズ</td><td>：</td><td><input type="hidden" name="shohinsize" value=shohinsize><%=shohinsize %></td>
</tr><tr>
<td>柄</td><td>：</td><td><input type="hidden" name="shohingara" value=shohingara><%=shohingara %></td>
</tr><tr>
<td>色</td><td>：</td><td><input type="hidden" name="shohincolor" value=shohincolor><%=shohincolor %></td>
</tr><tr>
<td>単位</td><td>：</td><td><input type="hidden" name="shohintani" value=shohintani><%=shohintani %></td>
</tr><tr>
<td>取り扱い状況</td><td>：</td><td><input type="hidden" name="atsukai" value=atsukai><%=atsukai %></td>
</tr><tr>
<td>販売開始日</td><td>：</td><td><input type="hidden" name="startday" value=startday><%=startday %></td>
</tr><tr>
<td>販売終了日</td><td>：</td><td><input type="hidden" name="endday" value=endday><%=endday %></td>
</tr><tr>
<td>商品価格</td><td>：</td><td><input type="hidden" name="price" value=price><%=price %>円</td>
</tr><tr>
<td>契約店</td><td>：</td><td><input type="hidden" name="keiyakuno" value=keiyakuno><%=keiyakuno %></td>
</tr><tr>
<td>備考</td><td>：</td><td><input type="hidden" name="memo" value=memo><%=memo %></td>
</tr>
</table>


<br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="登録完了" />
</form>
<form action="B025.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>
