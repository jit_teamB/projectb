<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
        //文字コード
		request.setCharacterEncoding("utf8");

		//htmlから
      	String keiyakuno = request.getParameter("keiyakuno");
      	String keiyakuname = request.getParameter("keiyakuname");
      	String keiyakukana = request.getParameter("keiyakukana");
      	int keiyakutel = Integer.parseInt(request.getParameter("keiyakutel"));
      	int keiyakufax = Integer.parseInt(request.getParameter("keiyakufax"));
      	String keiyakupost = request.getParameter("keiyakupost");
      	String keiyakuadd = request.getParameter("keiyakuadd");
      	String keiyakutanto = request.getParameter("keiyakutanto");
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" /><title>契約店情報登録</title>
</head>
<body>
<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<body>
<form action="B040" method="post">
登録する契約店情報を確認してください。
<br><br>
<table align="center">
<tr>
<td>契約店番号　</td><td>：　</td><td>　<input type="hidden" name="keiyakuno" value=keiyakuno><%=keiyakuno %></td>
</tr><tr>
<td>契約店名(漢字)　</td><td>：</td><td>　<input type="hidden" name="keiyakuname" value=keiyakuname><%=keiyakuname %></td>
</tr><tr>
<td>契約店名(カタカナ)　</td><td>：</td><td>　<input type="hidden" name="keiyakukana" value=keiyakukana><%=keiyakukana %></td>
</tr><tr>
<td>契約店電話番号　</td><td>：</td><td>　<input type="hidden" name="keiyakutel" value=keiyakutel><%=keiyakutel %></td>
</tr><tr>
<td>契約店FAX番号　</td><td>：</td><td>　<input type="hidden" name="keiyakufax" value=keiyakufax><%=keiyakufax %></td>
</tr><tr>
<td>契約店郵便番号　</td><td>：</td><td>　<input type="hidden" name="keiyakupost" value=keiyakupost><%=keiyakupost %></td>
</tr><tr>
<td>契約店住所　</td><td>：</td><td>　<input type="hidden" name="keiyakuadd" value=keiyakuadd><%=keiyakuadd %></td>
</tr><tr>
<td>契約店担当者　</td><td>：</td><td>　<input type="hidden" name="keiyakutento" value=keiyakutanto><%=keiyakutanto %></td>
</tr><tr>
</table>
<br>
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="登録完了" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="戻る" />

</form>
</body>
</html>
