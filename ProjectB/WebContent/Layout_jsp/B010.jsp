<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@page
	import="java.util.ArrayList,DTO.ChumonuketukehyoDTO,Chumonuketuke.ChumonuketukehyoDAO"%>
<%
	int shokei = (Integer) session.getAttribute("shokei");
	ArrayList<ChumonuketukehyoDTO> ShohinList = (ArrayList<ChumonuketukehyoDTO>) session
			.getAttribute("ShohinArray");
	ArrayList<ChumonuketukehyoDTO> KaiinList = (ArrayList<ChumonuketukehyoDTO>) session
			.getAttribute("KaiinArray");
%>
<head>
<title>発注書</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout1.css" rel="stylesheet"
	type="text/css" />
</head>
<body onload="window.print();">
	<%
		int kubun = 0;
		for (ChumonuketukehyoDTO com : ShohinList) {
			if (com.getSiharaikubun() != 0) {
	%>
	<%
		kubun = com.getSiharaikubun();
	%>
	<%
		break;
			}
	%>
	<%
		}
	%>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<%
							for (ChumonuketukehyoDTO kei : ShohinList) {
								if (kei.getShuturyoku() == 1) {
						%>
						<%
							kei.setShuturyoku(0);
						%>
						<%
							}
						%>
						<%
							}
						%>
						<%
							for (ChumonuketukehyoDTO kei : ShohinList) {
								if (kei.getKeiyakutenno() != 0 && kei.getShuturyoku() == 0) {
									int x = kei.getKeiyakutenno();
						%>
						<div class="page-break-before">
							<p>
								<br> <br> <font size=2> <font size=3>発注書（依頼請書）</font><br>
									<br>
									<div id="box695"></div>
									<div id="box100">ご依頼主</div>
									<div id="box200">
										<%
											for (ChumonuketukehyoDTO com : KaiinList) {
														if (com.getKanji()!=null) {
										%>
										<%=com.getKanji()%>
										<%
											break;
														}
										%>
										<%
											}
										%>
									</div> <br>
									<div id="box235n">
										<u> <%
 	for (ChumonuketukehyoDTO com : ShohinList) {
 				if (com.getKeiyakutenno() == x) {
 %> <%=com.getKeiyakutenmei()%> <%
 	break;
 				}
 %> <%
 	}
 %>
										</u>御中
									</div>
									<div id="box235n">
										<font size=1><u>契約販売店様番号<%=x%></u></font>
									</div>
									<div id="box525">
										<font size=1>枠内：注文受付票のお届け先コピー欄。 上のご依頼主は会員氏名を必ず記入のこと。</font>
									</div> <br>
									<div id="box235n">下記内容にて、発注いたします</div>
									<div id="box235n">
										<u>発送日:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
											size=1>(FAX送信日)</font></u>
									</div>
									<div id="box100">受注担当者</div>
									<div id="box100"></div>
									<div id="box75">注文受付</div>
									<div id="box100">
										<%=ShohinList.get(0).getUpdate()%></div>
									<div id="box50">番号</div>
									<div id="box100"><%=ShohinList.get(0).getJuchuno()%></div> <br>
									<div id="box235n2">
										<font size=1>なお、納期などの諸条件は<br>基本契約内容に従うものとします。
										</font>
									</div>
									<div id="box235n"></div>
									<div id="box250z">
										&nbsp;&nbsp;お届け先住所&nbsp;&nbsp;: &nbsp;&nbsp; : 〒
										<%
											for (ChumonuketukehyoDTO com : ShohinList) {
														if (com.getTodokesakipost()!=null) {
										%>
										<%=com.getTodokesakipost()%>
										<%
											break;
														}
										%>
										<%
											}
										%>
									</div>
									<div id="box125">届け先区分</div>
									<div id="box150">
										<%
											for (ChumonuketukehyoDTO com : ShohinList) {
														if (com.getTodokesakikubun() != 0) {
										%>
										<%
											if (com.getTodokesakikubun() == 1) {
										%>
										自宅
										<%
											break;
															} else if (com.getTodokesakikubun() == 2) {
										%>会社
										<%
											break;
															} else if (com.getTodokesakikubun() == 3) {
										%>その他
										<%
											break;
															}
										%>
										<%
											}
										%>
										<%
											}
										%>
									</div> <br>
									<div id="box235n1">
										<font size=1>納品条件が基本契約と異なる場合は、</font>
									</div>
									<div id="box235n">
										<font size=4>はるみ通信販売株式会社</font>
									</div>
									<div id="box525">
										<%
											for (ChumonuketukehyoDTO com : ShohinList) {
														if (com.getTodokesakiad()!=null) {
										%>
										<%=com.getTodokesakiad()%>
										<%
											break;
														}
										%>
										<%
											}
										%>
									</div> <br>
									<div id="box235n2">
										<font size=1>必ず弊社担当までご連絡願います。</font>
									</div>
									<div id="box235n2">
										<font size=1>営業部発注 <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
											印<br> Tel:0541341232 Fax:0541341233
										</font>
									</div>
									<div id="box200">留守の時</div>
									<div id="box325">
										<%
											for (ChumonuketukehyoDTO com : ShohinList) {
														if (com.getRusu()!=null) {
										%>
										<%=com.getRusu()%>
										<%
											break;
														}
										%>
										<%
											}
										%>
									</div> <br>
									<div id="box235n"></div>
									<div id="box235n2">
										<font size=1>色、柄、サイズ指定商品は<br>必ず弊社カタログをご確認ください。
										</font></u>
									</div>
									<div id="box100">氏名</div>
									<div id="box175">
										<%
											for (ChumonuketukehyoDTO com : ShohinList) {
														if (com.getTodokesakimei()!=null) {
										%>
										<%=com.getTodokesakimei()%>
										<%
											break;
														}
										%>
										<%
											}
										%>
									</div>
									<div id="box100">電話番号</div>
									<div id="box150">
										<%
											for (ChumonuketukehyoDTO com : ShohinList) {
														if (com.getTodokesakitel()!=null) {
										%>
										<%=com.getTodokesakitel()%>
										<%
											break;
														}
										%>
										<%
											}
										%>
									</div> <br> <br> <br> <br> <br> <br> <br>
									<br> <br> <br> <br> <br> <br> <br>
									<br>
									<div id="box75">商品番号</div>
									<div id="box175">商品名</div>
									<div id="box40">配送</div>
									<div id="box40">色</div>
									<div id="box40">柄</div>
									<div id="box50">
										<font size="1">サイズ</font>
									</div>
									<div id="box75">単価</div>
									<div id="box50">単位</div>
									<div id="box50">数量</div>
									<div id="box75">金額</div>
									<div id="box75">他・実費</div>
									<div id="box75">合計金額</div>
									<div id="box75">補足</div>
									<div id="box50">発注先</div>
									<div id="box50">発送日</div> <br> <%
 	int goukeikingaku = 0;
 			int goukeijip = 0;
 			int goukeigoukei = 0;
 			int goukeisuryo = 0;
 			int jip;
 			int kin;
 			for (ChumonuketukehyoDTO com : ShohinList) {
 				if (com.getShohinno() != 0
 						&& x == com.getKeiyakutenno()
 						&& com.getShuturyoku() == 0) {
 %>
									<div align="center">
										<div id="box75s">
											<%=com.getShohinno()%>
										</div>
										<div id="box175s">
											<font size="1"> <%=com.getShohinmei()%></font>
										</div>
										<div id="box40s">
											<%=com.getHaisosize()%>
										</div>
										<div id="box40s">
											<font size="1"> <%=com.getColor()%></font>
										</div>
										<div id="box40s">
											<font size="1"> <%=com.getGara()%></font>
										</div>
										<div id="box50s">
											<font size="1"> <%=com.getSize()%></font>
										</div>
										<div id="box75s">
											<%=com.getPrice()%>
										</div>
										<div id="box50s">
											<%=com.getTani()%>
										</div>
										<div id="box50s">
											<%=com.getSuryo()%>
											<%
												goukeisuryo = goukeisuryo + com.getSuryo();
											%>
										</div>
										<div id="box75s">
											<%
												kin = (int) ((com.getPrice() * com.getSuryo() * 1.08));
											%>
											<%=kin%>
											<%
												goukeikingaku = goukeikingaku + kin;
											%>
										</div>
										<div id="box75s">
											<%
												if (kubun == 3) {
																	jip = (int) (kin * 0.03);
											%>
											<%=jip%>
											<%
												goukeijip = goukeijip + jip;
											%>
											<%
												} else {
																	jip = 0;
											%>
											<%=jip%>
											<%
												}
											%>
										</div>
										<div id="box75s">
											<%
												int goukei = kin + jip;
											%>
											<%=goukei%>
											<%
												goukeigoukei = goukei + goukeigoukei;
											%>
										</div>
										<div id="box75s"></div>
										<div id="box50s"><%=com.getKeiyakutenno()%></div>
										<div id="box50s"></div>
									</div> <br> <br> <br> <%
 	com.setShuturyoku(1);
 %> <%
 	}
 			}
 %>
									<div id="box495n"></div>
									<div id="box50">合計</div>
									<div id="box50"><%=goukeisuryo%></div>
									<div id="box75"><%=goukeikingaku%></div>
									<div id="box75"><%=goukeijip%></div>
									<div id="box75"><%=goukeigoukei%></div>
									<div id="box175a"></div> <br> <br> <br> <br>
									<br> <br> <br>
									<div align=left>
										<div id="box385a">
											<font size=1>備考欄(弊社通信欄)</font>
										</div>
										<div id="box385a">
											<font size=1>摘要欄(契約販売店様通信欄)</font>
										</div>
									</div>
									<div id="box100a">発送方法</div>
									<div align=left>
										<div id="box125a">
											<font size=1> 白ねこダイワ</font>
										</div>
										<br>
										<div id="box385"></div>
										<div id="box385"></div>
										<div id="box100b"></div>
										<div id="box125b">
											<font size=1> 瀬川郵便局</font>
										</div>
										<br>
										<div id="box385"></div>
										<div id="box385"></div>
										<div id="box100b"></div>
										<div id="box125b">
											<font size=1> その他( )</font>
										</div>
										<br>
									</div>
									<div id="box385"></div>
									<div id="box385"></div>
									<div id="box225n3">
										<font size=1>問合せ伝票番号</font>
									</div>
									<div id="box385"></div>
									<div id="box385"></div>
									<div id="box225n3"></div> <br> <br> <br> <br>
									<br> <br> <br> <br> <br> <%
 	}
 	}
 %>
								</font>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
