<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,DTO.shohinDTO" %>
<%
ArrayList<shohinDTO> shohinList = (ArrayList<shohinDTO>)request.getAttribute("shohinList");
shohinDTO shohin = new shohinDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>商品情報編集</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<form action="B028" method="post">
編集する商品情報を選択し、入力してください。
<br><br>
<table align="center">
<tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>商品名　</td><td>：</td><td>　<%=shohin.getShohinmei() %>
<td><input type="text" name="shohinmei" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>柄　</td><td>：</td><td>　<%=shohin.getGara() %>
<td><input type="text" name="gara" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>色　</td><td>：</td><td>　<%=shohin.getColor() %>
<td><input type="text" name="color" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>サイズ　</td><td>：</td><td>　<%=shohin.getSize() %>
<td><input type="text" name="size" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>販売開始日　</td><td>：</td><td>　<%=shohin.getStartdate() %>
<td><input type="text" name="startday" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>販売終了日　</td><td>：</td><td>　<%=shohin.getEnddate() %>
<td><input type="text" name="endday" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>価格　</td><td>：</td><td>　<%=shohin.getPrice() %>
<td><input type="text" name="price" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店番号　</td><td>：</td><td>　<%=shohin.getKeiyakuten() %>
<td><input type="text" name="keiyakutenno" id="textforscb3"  />
</tr><tr>
</tr>
</table>
<br><br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="編集完了" />
</form>
<form action="B004A.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>