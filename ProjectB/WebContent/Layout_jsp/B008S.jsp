<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet"
	type="text/css" />
<title>支払方法選択</title>
</head>
<body>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<br> はるみ通信販売管理システム <br>
						<p>
						<div id="nav">
							<ul>
								<li><a target="_top" href="/ProjectB/Layout_jsp/B001.jsp">TOP</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B004S.jsp">商品番号入力</a></li>
								<li><a>商品詳細選択</a></li>
								<li><a>受注情報確認</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B007S.jsp">配送先選択</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B008S.jsp">支払方法選択</a></li>
								<li><a>注文受付確認</a></li>
							</ul>
						</div>
						<p>支払方法を選択してください</p>
						<br>
						<form action="/ProjectB/siharaihouhouS" method="post">
							<input type="radio" name="shiharai" value="1" checked>銀行振り込み
							<br> <br> <input type="radio" name="shiharai" value="2">一括給与控除
							<br> <br> <br> <input type="radio" name="shiharai"
								value="3">分割給与控除 <br> 支払い回数&nbsp;：&nbsp;<input
								type="number" name="bunkatu" step="1" min="2" max="24" value="2">
							<br> <br> <br> <input type="submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
