<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,DTO.KaiinDTO" %>
<%
ArrayList<KaiinDTO> kaiinList = (ArrayList<KaiinDTO>)request.getAttribute("kaiinList");
KaiinDTO kaiin = new KaiinDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>会員情報登録完了</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>
<div style="text-align:center">
<form action="B019" method="post">
会員情報
<br><br>
<table align="center">
<tr>
<td>社員番号　</td><td>：</td><td><%=kaiin.getShainno() %>
</tr><tr>
<td>氏名(漢字)　</td><td>：</td><td>　<%=kaiin.getKanji() %>
</tr><tr>
<td>氏名(カタカナ)　</td><td>：</td><td>　<%=kaiin.getKatakana() %>
</tr><tr>
<td>電話番号　</td><td>：</td><td>　<%=kaiin.getDenwano() %>
</tr><tr>
<td>FAX番号　</td><td>：</td><td>　<%=kaiin.getFaxno() %>
</tr><tr>
<td>郵便番号　</td><td>：</td><td>　<%=kaiin.getPostcode() %>
</tr><tr>
<td>会員住所　</td><td>：</td><td>　<%=kaiin.getAddress() %>
</tr><tr>
<td>会社名　</td><td>：</td><td>　<%=kaiin.getKaishamei() %>
</tr><tr>
<td>所属部署　</td><td>：</td><td>　<%=kaiin.getShozokubusho() %>
</tr><tr>
<td>入会日　</td><td>：</td><td>　<%=kaiin.getNyukaidate() %>
</tr><tr>
<td>備考　</td><td>：</td><td>　<%=kaiin.getMemo() %>
</table>
<br><br>
上記の会員情報を登録しました。
<br><br>
</form>
<form action="B001.jsp" method="post">
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="TOPへ戻る" />
</form>
</div></div></div></div>
</body>
</html>