<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,DTO.shohinDTO" %>
<%
ArrayList<shohinDTO> shohinList = (ArrayList<shohinDTO>)request.getAttribute("shohinList");
shohinDTO shohin = new shohinDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>商品登録完了画面</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
商品情報
<br><br>
<table align="center">
<tr>
<td>商品番号</td><td>：</td><td><%=shohin.getShohinno() %></td>
</tr><tr>
<td>商品名</td><td>：</td><td><%=shohin.getShohinmei() %></td>
</tr><tr>
<td>重量・サイズ</td><td>：</td><td><%=shohin.getSize() %></td>
</tr><tr>
<td>柄</td><td>：</td><td><%=shohin.getGara() %></td>
</tr><tr>
<td>色</td><td>：</td><td><%=shohin.getColor() %></td>
</tr><tr>
<td>単位</td><td>：</td><td><%=shohin.getTani() %>個</td>
</tr><tr>
<td>取り扱い状況</td><td>：</td><td><%=shohin.getToriatukai() %></td>
</tr><tr>
<td>販売開始日</td><td>：</td><td><%=shohin.getStartdate() %></td>
</tr><tr>
<td>販売終了日</td><td>：</td><td><%=shohin.getEnddate() %></td>
</tr><tr>
<td>商品価格</td><td>：</td><td><%=shohin.getPrice() %></td>
</tr><tr>
<td>契約店</td><td>：</td><td><%=shohin.getKeiyakuten() %></td>
</tr><tr>
<td>備考</td><td>：</td><td><%=shohin.getMemo() %></td>
</tr>
</table>
<br><br>
登録完了しました。
<br><br>
<form action="B001.jsp" method="post">
<input type="submit" style="width: 16%;padding: 8px;font-size:20px;" value="TOPへ戻る" />
</form>
</div></div></div></div>
</body>
</html>