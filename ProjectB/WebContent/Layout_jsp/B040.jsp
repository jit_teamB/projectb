<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>契約店情報入力</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>
<form action="B041.jsp" method="post">
新規の契約店情報を入力してください。
<br>
<table align="center">
<tr>
<td>契約店名(漢字)</td><td>：</td><td><input type="text" name="keiyakuname"></td>
</tr><tr>
<td>契約店名(カタカナ)</td><td>：</td><td><input type="text" name="keiyakukana"></td>
</tr><tr>
<td>契約店電話番号</td><td>：</td><td><input type="text" name="keiyakutel"></td>
</tr><tr>
<td>契約店FAX番号</td><td>：</td><td><input type="text" name="keiyakufax"></td>
</tr><tr>
<td>契約店郵便番号</td><td>：</td><td><input type="text" name="keiyakupost"></td>
</tr><tr>
<td>契約店住所</td><td>：</td><td><input type="text" name="keiyakuadd"></td>
</tr><tr>
<td>契約店担当者</td><td>：</td><td><input type="text" name="keiyakutanto"></td>
</tr><tr>
<td>備考</td><td>：</td><td><input type="text" name="bikou" ></td>
</table>
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="登録" />
</form>
<form action="B018.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div>
</body>
</html>