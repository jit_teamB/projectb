<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.sql.Date" %>
<%
//文字コード
request.setCharacterEncoding("utf8");
//jspから取得
String shohinmei = request.getParameter("shohinmei");
String gara = request.getParameter("gara");
String color = request.getParameter("color");
String size = request.getParameter("size");
String startday = request.getParameter("startday");
String endday = request.getParameter("endday");
int price = Integer.parseInt(request.getParameter("price"));
int keiyakutenno = Integer.parseInt(request.getParameter("keiyakutenno"));
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>編集内容確認画面</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報削除</a></li>
					</ul>
				</div>
				<br> <br>

			</div>

<div style="text-align:center">
<form action="B028" method="post">
編集内容を確認してください。
<br><br>
<table align="center">
<tr>
<td>商品番号　</td><td>：</td><td>　<input type="hidden" name="shohinno" value=shohinno>
</tr><tr>
<td>商品名　</td><td>：</td><td>　<input type="hidden" name="shohinmei" value=shohinmei><%=shohinmei %>
</tr><tr>
<td>柄　</td><td>：</td><td>　<input type="hidden" name="gara" value=gara><%=gara %>
</tr><tr>
<td>色　</td><td>：</td><td>　<input type="hidden" name="color" value=color><%=color %>
</tr><tr>
<td>サイズ　</td><td>：</td><td>　<input type="hidden" name="size" value=size><%=size %>
</tr><tr>
<td>販売開始日　</td><td>：</td><td>　<input type="hidden" name="startday" value=startday><%=startday %>
</tr><tr>
<td>販売終了日　</td><td>：</td><td>　<input type="hidden" name="endday" value=endday><%=endday %>
</tr><tr>
<td>価格　</td><td>：</td><td>　<input type="hidden" name="price" value=price><%=price %>
</tr><tr>
<td>契約店番号　</td><td>：</td><td>　<input type="hidden" name="keiyakuno" value=keiyakuno><%=keiyakutenno %>
</tr><tr>


</tr><tr>
</table>
<br>
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="編集完了" />
</form>
<form action="B028.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>
