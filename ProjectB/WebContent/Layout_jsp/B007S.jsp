<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="/ProjectB/Layout_jsp/layout.css" rel="stylesheet"
	type="text/css" />
<title>配送先選択</title>
</head>
<body>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<br> はるみ通信販売管理システム <br>
						<p>
						<div id="nav">
							<ul>
								<li><a target="_top" href="/ProjectB/Layout_jsp/B001.jsp">TOP</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B004S.jsp">商品番号入力</a></li>
								<li><a>商品詳細選択</a></li>
								<li><a>受注情報確認</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B007S.jsp">配送先選択</a></li>
								<li><a target="_self" href="/ProjectB/Layout_jsp/B008S.jsp">支払方法選択</a></li>
								<li><a>注文受付確認</a></li>
							</ul>
						</div>
						<br>
						お届け先を選択してください<br><br>
						<p><%=session.getAttribute("kanji")%>&nbsp;様
						</p>
						<form name="todokesakikubun" method="post"
							action="/ProjectB/todokesaki_servlet">
							<div id="box100d"></div>
							<div id="box75c">
								<div align="left">
									<input type="radio" name="todokesakikubun" value="1" checked>自宅&nbsp;
								</div>
							</div>
							<div id="box200c">お届け先氏名:</div>
							<div id="box200c">
								<input type="text" name="todokesakimei1">
							</div>
							<div id="box100d">
								<p style="display: inline;">留守：</p>
							</div>
							<div id="box200c">
								&nbsp;<input type="text" name="rusu1">
							</div>
							<div id="box100d"></div>
							<div id="box100d"></div>
							<div id="box75c">
								<div align="left">
									<input type="radio" name="todokesakikubun" value="2">会社&nbsp;
								</div>
							</div>
							<div id="box200c">お届け先氏名:</div>
							<div id="box200c">
								<input type="text" name="todokesakimei2">
							</div>
							<div id="box100d">
								<p style="display: inline;">留守：
							</div>
							<div id="box200c">
								<input type="text" name="rusu2">
							</div>
							<div id="box100d"></div>
							<div id="box100d"></div>
							<div id="box75c">
								<div align="left">
									<input type="radio" name="todokesakikubun" value="3">その他&nbsp;
								</div>
							</div>
							<div id="box200c"></div>
							<div id="box200c"></div>
							<div id="box100d"></div>
							<div id="box200c"></div>
							<div id="box100d"></div>
							<div id="box100d"></div>
							<div id="box75c"></div>
							<div id="box200c">
								<p style="display: inline;">郵便番号</p>
							</div>
							<div id="box200c">
								<input type="text" name="todokesakipost">
							</div>
							<div id="box100d"></div>
							<div id="box200c"></div>
							<div id="box100d"></div>
							<div id="box100d"></div>
							<div id="box75c"></div>
							<div id="box200c">
								<p style="display: inline;">住所</p>
							</div>
							<div id="box200c">
								<input type="text"  name="todokesakiad">
							</div>
							<div id="box100d">
								<p style="display: inline;">電話番号&nbsp;：&nbsp;</p>
							</div>
							<div id="box200c">
								<input type="text" name="todokesakitel">
							</div>
							<div id="box100d"></div>
							<div id="box100d"></div>
							<div id="box75c"></div>
							<div id="box200c">
								<p style="display: inline;">お届け先氏名
							</div>
							<div id="box200c">
								<input type="text" name="todokesakimei3">
							</div>
							<div id="box100d">
								<p style="display: inline;">留守&nbsp;：&nbsp;</p>
							</div>
							<div id="box200c">
								<input type="text" name="rusu3">
							</div>
							<div id="box100d"></div>
							<br> <br> <br> <br> <br> <br> <br>
							<br> <br> <br> <br> <br> <br> <br>
							<br> <br>
							<div align="center">
								<input type="submit"
									style="width: 8%; padding: 8px; font-size: 20px;" value="次へ" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
