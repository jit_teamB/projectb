<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,DTO.KeiyakutenDTO" %>
<%
ArrayList<KeiyakutenDTO> keiyakuList = (ArrayList<KeiyakutenDTO>)request.getAttribute("keiyakuList");
KeiyakutenDTO keiyaku = new KeiyakutenDTO();
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>契約店情報編集</title>
</head>
<div style="text-align:center">
<body>
<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店業情報削除</a></li>

					</ul>
				</div>
				<br> <br>



			</div>
編集する契約店情報を選択し、入力してください。
<br><br>
<table align="center">
<tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店名(漢字)　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenmei() %></td>
<td><input type="text" name="keiyakutenmei" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店名(カタカナ)　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenkana() %></td>
<td><input type="text" name="keiyakutenkana" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店電話番号　</td><td>：</td><td>　<%=keiyaku.getKeiyakutendenwano() %></td>
<td><input type="text" name="keiyakutel" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店FAX番号　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenfaxno() %></td>
<td><input type="text" name="keiyakufax" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店郵便番号　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenpost() %></td>
<td><input type="text" name="keiyakupost" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店住所　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenad() %></td>
<td><input type="text" name="keiyakuadd" id="textforscb3" />
</tr><tr>
<td><input type="checkbox" name="scb3" value="on" onclick="connecttext('textforscb3',this.checked);" /></td><td>契約店担当者　</td><td>：</td><td>　<%=keiyaku.getKeiyakutenmei() %></td>
<td><input type="text" name="keiyakutantou" id="textforscb3" />
</tr>
</table>
<br><br>
<form action="B041A.jsp" method="post">
<input type="submit" style="width: 10%;padding: 8px;font-size:20px;" value="編集" />
</form>
<br><br>
<form action="B043.jsp" method="post">
<input type="submit" style="width: 10%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</body>
</html>