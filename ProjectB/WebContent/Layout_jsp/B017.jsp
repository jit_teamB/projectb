<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>DB管理システム選択</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="layout.css" rel="stylesheet" type="text/css" />
</head>
<div id="contents">
	<div class="IE">
		<div class="Other">
			<div id="header">
				<div style="text-align: center">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
					</ul>
				</div>
				<br> <br>
			</div>
		</div>
	</div>
</div>
</body>

<body>
	<div style="text-align: center">
		管理するDBを選択してください <br> <br>
		<form action="B018A.jsp" method="post">
			<input type="submit" style="width: 16%; padding: 8px; font-size: 20px;" value="商品情報DB" />
			</form>
			<br> <br>
			<form action="B018.jsp" method="post">
			<input type="submit" style="width: 16%; padding: 8px; font-size: 20px;" value="会員情報DB" />
			</form>
			<br> <br>
			<form action="B018B.jsp" method="post">
			<input type="submit" style="width: 16%; padding: 8px; font-size: 20px;" value="協賛企業情報DB" />
			</form>
			<br> <br>
	 		<form action="B018C.jsp" method="post">
			 <input type="submit" style="width: 16%; padding: 8px; font-size: 20px;" value="契約店情報DB" />
			 </form>
			<br> <br>
	</div>
</body>
</html>
