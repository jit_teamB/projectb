<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="DTO.shohinDTO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
//文字コード
request.setCharacterEncoding("utf8");
//htmlから取得
String shohinno=request.getParameter("shohinno");
ArrayList<shohinDTO> shoihnList = (ArrayList<shohinDTO>)request.getAttribute("shoihnList");
%>
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
 <title>削除内容確認画面</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<p>はるみ通信販売管理システム</p>
				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報登録</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品情報編集</a></li>
					</ul>
				<br> <br>
削除内容を確認してください。
<br><br>
<table align="center">
<%
for(shohinDTO shohinList: shoihnList){
%>
<tr>
<td>商品番号　</td><td>：</td><td><input type="hidden" name="shohinno" value=<%=shohinList.getShohinno() %>><%=shohinList.getShohinno() %></td>
</tr><tr>
<td>商品名　</td><td>：</td><td><input type="hidden" name="shohinmei" value=<%=shohinList.getShohinmei() %>><%=shohinList.getShohinmei() %></td>
</tr><tr>
<td>重量・サイズ</td><td>：</td><td><input type="hidden" name="shohinsize" value=<%=shohinList.getSize() %>><%=shohinList.getSize() %></td>
</tr><tr>
<td>柄　</td><td>：</td><td><input type="hidden" name="shohingara" value=<%=shohinList.getGara() %>><%=shohinList.getGara() %></td>
</tr><tr>
<td>色　</td><td>：</td><td><input type="hidden" name="shohincolor" value=<%=shohinList.getColor() %>><%=shohinList.getColor() %></td>
</tr><tr>
<td>単位　</td><td>：</td><td><input type="hidden" name="shohintani" value=<%=shohinList.getTani() %>><%=shohinList.getTani() %></td>
</tr><tr>
<td>価格　</td><td>：</td><td><input type="hidden" name="price" value=<%=shohinList.getPrice() %>><%=shohinList.getPrice() %></td>
</tr><tr>
<td>販売開始日　</td><td>：</td><td><input type="hidden" name="startday" value=<%=shohinList.getStartdate() %>><%=shohinList.getStartdate() %></td>
</tr><tr>
<td>販売終了日　</td><td>：</td><td><input type="hidden" name="endday" value=<%=shohinList.getEnddate() %>><%=shohinList.getEnddate() %></td>
</tr><tr>

<td>契約店番号　</td><td>：</td><td><input type="hidden" name="keiyakuno" value=<%=shohinList.getKeiyakuten() %>><%=shohinList.getKeiyakuten() %></td>
</tr>
<%
}
%>
</table>
<br><br><br>
<form action="../B026B" method="post">
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="削除完了" />
</form>
<br>
<form action="B004B.jsp" method="post">
<input type="submit" style="width: 13%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div></div></div>
</body>
</html>