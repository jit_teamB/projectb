<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>発注書</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="layout.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="contents">
		<div class="IE">
			<div class="Other">
				<div id="header">
					<div align="center">
						<p>はるみ通信販売管理システム</p>
						<p>


						<div id="nav">
							<ul>
								<li><a target="_top" href="BBS.jsp">TOP</a></li>
								<li><a target="_blank"
									onclick="OpenWin(this.href,'685','705'); return false;"
									href="Signin.html">商品番号入力</a></li>
								<li><a target="_blank"
									onclick="OpenWin(this.href,'685','705'); return false;"
									href="FAQ.html">商品詳細選択</a></li>
								<li><a target="_blank"
									onclick="OpenWin(this.href,'685','705'); return false;"
									href="FAQ.html">受注情報確認</a></li>
								<li><a target="_blank"
									onclick="OpenWin(this.href,'685','705'); return false;"
									href="FAQ.html">配送先選択</a></li>
								<li><a target="_blank"
									onclick="OpenWin(this.href,'685','705'); return false;"
									href="FAQ.html">支払方法選択</a></li>
								<li><a target="_blank"
									onclick="OpenWin(this.href,'685','705'); return false;"
									href="FAQ.html">注文受付確認</a></li>

							</ul>
						</div>

						<div align="right">
						<p id="b_print">
<a href="Printout-Hacchu.html" target="_blank">
<button>印刷する</button>
</a>
</p></div>


						<br> <br> <font size=2> <font size=3>発注書（依頼請書）</font><br>
							<br>
							<div id="box675"></div>
							<div id="box100">ご依頼主</div>
							<div id="box200">西沢順子</div> <br>
							<div id="box225n">
								<u> かっぱ橋 電気店 </u>御中
							</div>
							<div id="box225n">
								<font size=1><u>契約販売店様番号 ６</u></font>
							</div>
							<div id="box525">
								<font size=1>枠内：注文受付票のお届け先コピー欄。 上のご依頼主は会員氏名を必ず記入のこと。</font>
							</div> <br>
							<div id="box225n">下記内容にて、発注いたします</div>
							<div id="box225n">
								<u>発送日:2014-06-14 <font size=1>(FAX送信日)</font></u>
							</div>
							<div id="box100">受注担当者</div>
							<div id="box100">田中</div>
							<div id="box75">注文受付</div>
							<div id="box100">2014-06-16</div>
							<div id="box50">番号</div>
							<div id="box100">00011</div>
							<br>


							<div id="box225n2">
								<font size=1>なお、納期などの諸条件は<br>基本契約内容に従うものとします。
								</font>
							</div>
							<div id="box225n"></div>

							<div id="box250z">お届け先住所</div>
							<div id="box125">届け先区分</div>
							<div id="box150">自宅</div>
							<br>

							<div id="box225n1">
								<font size=1>納品条件が基本契約と異なる場合は、</font>
							</div>
							<div id="box225n">
								<font size=4>はるみ通信販売株式会社</font>
							</div>
							<div id="box525">東京都大田区大森中３－１６－６ グッドハーモニー樹心１００</div> <br>
							<div id="box225n2">
								<font size=1>必ず弊社担当までご連絡願います。</font>
							</div>
							<div id="box225n2">
								<font size=1>営業部発注 <u>北田</u> 印<br> Tel:0541341232
									Fax:0541341233
								</font>
							</div>
							<div id="box200">留守の時</div>
							<div id="box325">隣のジミー 様</div> <br>
							<div id="box225n"></div>
							<div id="box225n2">
								<font size=1>色、柄、サイズ指定商品は<br>必ず弊社カタログをご確認ください。
								</font></u>
							</div>
							<div id="box100">氏名</div>
							<div id="box175">東山良子</div>
							<div id="box100">電話番号</div>
							<div id="box150">0342226666</div>
							<br><br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
							<div id="box75">商品番号</div>
							<div id="box175">商品名</div>
							<div id="box40">配送</div>
							<div id="box30">色</div>
							<div id="box40">柄</div>
							<div id="box40">
								<font size="1">サイズ</font>
							</div>
							<div id="box75">単価</div>
							<div id="box50">単位</div>
							<div id="box50">数量</div>
							<div id="box75">金額</div>
							<div id="box75">他・実費</div>
							<div id="box75">合計金額</div>
							<div id="box75">補足</div>
							<div id="box50">発送先</div>
							<div id="box50">発送日</div> <br>

							<div id="box75">1406070003</div>
							<div id="box175">ダンディーパンツ</div>
							<div id="box40">L</div>
							<div id="box30">白</div>
							<div id="box40">無地</div>
							<div id="box40">白</div>
							<div id="box75">12800</div>
							<div id="box50">個</div>
							<div id="box50">1</div>
							<div id="box75">12800</div>
							<div id="box75">0</div>
							<div id="box75">12800</div>
							<div id="box75"></div>
							<div id="box50">017</div>
							<div id="box50"></div>
							<div id="box475n"></div>
							<div id="box50">合計</div>
							<div id="box50">1</div>
							<div id="box75">12800</div>
							<div id="box75">0</div>
							<div id="box75">12800</div>
							<div id="box175a"></div> <br> <br> <br> <br><br>
							 <br>
							<div align=left>
								<div id="box375a">
									<font size=1>備考欄(弊社通信欄)</font>
								</div>
								<div id="box375a">
									<font size=1>摘要欄(契約販売店様通信欄)</font>
								</div>
							</div>
							<div id="box100a">発送方法</div>
							<div align=left>
								<div id="box125a">
									<font size=1>　白ねこダイワ</font>
								</div>
								<br>
								<div id="box375"></div>
								<div id="box375"></div>
								<div id="box100b"></div>
								<div id="box125b">
									<font size=1>　瀬川郵便局</font>
								</div>
								<br>
								<div id="box375"></div>
								<div id="box375"></div>
								<div id="box100b"></div>
								<div id="box125b">
									<font size=1>　その他( )</font>
								</div>
								<br>
							</div>
							<div id="box375"></div>
							<div id="box375"></div>
							<div id="box225n3">
								<font size=1>問合せ伝票番号</font>
							</div>
							<div id="box375"></div>
							<div id="box375"></div>
							<div id="box225n3">823-3591855</div>
							<br><br><br>	<br><br><br>	<br><br><br>






						</font>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
