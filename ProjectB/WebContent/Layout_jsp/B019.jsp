<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css" />
 <link href="layout.css" rel="stylesheet" type="text/css" />
<title>会員情報入力</title>
</head>

<div style="text-align:center">
<div id="contents">
		<div class="IE">
			<div class="Other">

				<div id="header">
					<p>はるみ通信販売管理システム</p>
				</div>

				<p>
				<div id="nav">
					<ul>
						<li><a target="_top" href="BBS.jsp">TOP</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="Signin.html">会員DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">商品DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">協賛企業DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">契約店DB</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報編集</a></li>
						<li><a target="_blank"
							onclick="OpenWin(this.href,'685','705'); return false;"
							href="FAQ.html">会員情報削除</a></li>

					</ul>
				</div>
				<br> <br>

会員情報を入力してください。
<form action="B019" method="post">
<br>
<table align="center">
<tr>
<td>社員番号</td><td>：</td><td><input type="text" name="shainno" size="70"><td>
</tr><tr>
<td>氏名(漢字)</td><td>：</td><td><input type="text" name="shainkanji" size="70"><td>
</tr>
<tr>
<td>氏名(カタカナ)</td><td>：</td><td><input type="text" name="shainkana" size="70"><td>
</tr>
<tr>
<td>電話番号</td><td>：</td><td><input type="text" name="shaintel" size="70"><td>
</tr>
<tr>
<td>FAX番号</td><td>：</td><td><input type="text" name="shainfax" size="70"><td>
</tr>
<tr>
<td>郵便番号</td><td>：</td><td><input type="text" name="shainpost" size="70"><td>
</tr>
<tr>
<td>会員住所</td><td>：</td><td><input type="text" name="shainadd" size="70" size="70"><td>
</tr>
<tr>
<td>会社名</td><td>：</td><td><input type="text" name="kaishamei" size="70"><td>
</tr>
<tr>
<td>所属部署</td><td>：</td><td><input type="text" name="shainbusho" size="70"><td>
</tr>
<tr>
<td>備考</td><td>：</td><td><input type="text" name="bikou"  size="70"><td>
</tr>
<tr>
</table>
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="登録" />
</form>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="B018.jsp" method="post">
<input type="submit" style="width: 8%;padding: 8px;font-size:20px;" value="戻る" />
</form>
</div></div></div></div>
</body>
</html>
